/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.util;

/**
 *
 * @author Achmad.Zulfikar
 */
public class NullChecker {
    
    public static Boolean notNull(Object o) {
        Boolean result = false;
        if (null != o) {
            result = true;
        }
        
        return result;
    }
    
}
