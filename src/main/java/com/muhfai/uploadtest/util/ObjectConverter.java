/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.util;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Achmad.Zulfikar
 */
public class ObjectConverter {
    
    public static Integer toInteger(Object o) {
        Integer result = null;
        if (null != o) {
            if (o instanceof BigDecimal) {
                result = ((BigDecimal) o).intValue();
            } else if (o instanceof Long) {
                result = ((Long) o).intValue();
            } else {
                result = (Integer) o;
            }
        }
        
        return result;
    }
    
    public static String toString(Object o) {
        String result = null;
        if (null != o) {
            result = (String) o;
        }
        
        return result;
    }
    
    public static Boolean toBoolean(Object o) {
        Boolean result = null;
        if (null != o) {
            result = (Boolean) o;
        }
        return result;
    }
    
    public static Date toDate(Object o) {
        Date result = null;
        if (null != o) {
            result = (Date) o;
        }
        return result;
    }
    
    public static BigDecimal toBigDecimal(Object o) {
        BigDecimal result = null;
        try {
            result = (BigDecimal) o;
        } catch (Exception e) {
        }
        return result;
    }
    
    public static Double toDouble(Object o) {
        Double result = null;
        try {
            if (o instanceof BigDecimal) {
                BigDecimal bd = (BigDecimal) o;
                result = bd.doubleValue();
            } else {
                result = (Double) o;
            }
        } catch (Exception e) {
        }
        return result;
    }
}
