/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core;

/**
 *
 * @author MuhFai10
 */
public class Message {
    public static final String SURVEY_SUCCESS = "Input Survey Success";
    public static final String INPUT_MATKUL_SUCCESS = "Input Data Matakuliah Success";
    public static final String UPDATE_MATKUL_SUCCESS = "Update Data Matakuliah Success";
    public static final String DELETE_MATKUL_SUCCESS = "Delete Data Matakuliah Success";
    public static final String INPUT_DOSEN_SUCCESS = "Input Data Dosen Success";
    public static final String UPDATE_DOSEN_SUCCESS = "Update Data Dosen Success";
    public static final String DELETE_DOSEN_SUCCESS = "Delete Data Dosen Success";
    public static final String INPUT_KUESIONER_SUCCESS = "Input Data Pertanyaan Success";
    public static final String UPDATE_KUESIONER_SUCCESS = "Update Data Pertanyaan Success";
    public static final String DELETE_KUESIONER_SUCCESS = "Delete Data Pertanyaan Success";
    public static final String INPUT_KELAS_SUCCESS = "Input Data Kelas Success";
    public static final String UPDATE_KELAS_SUCCESS = "Update Data Kelas Success";
    public static final String DELETE_KELAS_SUCCESS = "Delete Data Kelas Success";
    public static final String INPUT_MAHASISWA_SUCCESS = "Input Data Mahasiswa Success";
    public static final String UPDATE_MAHASISWA_SUCCESS = "Update Data Mahasiswa Success";
    public static final String DELETE_MAHASISWA_SUCCESS = "Delete Data Mahasiswa Success";
    public static final String INPUT_PERKULIAHAN_SUCCESS = "Input Data Perkuliahan Success";
    public static final String UPDATE_PERKULIAHAN_SUCCESS = "Update Data Perkuliahan Success";
    public static final String DELETE_PERKULIAHAN_SUCCESS = "Delete Data Perkuliahan Success";
    public static final String SEARCH_PERKULIAHAN_SUCCESS = "Search Data Perkuliahan Success";
    public static final String INPUT_USER_SUCCESS = "Input Data User Success";
    public static final String UPDATE_USER_SUCCESS = "Update Data User Success";
    public static final String DELETE_USER_SUCCESS = "Delete Data User Success";
    public static final String UPDATE_SEMESTER_SUCCESS = "Update Data Semester Success";
    public static final String INSERT_SEMESTER_SUCCESS = "Insert Data Semester Success";
    public static final String MATAKULIAH_FAILED = "Matakuliah Sudah Ada!";
}
