/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.mahasiswa;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.customdto.MhsCustomDto;
import com.muhfai.uploadtest.customrepository.MahasiswaCustomRepository;
import com.muhfai.uploadtest.dto.MahasiswaDto;
import com.muhfai.uploadtest.repository.mahasiswa.MahasiswaRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class MahasiswaManager extends MainResource {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(MahasiswaManager.class);
    private static final String TAG = "MAHASISWA_MGR";
    private final MahasiswaRepository mahasiswaRepository = new MahasiswaRepository();
    private final MahasiswaCustomRepository newMhsRepository = new MahasiswaCustomRepository();

    public MahasiswaBean findMahasiswaById(String noBp) {
        MahasiswaBean result = new MahasiswaBean();
        try {
            open();
            MahasiswaDto dto = mahasiswaRepository.findMahasiswaById(noBp);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load mahasiswa list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public NewMhsBean getMahasiswa(String noBp) {
        NewMhsBean result = new NewMhsBean();
        try {
            open();
            MhsCustomDto dto = newMhsRepository.getMahasiswa(noBp);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load mahasiswa list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBean_(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }
    public List<MahasiswaBean> findMahasiswa(String freeText) {
        List<MahasiswaBean> result = new ArrayList<MahasiswaBean>();
        try {
            open();
            List<MahasiswaDto> dtoList = mahasiswaRepository.findMahasiswa(freeText);
            if (dtoList == null) {
                managerResult = new ManagerResult(false, "Failed to load mahasiswa list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto(dtoList);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public void insertMahasiswa(MahasiswaBean mahasiswaBean) {
        try {
            open();
            MahasiswaDto dto = convertToDto(mahasiswaBean);
            String noBp = mahasiswaRepository.insertMahasiswa(dto);
            if (noBp != null) {
                managerResult = new ManagerResult(true, Message.INPUT_MAHASISWA_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Input data mahasiswa failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    public void updateMahasiswa(MahasiswaBean mahasiswaBean) {
        try {
            open();
            MahasiswaDto dto = convertToDto(mahasiswaBean);
            Boolean status = mahasiswaRepository.updateMahasiswa(dto);
            if (status == true) {
                managerResult = new ManagerResult(true, Message.UPDATE_MAHASISWA_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Update data mahasiswa failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    public void deleteMahasiswaById(String noBp) {
        try {
            open();

            Integer generatedId = mahasiswaRepository.deleteMahasiswaById(noBp);
            if (generatedId == 1) {
                managerResult = new ManagerResult(true, Message.DELETE_MAHASISWA_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Delete data mahasiswa failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    public List<MahasiswaBean> findAllMahasiswa() {
        List<MahasiswaBean> result = new ArrayList<MahasiswaBean>();
        try {
            open();
            List<MahasiswaDto> listDto = mahasiswaRepository.findAllMahasiswa();
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load mahasiswa list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    private List<MahasiswaBean> convertFromDto(List<MahasiswaDto> dtoList) {
        List<MahasiswaBean> beanList = new ArrayList<MahasiswaBean>();
        for (MahasiswaDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }
    
     private List<NewMhsBean> convertFromDto_(List<MhsCustomDto> dtoList) {
        List<NewMhsBean> beanList = new ArrayList<NewMhsBean>();
        for (MhsCustomDto dto : dtoList) {
            beanList.add(convertToBean_(dto));
        }
        return beanList;
    }

    private MahasiswaBean convertToBean(MahasiswaDto dto) {
        MahasiswaBean bean = new MahasiswaBean();
        bean.setNoBp(dto.getNoBp());
        bean.setNama(dto.getNama());
        bean.setKelas(dto.getKelas());
        bean.setLokal(dto.getLokal());
        bean.setStatus(dto.getStatus());
        //bean.setSemester(dto.getSemester());
        //bean.setProdi(dto.getProdi());
        return bean;
    }
    
    private NewMhsBean convertToBean_(MhsCustomDto dto) {
        NewMhsBean bean = new NewMhsBean();
        bean.setNoBp(dto.getNoBp());
        bean.setNama(dto.getNama());
        bean.setSemester(dto.getSemester());
        bean.setLokal(dto.getLokal());
        bean.setTahunAjaran(dto.getTahunAjaran());
        //bean.setSemester(dto.getSemester());
        //bean.setProdi(dto.getProdi());
        return bean;
    }

    private MahasiswaDto convertToDto(MahasiswaBean bean) {
        MahasiswaDto dto = new MahasiswaDto();
        dto.setNoBp(bean.getNoBp());
        dto.setNama(bean.getNama());
        dto.setKelas(bean.getKelas());
        dto.setLokal(bean.getLokal());
        dto.setStatus(bean.getStatus());
        //dto.setSemester(bean.getSemester());
        //dto.setProdi(bean.getProdi());
        return dto;
    }
    
}
