/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.mahasiswa;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class MahasiswaBean {
    @NotNull
    @NotEmpty
    @Min(12)
    private String noBp;
    @NotNull
    @NotEmpty
    private String nama;
    @NotNull
    private Integer kelas;
    @NotNull
    @NotEmpty
    private String lokal;
    private Integer status;
    //private Integer semester;
    //private Integer prodi;
}
