/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.perkuliahan;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class TampilKuliahBean {
    private Integer idPerkuliahan;
    private String kodeDosen;
    private String kodeDosen2;
    private String kodeMatkul;
    private String mataKuliah;
    private String namaDosen;
    private String namaDosen2;
}
