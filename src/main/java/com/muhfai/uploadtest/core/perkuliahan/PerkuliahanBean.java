/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.perkuliahan;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class PerkuliahanBean {
    private Integer idPerkuliahan;
    @NotNull
    private Integer idKelas;
    @NotNull
    @NotEmpty
    private String kodeMk;
    @NotNull
    @NotEmpty
    private String kodeDosen;
    private String kodeDosen2;
    @NotNull
    private Integer idSemester;
   // private Integer prodi;
}
