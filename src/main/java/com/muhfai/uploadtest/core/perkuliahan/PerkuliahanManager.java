/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.perkuliahan;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.customdto.NewPerkuliahanDto;
import com.muhfai.uploadtest.customrepository.PerkuliahanCustomRepository;
import com.muhfai.uploadtest.dto.PerkuliahanDto;
import com.muhfai.uploadtest.model.Perkuliahan;
import com.muhfai.uploadtest.repository.perkuliahan.PerkuliahanRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class PerkuliahanManager extends MainResource {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(PerkuliahanManager.class);
    private static final String TAG = "PERKULIAHAN_MGR";
    private final PerkuliahanRepository perkuliahanRepository = new PerkuliahanRepository();
    private final PerkuliahanCustomRepository newPerkuliahanRepository = new PerkuliahanCustomRepository();

    public List<PerkuliahanBean> findAllPerkuliahan(Integer kelas) {
        List<PerkuliahanBean> result = new ArrayList<PerkuliahanBean>();
        try {
            open();
            List<NewPerkuliahanDto> listDto = newPerkuliahanRepository.getAllPerkuliahan(kelas);
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load perkuliahan list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto_(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public List<PerkuliahanBean> findPerkuliahan(String freeText) {
        List<PerkuliahanBean> result = new ArrayList<PerkuliahanBean>();
        try {
            open();
            List<PerkuliahanDto> dtoList = perkuliahanRepository.findPerkuliahan(freeText);
            if (dtoList == null) {
                managerResult = new ManagerResult(false, "Failed to load perkuliahan list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto(dtoList);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public List<PerkuliahanBean> findAllPerkuliahan2() {
        List<PerkuliahanBean> result = new ArrayList<PerkuliahanBean>();
        try {
            open();
            List<NewPerkuliahanDto> listDto = newPerkuliahanRepository.getAllPerkuliahan2();
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load perkuliahan list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto_(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public List<PerkuliahanBean> findAllPerkuliahan3(Integer idProdi) {
        List<PerkuliahanBean> result = new ArrayList<PerkuliahanBean>();
        try {
            open();
            List<NewPerkuliahanDto> listDto = newPerkuliahanRepository.getAllPerkuliahan3(idProdi);
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load perkuliahan list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto_(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public List<PerkuliahanBean> getPerkuliahanBySemester(Integer idSemester) {
        List<PerkuliahanBean> result = new ArrayList<PerkuliahanBean>();
        try {
            open();
            List<NewPerkuliahanDto> listDto = newPerkuliahanRepository.getPerkuliahanBySemester(idSemester);
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load perkuliahan list");
            } else {
                managerResult = new ManagerResult(true, Message.SEARCH_PERKULIAHAN_SUCCESS);
                result = convertFromDto_(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    
    public List<PerkuliahanBean> getAllPerkuliahan() {
        List<PerkuliahanBean> listResult = new ArrayList<PerkuliahanBean>();
        try {
            open();
            List<PerkuliahanDto> listDto = perkuliahanRepository.getAllPerkuliahan();
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load perkuliahan list");
            } else {
                managerResult = new ManagerResult(true, null);
                listResult = convertFromDto(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return listResult;
    }

    public PerkuliahanBean findPerkuliahanById(Integer idPerkuliahan) {
        PerkuliahanBean result = new PerkuliahanBean();
        try {
            open();
            PerkuliahanDto dto = perkuliahanRepository.findPerkuliahanById(idPerkuliahan);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load perkuliahan list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public void insertPerkuliahan(PerkuliahanBean perkuliahanBean) {
        try {
            open();
            PerkuliahanDto dto = convertToDto(perkuliahanBean);
            Integer idPerkuliahan = null;
            if (dto.getKodeDosen2() == null || dto.getKodeDosen2().equals("")) {
                idPerkuliahan = perkuliahanRepository.insertPerkuliahan_(dto);
            } else {
                idPerkuliahan = perkuliahanRepository.insertPerkuliahan(dto);
            }

            if (idPerkuliahan != null) {
                managerResult = new ManagerResult(true, Message.INPUT_PERKULIAHAN_SUCCESS);
            } else {
                //perkuliahanRepository.updatePerkuliahan(dto);
                managerResult = new ManagerResult(true, "Input Perkuliahan Failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    public void updatePerkuliahan(PerkuliahanBean perkuliahanBean) {
        try {
            open();
            PerkuliahanDto dto = convertToDto(perkuliahanBean);
            Boolean status;
            if (dto.getKodeDosen2() == null || dto.getKodeDosen2().equals("")) {
                status = perkuliahanRepository.updatePerkuliahan_(dto);
            } else {
                status = perkuliahanRepository.updatePerkuliahan(dto);
            }

            if (status == true) {
                managerResult = new ManagerResult(true, Message.UPDATE_PERKULIAHAN_SUCCESS);
            } else {
                //perkuliahanRepository.updatePerkuliahan(dto);
                managerResult = new ManagerResult(true, "Update Perkuliahan Failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    public void deletePerkuliahanById(Integer idPerkuliahan) {
        try {
            open();

            Integer generatedId = perkuliahanRepository.deletePerkuliahanById(idPerkuliahan);
            if (generatedId == 1) {
                managerResult = new ManagerResult(true, Message.DELETE_PERKULIAHAN_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Delete data perkuliahan failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    private List<PerkuliahanBean> convertFromDto(List<PerkuliahanDto> dtoList) {
        List<PerkuliahanBean> beanList = new ArrayList<PerkuliahanBean>();
        for (PerkuliahanDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }

    private List<PerkuliahanBean> convertFromDto_(List<NewPerkuliahanDto> dtoList) {
        List<PerkuliahanBean> beanList = new ArrayList<PerkuliahanBean>();
        for (NewPerkuliahanDto dto : dtoList) {
            beanList.add(convertToBean_(dto));
        }
        return beanList;
    }

    private PerkuliahanBean convertToBean(PerkuliahanDto dto) {
        PerkuliahanBean bean = new PerkuliahanBean();
        bean.setIdPerkuliahan(dto.getIdPerkuliahan());
        bean.setIdKelas(dto.getIdKelas());
        bean.setKodeMk(dto.getKodeMk());
        bean.setKodeDosen(dto.getKodeDosen());
        bean.setKodeDosen2(dto.getKodeDosen2());
        bean.setIdSemester(dto.getIdSemester());
        //bean.setProdi(dto.getProdi());
        return bean;
    }

    private PerkuliahanBean convertToBean_(NewPerkuliahanDto dto) {
        PerkuliahanBean bean = new PerkuliahanBean();
        bean.setIdPerkuliahan(dto.getIdPerkuliahan());
        bean.setIdKelas(dto.getIdKelas());
        bean.setKodeMk(dto.getKodeMk());
        bean.setKodeDosen(dto.getKodeDosen());
        bean.setKodeDosen2(dto.getKodeDosen2());
        bean.setIdSemester(dto.getIdSemester());
        //bean.setProdi(dto.getProdi());
        return bean;
    }

    private PerkuliahanDto convertToDto(PerkuliahanBean bean) {
        PerkuliahanDto dto = new PerkuliahanDto();
        dto.setIdPerkuliahan(bean.getIdPerkuliahan());
        dto.setIdKelas(bean.getIdKelas());
        dto.setKodeMk(bean.getKodeMk());
        dto.setKodeDosen(bean.getKodeDosen());
        dto.setKodeDosen2(bean.getKodeDosen2());
        dto.setIdSemester(bean.getIdSemester());
        return dto;
    }

    private NewPerkuliahanDto convertToDto_(PerkuliahanBean bean) {
        NewPerkuliahanDto dto = new NewPerkuliahanDto();
        dto.setIdPerkuliahan(bean.getIdPerkuliahan());
        dto.setIdKelas(bean.getIdKelas());
        dto.setKodeMk(bean.getKodeMk());
        dto.setKodeDosen(bean.getKodeDosen());
        dto.setKodeDosen2(bean.getKodeDosen2());
        dto.setIdSemester(bean.getIdSemester());
        return dto;
    }
}
