/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.surveydetail;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.dto.SurveyDetailDto;
import com.muhfai.uploadtest.repository.surveydetail.SurveyDetailRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class SurveyDetailManager extends MainResource{
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SurveyDetailManager.class);
    private static final String TAG = "SURVEYDETAIL_MGR";
    private final SurveyDetailRepository svDetailRepository = new SurveyDetailRepository();
    
    public void insertSurveyDetail(List<SurveyDetailBean> svDetailBeanList) {
        try {
            open();
            List<SurveyDetailDto> dtoList = convertToDto(svDetailBeanList);
            Integer generatedId = svDetailRepository.insertSurveyDetail(dtoList);
            if (generatedId > 0) {
                managerResult = new ManagerResult(true, Message.SURVEY_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Insert survey detail failed");
            }
            
        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }
    
    private List<SurveyDetailBean> convertFromDto(List<SurveyDetailDto> dtoList) {
        List<SurveyDetailBean> beanList = new ArrayList<SurveyDetailBean>();
        for (SurveyDetailDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }
    
     private List<SurveyDetailDto> convertToDto(List<SurveyDetailBean> beanList) {
        List<SurveyDetailDto> dtoList = new ArrayList<SurveyDetailDto>();
        for (SurveyDetailBean bean : beanList) {
            dtoList.add(convertToDto(bean));
        }
        return dtoList;
    }

    private SurveyDetailBean convertToBean(SurveyDetailDto dto) {
        SurveyDetailBean bean = new SurveyDetailBean();
        bean.setIdSurveyDetail(dto.getIdSurveyDetail());
        bean.setIdSurvey(dto.getIdSurvey());
        bean.setIdKuesioner(dto.getIdKuesioner());
        bean.setNilai(dto.getNilai());
        return bean;
    }

    private SurveyDetailDto convertToDto(SurveyDetailBean bean) {
        SurveyDetailDto dto = new SurveyDetailDto();
        dto.setIdSurveyDetail(bean.getIdSurveyDetail());
        dto.setIdSurvey(bean.getIdSurvey());
        dto.setIdKuesioner(bean.getIdKuesioner());
        dto.setNilai(bean.getNilai());
        return dto;
    }
}
