/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.user;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.dto.UserDto;
import com.muhfai.uploadtest.repository.login.LoginRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class UserManager extends MainResource{
     private static final Logger LOGGER = LoggerFactory
            .getLogger(UserManager.class);
    private static final String TAG = "USER_MGR";
    private final LoginRepository loginRepository = new LoginRepository();
    
    public UserBean findUserById(Integer idUser) {
        UserBean result = new UserBean();
        try {
            open();
            UserDto dto = loginRepository.findUserById(idUser);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load user list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public UserBean findByUser(String username) {
        UserBean result = new UserBean();
        try {
            open();
            UserDto dto = loginRepository.findByUser(username);
            if(dto == null){
                managerResult = new ManagerResult(false, "Failed to load user list");
            }else{
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return result;
    }
    public List<UserBean> findUser(String freeText) {
        List<UserBean> result = new ArrayList<UserBean>();
        try {
            open();
            List<UserDto> dtoList = loginRepository.findUser(freeText);
            if (dtoList == null) {
                managerResult = new ManagerResult(false, "Failed to load user list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto(dtoList);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public void insertUser(UserBean userBean) {
        try {
             open();
            UserDto dto = convertToDto(userBean);
            
            if (dto.getIdUser()== null) {
                loginRepository.insertUser(dto);
                managerResult = new ManagerResult(true, Message.INPUT_USER_SUCCESS);
            } else {
                loginRepository.updateUser(dto);
                managerResult = new ManagerResult(true, Message.UPDATE_USER_SUCCESS);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }
    public void deleteUserById(Integer idUser) {
        try {
            open();

            Integer generatedId = loginRepository.deleteUserById(idUser);
            if (generatedId == 1) {
                managerResult = new ManagerResult(true, Message.DELETE_USER_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Delete data user failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    public List<UserBean> findAllUser() {
        List<UserBean> result = new ArrayList<UserBean>();
        try {
            open();
            List<UserDto> listDto = loginRepository.findAllUser();
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load user list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }
    
    
    private List<UserBean> convertFromDto(List<UserDto> dtoList) {
        List<UserBean> beanList = new ArrayList<UserBean>();
        for (UserDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }

    private UserBean convertToBean(UserDto dto) {
        UserBean bean = new UserBean();
        bean.setIdUser(dto.getIdUser());
        bean.setUsername(dto.getUsername());
        bean.setPassword(dto.getPassword());
        bean.setNama(dto.getNama());
        bean.setRole(dto.getRole());
        return bean;
    }

    private UserDto convertToDto(UserBean bean) {
        UserDto dto = new UserDto();
        dto.setIdUser(bean.getIdUser());
        dto.setUsername(bean.getUsername());
        dto.setPassword(bean.getPassword());
        dto.setNama(bean.getNama());
        dto.setRole(bean.getRole());
        return dto;
    }
}
