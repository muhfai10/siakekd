/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.prodi;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class ProdiBean {
    private Integer idProdi;
    private String prodi;
    private String kode;
}
