/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.prodi;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.dto.ProdiDto;
import com.muhfai.uploadtest.repository.prodi.ProdiRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class ProdiManager extends MainResource{
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ProdiManager.class);
    private static final String TAG = "PRODI_MGR";
    private final ProdiRepository prodiRepository = new ProdiRepository();
    
    public List<ProdiBean> findAllProdi(){
        List<ProdiBean> listResult = new ArrayList<ProdiBean>();
        try {
            open();
            List<ProdiDto> listDto = prodiRepository.findAllProdi();
            if(listDto == null){
                managerResult = new ManagerResult(false, "Failed to load prodi list");
            }else{
                managerResult = new ManagerResult(true, null);
                listResult = convertFromDto(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return listResult;
    }
    
    public ProdiBean findProdiById(Integer id){
        ProdiBean result = new ProdiBean();
        try {
            open();
            ProdiDto dto = prodiRepository.findProdiById(id);
            if(dto == null){
                managerResult = new ManagerResult(false, "Failed to load prodi list");
            }else{
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return result;
    }
    
    private List<ProdiBean> convertFromDto(List<ProdiDto> dtoList) {
        List<ProdiBean> beanList = new ArrayList<ProdiBean>();
        for (ProdiDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }

    private ProdiBean convertToBean(ProdiDto dto) {
        ProdiBean bean = new ProdiBean();
        bean.setProdi(dto.getProdi());
        bean.setKode(dto.getKode());
        bean.setIdProdi(dto.getIdProdi());
        return bean;
    }

    private ProdiDto convertToDto(ProdiBean bean) {
        ProdiDto dto = new ProdiDto();
        dto.setIdProdi(bean.getIdProdi());
        dto.setProdi(bean.getProdi());
        dto.setKode(bean.getKode());
        return dto;
    }
}
