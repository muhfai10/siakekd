/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.aspek;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.dto.AspekDto;
import com.muhfai.uploadtest.repository.aspek.AspekRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class AspekManager extends MainResource{
     private static final Logger LOGGER = LoggerFactory
            .getLogger(AspekManager.class);
    private static final String TAG = "ASPEK_MGR";
    private final AspekRepository aspekRepository = new AspekRepository();
    
    public List<AspekBean> findAllAspek(){
        List<AspekBean> listResult = new ArrayList<AspekBean>();
        try {
            open();
            List<AspekDto> listDto = aspekRepository.findAllAspek();
            if(listDto == null){
                managerResult = new ManagerResult(false, "Failed to load aspek list");
            }else{
                managerResult = new ManagerResult(true, null);
                listResult = convertFromDto(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return listResult;
    }
    
    public AspekBean findAspekById(Integer id){
        AspekBean result = new AspekBean();
        try {
            open();
            AspekDto dto = aspekRepository.findAspekById(id);
            if(dto == null){
                managerResult = new ManagerResult(false, "Failed to load aspek list");
            }else{
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return result;
    }
    
    private List<AspekBean> convertFromDto(List<AspekDto> dtoList) {
        List<AspekBean> beanList = new ArrayList<AspekBean>();
        for (AspekDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }

    private AspekBean convertToBean(AspekDto dto) {
        AspekBean bean = new AspekBean();
        bean.setIdAspek(dto.getIdAspek());
        bean.setAspek(dto.getAspek());
        return bean;
    }

    private AspekDto convertToDto(AspekBean bean) {
        AspekDto dto = new AspekDto();
        dto.setIdAspek(bean.getIdAspek());
        dto.setAspek(bean.getAspek());
        return dto;
    }
}
