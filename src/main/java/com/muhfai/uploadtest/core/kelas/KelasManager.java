/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.kelas;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.dto.KelasDto;
import com.muhfai.uploadtest.repository.kelas.KelasRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class KelasManager extends MainResource{
    private static final Logger LOGGER = LoggerFactory
            .getLogger(KelasManager.class);
    private static final String TAG = "KELAS_MGR";
    private final KelasRepository kelasRepository = new KelasRepository();
    
    public List<KelasBean> findAllKelas(){
        List<KelasBean> listResult = new ArrayList<KelasBean>();
        try {
            open();
            List<KelasDto> listDto = kelasRepository.findAllKelas();
            if(listDto == null){
                managerResult = new ManagerResult(false, "Failed to load kelas list");
            }else{
                managerResult = new ManagerResult(true, null);
                listResult = convertFromDto(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return listResult;
    }
    
    public KelasBean findKelasById(Integer idKelas){
        KelasBean result = new KelasBean();
        try {
            open();
            KelasDto dto = kelasRepository.findKelasById(idKelas);
            if(dto == null){
                managerResult = new ManagerResult(false, "Failed to load kelas list");
            }else{
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return result;
    }
    
    public void insertKelas(KelasBean kelasBean) {
        try {
            open();
            KelasDto dto = convertToDto(kelasBean);

            if (dto.getIdKelas()== null) {
                kelasRepository.insertKelas(dto);
                managerResult = new ManagerResult(true, Message.INPUT_KELAS_SUCCESS);
            } else {
                kelasRepository.updateKelas(dto);
                managerResult = new ManagerResult(true, Message.UPDATE_KELAS_SUCCESS);
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    public void deleteKelasById(Integer idKelas) {
        try {
            open();

            Integer generatedId = kelasRepository.deleteKelasById(idKelas);
            if (generatedId == 1) {
                managerResult = new ManagerResult(true, Message.DELETE_KELAS_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Delete data kelas failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }
    
    private List<KelasBean> convertFromDto(List<KelasDto> dtoList) {
        List<KelasBean> beanList = new ArrayList<KelasBean>();
        for (KelasDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }

    private KelasBean convertToBean(KelasDto dto) {
        KelasBean bean = new KelasBean();
        bean.setIdKelas(dto.getIdKelas());
        bean.setKelas(dto.getKelas());
        bean.setProdi(dto.getProdi());
        bean.setKode(dto.getKode());
        return bean;
    }

    private KelasDto convertToDto(KelasBean bean) {
        KelasDto dto = new KelasDto();
        dto.setIdKelas(bean.getIdKelas());
        dto.setKelas(bean.getKelas());
        dto.setProdi(bean.getProdi());
        dto.setKode(bean.getKode());
        return dto;
    }
}
