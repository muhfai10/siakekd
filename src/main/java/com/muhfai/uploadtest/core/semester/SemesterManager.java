/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.semester;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.customdto.SemesterCustomDto;
import com.muhfai.uploadtest.customrepository.SemesterCustomRepository;
import com.muhfai.uploadtest.dto.SemesterDto;
import com.muhfai.uploadtest.repository.semester.SemesterRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */

public class SemesterManager extends MainResource{
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SemesterManager.class);
    private static final String TAG = "SEMESTER_MGR";
    private final SemesterRepository semesterRepository = new SemesterRepository();
    private final SemesterCustomRepository smtRepo = new SemesterCustomRepository();
    
    public List<SemesterBean> findAllSemester(){
        List<SemesterBean> listResult = new ArrayList<SemesterBean>();
        try {
            open();
            List<SemesterDto> listDto = semesterRepository.findAllSemester();
            if(listDto == null){
                managerResult = new ManagerResult(false, "Failed to load semester list");
            }else{
                managerResult = new ManagerResult(true, null);
                listResult = convertFromDto(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return listResult;
    }
    
    public List<SemesterBean> getSemesterInPerkuliahan(){
        List<SemesterBean> listResult = new ArrayList<SemesterBean>();
        try {
            open();
            List<SemesterCustomDto> listDto = smtRepo.getSemesterInPerkuliahan();
            if(listDto == null){
                managerResult = new ManagerResult(false, "Failed to load semester list");
            }else{
                managerResult = new ManagerResult(true, null);
                listResult = convertFromDto_(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return listResult;
    }
    
    public SemesterBean findSemesterById(Integer id){
        SemesterBean result = new SemesterBean();
        try {
            open();
            SemesterDto dto = semesterRepository.findSemesterById(id);
            if(dto == null){
                managerResult = new ManagerResult(false, "Failed to load semester list");
            }else{
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return result;
    }
    
    public void updateSemester(SemesterBean semesterBean) {
        try {
            open();
            SemesterDto dto = convertToDto(semesterBean);
            Boolean status = semesterRepository.updateSemester(dto);
            if (status == true) {
                managerResult = new ManagerResult(true, Message.UPDATE_SEMESTER_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Update data semester failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }
    
    public void insertSemester(SemesterBean semesterBean) {
        try {
            open();
            SemesterDto dto = convertToDto(semesterBean);
            Integer idSemester = semesterRepository.insertSemester(dto);
            if (idSemester != null) {
                managerResult = new ManagerResult(true, Message.INSERT_SEMESTER_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Insert data semester failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }
    
    private List<SemesterBean> convertFromDto(List<SemesterDto> dtoList) {
        List<SemesterBean> beanList = new ArrayList<SemesterBean>();
        for (SemesterDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }
    private List<SemesterBean> convertFromDto_(List<SemesterCustomDto> dtoList) {
        List<SemesterBean> beanList = new ArrayList<SemesterBean>();
        for (SemesterCustomDto dto : dtoList) {
            beanList.add(convertToBean_(dto));
        }
        return beanList;
    }

    private SemesterBean convertToBean(SemesterDto dto) {
        SemesterBean bean = new SemesterBean();
        bean.setIdSemester(dto.getIdSemester());
        bean.setSemester(dto.getSemester());
        bean.setTahunAjaran(dto.getTahunAjaran());
        bean.setStatus(dto.getStatus());
        return bean;
    }
    private SemesterBean convertToBean_(SemesterCustomDto dto) {
        SemesterBean bean = new SemesterBean();
        bean.setIdSemester(dto.getIdSemester());
        bean.setSemester(dto.getSemester());
        bean.setTahunAjaran(dto.getTahunAjaran());
        bean.setStatus(dto.getStatus());
        return bean;
    }

    private SemesterDto convertToDto(SemesterBean bean) {
        SemesterDto dto = new SemesterDto();
        dto.setIdSemester(bean.getIdSemester());
        dto.setSemester(bean.getSemester());
        dto.setTahunAjaran(bean.getTahunAjaran());
        dto.setStatus(bean.getStatus());
        return dto;
    }
}
