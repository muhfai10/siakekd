/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.matakuliah;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.dto.MatakuliahDto;
import com.muhfai.uploadtest.repository.Matakuliah.MatakuliahRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class MatakuliahManager extends MainResource{
    private static final Logger LOGGER = LoggerFactory
            .getLogger(MatakuliahManager.class);
    private static final String TAG = "MATKUL_MGR";
    private final MatakuliahRepository matakuliahRepository = new MatakuliahRepository();
    
    public List<MatakuliahBean> findAllMatakuliah(){
        List<MatakuliahBean> listResult = new ArrayList<MatakuliahBean>();
        try {
            open();
            List<MatakuliahDto> listDto = matakuliahRepository.findAllMatkul();
            if(listDto == null){
                managerResult = new ManagerResult(false, "Failed to load matakuliah list");
            }else{
                managerResult = new ManagerResult(true, null);
                listResult = convertFromDto(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return listResult;
    }
    
    public MatakuliahBean findMatakuliahById(String kodeMk){
        MatakuliahBean result = new MatakuliahBean();
        try {
            open();
            MatakuliahDto dto = matakuliahRepository.findMatkulById(kodeMk);
            if(dto == null){
                managerResult = new ManagerResult(false, "Failed to load matakuliah list");
            }else{
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return result;
    }
    
    public List<MatakuliahBean> findMatkul(String freeText) {
        List<MatakuliahBean> result = new ArrayList<MatakuliahBean>();
        try {
            open();
            List<MatakuliahDto> dtoList = matakuliahRepository.findMatkul(freeText);
            if (dtoList == null) {
                managerResult = new ManagerResult(false, "Failed to load matakuliah list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto(dtoList);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }
    
    public void insertMatakuliah(MatakuliahBean matakuliahBean) {
        try {
            open();
            MatakuliahDto dto = convertToDto(matakuliahBean);
            String kodeMk = matakuliahRepository.insertMatakuliah(dto);
            if (kodeMk != null) {
                managerResult = new ManagerResult(true, Message.INPUT_MATKUL_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Input matakuliah failed");
            }
            
        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }
     
    public void updateMatakuliah(MatakuliahBean matakuliahBean) {
        try {
            open();
            MatakuliahDto dto = convertToDto(matakuliahBean);
            Boolean status = matakuliahRepository.updateMatakuliah(dto);
            if (status == true) {
                managerResult = new ManagerResult(true, Message.UPDATE_MATKUL_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Update matakuliah failed");
            }
            
        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }
    
      public void deleteMatakuliahById(String kodeMk) {
        try {
            open();
            
            Integer generatedId = matakuliahRepository.deleteMatakuliahById(kodeMk);
            if (generatedId == 1) {
                managerResult = new ManagerResult(true, Message.DELETE_MATKUL_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Delete survey failed");
            }
            
        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }
    
    private List<MatakuliahBean> convertFromDto(List<MatakuliahDto> dtoList) {
        List<MatakuliahBean> beanList = new ArrayList<MatakuliahBean>();
        for (MatakuliahDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }

    private MatakuliahBean convertToBean(MatakuliahDto dto) {
        MatakuliahBean bean = new MatakuliahBean();
        bean.setKodeMk(dto.getKodeMk());
        bean.setMataKuliah(dto.getMataKuliah());
        bean.setSks(dto.getSks());
        bean.setKeterangan(dto.getKeterangan());
        return bean;
    }

    private MatakuliahDto convertToDto(MatakuliahBean bean) {
        MatakuliahDto dto = new MatakuliahDto();
        dto.setKodeMk(bean.getKodeMk());
        dto.setMataKuliah(bean.getMataKuliah());
        dto.setSks(bean.getSks());
        dto.setKeterangan(bean.getKeterangan());
        return dto;
    }
}
