/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.matakuliah;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class MatakuliahBean {

    @NotNull
    @NotEmpty
    @Size(min = 6)
    private String kodeMk;
    @NotNull
    @NotEmpty
    private String mataKuliah;
    @NotNull
    private Integer sks;
    private String keterangan;
}
