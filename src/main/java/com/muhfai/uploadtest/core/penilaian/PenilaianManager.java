/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.penilaian;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.customdto.AssuranceDto;
import com.muhfai.uploadtest.customdto.EmpathyDto;
import com.muhfai.uploadtest.customdto.ReliabilityDto;
import com.muhfai.uploadtest.customdto.ResponsivenessDto;
import com.muhfai.uploadtest.customdto.TangibleDto;
import com.muhfai.uploadtest.customrepository.AssuranceCustomRepository;
import com.muhfai.uploadtest.customrepository.EmpathyCustomRepository;
import com.muhfai.uploadtest.customrepository.ReliabilityCustomRepository;
import com.muhfai.uploadtest.customrepository.ResponsivenessCustomRepository;
import com.muhfai.uploadtest.customrepository.TangibleCustomRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class PenilaianManager extends MainResource {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(PenilaianManager.class);
    private static final String TAG = "PENILAIAN_MGR";
    private final ReliabilityCustomRepository reliabilityRepository = new ReliabilityCustomRepository();
    private final ResponsivenessCustomRepository responsivenessRepository = new ResponsivenessCustomRepository();
    private final AssuranceCustomRepository assuranceRepository = new AssuranceCustomRepository();
    private final EmpathyCustomRepository empathyRepository = new EmpathyCustomRepository();
    private final TangibleCustomRepository tangibleRepository = new TangibleCustomRepository();

    public ReliabilityBean findReliability(String kodeDosen, Integer idPerkuliahan) {
        ReliabilityBean result = new ReliabilityBean();
        try {
            open();
            ReliabilityDto dto = reliabilityRepository.getReliability(kodeDosen, idPerkuliahan);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load reliability list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBeanReliability(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public ReliabilityBean findReliabilityBySemester(String kodeDosen, Integer idPerkuliahan, Integer semester) {
        ReliabilityBean result = new ReliabilityBean();
        try {
            open();
            ReliabilityDto dto = reliabilityRepository.getReliabilityBySemester(kodeDosen, idPerkuliahan, semester);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load reliability list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBeanReliability(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public ResponsivenessBean findResponsiveness(String kodeDosen, Integer idPerkuliahan) {
        ResponsivenessBean result = new ResponsivenessBean();
        try {
            open();
            ResponsivenessDto dto = responsivenessRepository.getResponsiveness(kodeDosen, idPerkuliahan);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load responsiveness list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBeanResponsiveness(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public ResponsivenessBean findResponsivenessBySemester(String kodeDosen, Integer idPerkuliahan, Integer semester) {
        ResponsivenessBean result = new ResponsivenessBean();
        try {
            open();
            ResponsivenessDto dto = responsivenessRepository.getResponsivenessBySemester(kodeDosen, idPerkuliahan, semester);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load responsiveness list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBeanResponsiveness(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public AssuranceBean findAssurance(String kodeDosen, Integer idPerkuliahan) {
        AssuranceBean result = new AssuranceBean();
        try {
            open();
            AssuranceDto dto = assuranceRepository.getAssurance(kodeDosen, idPerkuliahan);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load assurance list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBeanAssurance(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public AssuranceBean findAssuranceBySemester(String kodeDosen, Integer idPerkuliahan, Integer semester) {
        AssuranceBean result = new AssuranceBean();
        try {
            open();
            AssuranceDto dto = assuranceRepository.getAssuranceBySemester(kodeDosen, idPerkuliahan, semester);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load assurance list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBeanAssurance(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public EmpathyBean findEmpathy(String kodeDosen, Integer idPerkuliahan) {
        EmpathyBean result = new EmpathyBean();
        try {
            open();
            EmpathyDto dto = empathyRepository.getEmpathy(kodeDosen, idPerkuliahan);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load empathy list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBeanEmpathy(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public EmpathyBean findEmpathyBySemester(String kodeDosen, Integer idPerkuliahan, Integer semester) {
        EmpathyBean result = new EmpathyBean();
        try {
            open();
            EmpathyDto dto = empathyRepository.getEmpathyBySemester(kodeDosen, idPerkuliahan, semester);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load empathy list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBeanEmpathy(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public TangibleBean findTangible(String kodeDosen, Integer idPerkuliahan) {
        TangibleBean result = new TangibleBean();
        try {
            open();
            TangibleDto dto = tangibleRepository.getTangible(kodeDosen, idPerkuliahan);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load tangible list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBeanTangible(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public TangibleBean findTangibleBySemester(String kodeDosen, Integer idPerkuliahan, Integer semester) {
        TangibleBean result = new TangibleBean();
        try {
            open();
            TangibleDto dto = tangibleRepository.getTangibleBySemester(kodeDosen, idPerkuliahan, semester);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load tangible list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBeanTangible(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    private List<ReliabilityBean> convertFromDtoReliability(List<ReliabilityDto> dtoList) {
        List<ReliabilityBean> beanList = new ArrayList<ReliabilityBean>();
        for (ReliabilityDto dto : dtoList) {
            beanList.add(convertToBeanReliability(dto));
        }
        return beanList;
    }

    private ReliabilityBean convertToBeanReliability(ReliabilityDto dto) {
        ReliabilityBean bean = new ReliabilityBean();
        bean.setNilai(dto.getNilai());
        bean.setJumlah(dto.getJumlah());

        return bean;
    }

    private List<ResponsivenessBean> convertFromDtoResponsiveness(List<ResponsivenessDto> dtoList) {
        List<ResponsivenessBean> beanList = new ArrayList<ResponsivenessBean>();
        for (ResponsivenessDto dto : dtoList) {
            beanList.add(convertToBeanResponsiveness(dto));
        }
        return beanList;
    }

    private ResponsivenessBean convertToBeanResponsiveness(ResponsivenessDto dto) {
        ResponsivenessBean bean = new ResponsivenessBean();
        bean.setNilai(dto.getNilai());
        bean.setJumlah(dto.getJumlah());

        return bean;
    }

    private List<AssuranceBean> convertFromDtoAssurance(List<AssuranceDto> dtoList) {
        List<AssuranceBean> beanList = new ArrayList<AssuranceBean>();
        for (AssuranceDto dto : dtoList) {
            beanList.add(convertToBeanAssurance(dto));
        }
        return beanList;
    }

    private AssuranceBean convertToBeanAssurance(AssuranceDto dto) {
        AssuranceBean bean = new AssuranceBean();
        bean.setNilai(dto.getNilai());
        bean.setJumlah(dto.getJumlah());

        return bean;
    }

    private List<EmpathyBean> convertFromDtoEmpathy(List<EmpathyDto> dtoList) {
        List<EmpathyBean> beanList = new ArrayList<EmpathyBean>();
        for (EmpathyDto dto : dtoList) {
            beanList.add(convertToBeanEmpathy(dto));
        }
        return beanList;
    }

    private EmpathyBean convertToBeanEmpathy(EmpathyDto dto) {
        EmpathyBean bean = new EmpathyBean();
        bean.setNilai(dto.getNilai());
        bean.setJumlah(dto.getJumlah());

        return bean;
    }

    private List<TangibleBean> convertFromDtoTangible(List<TangibleDto> dtoList) {
        List<TangibleBean> beanList = new ArrayList<TangibleBean>();
        for (TangibleDto dto : dtoList) {
            beanList.add(convertToBeanTangible(dto));
        }
        return beanList;
    }

    private TangibleBean convertToBeanTangible(TangibleDto dto) {
        TangibleBean bean = new TangibleBean();
        bean.setNilai(dto.getNilai());
        bean.setJumlah(dto.getJumlah());

        return bean;
    }
}
