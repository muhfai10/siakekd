/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.dosen;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.dto.DosenDto;
import com.muhfai.uploadtest.repository.dosen.DosenRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class DosenManager extends MainResource {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(DosenManager.class);
    private static final String TAG = "DOSEN_MGR";
    private final DosenRepository dosenRepository = new DosenRepository();

    public List<DosenBean> findAllDosen() {
        List<DosenBean> listResult = new ArrayList<DosenBean>();
        try {
            open();
            List<DosenDto> listDto = dosenRepository.findAllDosen();
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load dosen list");
            } else {
                managerResult = new ManagerResult(true, null);
                listResult = convertFromDto(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return listResult;
    }

    public DosenBean findDosenById(String idDosen) {
        DosenBean result = new DosenBean();
        try {
            open();
            DosenDto dto = dosenRepository.findDosenById(idDosen);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load dosen list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public List<DosenBean> findDosen(String freeText) {
        List<DosenBean> result = new ArrayList<DosenBean>();
        try {
            open();
            List<DosenDto> dtoList = dosenRepository.findDosen(freeText);
            if (dtoList == null) {
                managerResult = new ManagerResult(false, "Failed to load dosen list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto(dtoList);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public void insertDosen(DosenBean dosenBean) {
        try {
            open();
            DosenDto dto = convertToDto(dosenBean);
            String kodeDosen = dosenRepository.insertDosen(dto);
            if (kodeDosen != null) {
                managerResult = new ManagerResult(true, Message.INPUT_DOSEN_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Input data dosen failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    public void updateDosen(DosenBean dosenBean) {
        try {
            open();
            DosenDto dto = convertToDto(dosenBean);
            Boolean status = dosenRepository.updateDosen(dto);
            if (status == true) {
                managerResult = new ManagerResult(true, Message.UPDATE_DOSEN_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Update data dosen failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    public void deleteDosenById(String kodeDosen) {
        try {
            open();

            Integer generatedId = dosenRepository.deleteDosenById(kodeDosen);
            if (generatedId == 1) {
                managerResult = new ManagerResult(true, Message.DELETE_DOSEN_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Delete data dosen failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    private List<DosenBean> convertFromDto(List<DosenDto> dtoList) {
        List<DosenBean> beanList = new ArrayList<DosenBean>();
        for (DosenDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }

    private DosenBean convertToBean(DosenDto dto) {
        DosenBean bean = new DosenBean();
        bean.setKodeDosen(dto.getKodeDosen());
        bean.setNama(dto.getNama());
        bean.setNip(dto.getNip());
        bean.setNidn(dto.getNidn());
        bean.setNoTelp(dto.getNoTelp());
        bean.setEmail(dto.getEmail());
        return bean;
    }

    private DosenDto convertToDto(DosenBean bean) {
        DosenDto dto = new DosenDto();
        dto.setKodeDosen(bean.getKodeDosen());
        dto.setNama(bean.getNama());
        dto.setNip(bean.getNip());
        dto.setNidn(bean.getNidn());
        dto.setNoTelp(bean.getNoTelp());
        dto.setEmail(bean.getEmail());
        return dto;
    }
}
