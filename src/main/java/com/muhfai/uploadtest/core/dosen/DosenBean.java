/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.dosen;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class DosenBean {
    @NotNull
    @NotEmpty
    @Size(min=5)
    private String kodeDosen;
    @NotNull
    @NotEmpty
    private String nama;
    @NotNull
    @NotEmpty(message="Nama tidak boleh kosong!")
    @Size(min=21,message="NIP berjumlah 21 karakter!")
    private String nip;
    @NotNull
    @NotEmpty
    @Size(min=10,message="NIDN berjumlah 10 karakter!")
    private String nidn;
    @NotNull
    @NotEmpty
    @Size(min=12,message="No. Telepon berjumlah 12 karakter!")
    private String noTelp;
    private String email;
}
