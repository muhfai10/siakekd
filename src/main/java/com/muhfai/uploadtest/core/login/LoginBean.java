/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.login;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class LoginBean {
    private Integer idUser;
    private String username;
    private String password;
    private String nama;
    private Integer role;
}
