/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.login;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.dto.UserDto;
import com.muhfai.uploadtest.repository.login.LoginRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class LoginManager extends MainResource{
     private static final Logger LOGGER = LoggerFactory
            .getLogger(LoginManager.class);
    private static final String TAG = "LOGIN_MGR";
    private final LoginRepository loginRepository = new LoginRepository();
    
    public LoginBean getLogin(String username, String password) {
        LoginBean result = new LoginBean();
        try {
            open();
            UserDto dto = loginRepository.getLogin(username,password);
            if(dto == null){
                managerResult = new ManagerResult(false, "Failed to Login");
            }else{
                managerResult = new ManagerResult(true, "Login Berhasil");
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return result;
    }

    private List<LoginBean> convertFromDto(List<UserDto> dtoList) {
        List<LoginBean> beanList = new ArrayList<LoginBean>();
        for (UserDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }

    private LoginBean convertToBean(UserDto dto) {
        LoginBean bean = new LoginBean();
        bean.setIdUser(dto.getIdUser());
        bean.setUsername(dto.getUsername());
        bean.setPassword(dto.getPassword());
        bean.setNama(dto.getNama());
        bean.setRole(dto.getRole());
        return bean;
    }

    private UserDto convertToDto(LoginBean bean) {
        UserDto dto = new UserDto();
        dto.setIdUser(bean.getIdUser());
        dto.setUsername(bean.getUsername());
        dto.setPassword(bean.getPassword());
        dto.setNama(bean.getNama());
        dto.setRole(bean.getRole());
        return dto;
    }
}
