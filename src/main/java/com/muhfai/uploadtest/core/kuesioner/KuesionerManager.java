/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.kuesioner;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.dto.KuesionerDto;
import com.muhfai.uploadtest.repository.kuesioner.KuesionerRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class KuesionerManager extends MainResource {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(KuesionerManager.class);
    private static final String TAG = "KUESIONER_MGR";
    private final KuesionerRepository kuesionerRepository = new KuesionerRepository();

    public List<KuesionerBean> findAllKuesioner() {
        List<KuesionerBean> listResult = new ArrayList<KuesionerBean>();
        try {
            open();
            List<KuesionerDto> listDto = kuesionerRepository.findAllKuesioner();
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load kuesioner list");
            } else {
                managerResult = new ManagerResult(true, null);
                listResult = convertFromDto(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return listResult;
    }

    public KuesionerBean findKuesionerById(Integer idKuesioner) {
        KuesionerBean result = new KuesionerBean();
        try {
            open();
            KuesionerDto dto = kuesionerRepository.findKuesionerById(idKuesioner);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load pertanyaan list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public List<KuesionerBean> findPertanyaan(String pertanyaan) {
        List<KuesionerBean> result = new ArrayList<KuesionerBean>();
        try {
            open();
            List<KuesionerDto> dtoList = kuesionerRepository.findPertanyaan(pertanyaan);
            if (dtoList == null) {
                managerResult = new ManagerResult(false, "Failed to load pertanyaan list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto(dtoList);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public List<KuesionerBean> findKuesionerByAspek(Integer idAspek) {
        List<KuesionerBean> result = new ArrayList<KuesionerBean>();
        try {
            open();
            List<KuesionerDto> dtoList = kuesionerRepository.findKuesionerByAspek(idAspek);
            if (dtoList == null) {
                managerResult = new ManagerResult(false, "Failed to load pertanyaan list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto(dtoList);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public void insertKuesioner(KuesionerBean kuesionerBean) {
        try {
            open();
            KuesionerDto dto = convertToDto(kuesionerBean);

            if (dto.getIdKuesioner() == null) {
                kuesionerRepository.insertKuesioner(dto);
                managerResult = new ManagerResult(true, Message.INPUT_KUESIONER_SUCCESS);
            } else {
                kuesionerRepository.updateKuesioner(dto);
                managerResult = new ManagerResult(true, Message.UPDATE_KUESIONER_SUCCESS);
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    public void deleteKuesionerById(Integer idKuesioner) {
        try {
            open();

            Integer generatedId = kuesionerRepository.deleteKuesionerById(idKuesioner);
            if (generatedId == 1) {
                managerResult = new ManagerResult(true, Message.DELETE_KUESIONER_SUCCESS);
            } else {
                managerResult = new ManagerResult(false, "Delete data pertanyaan failed");
            }

        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
    }

    private List<KuesionerBean> convertFromDto(List<KuesionerDto> dtoList) {
        List<KuesionerBean> beanList = new ArrayList<KuesionerBean>();
        for (KuesionerDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }

    private KuesionerBean convertToBean(KuesionerDto dto) {
        KuesionerBean bean = new KuesionerBean();
        bean.setIdKuesioner(dto.getIdKuesioner());
        bean.setPertanyaan(dto.getPertanyaan());
        bean.setAspek(dto.getAspek());
        return bean;
    }

    private KuesionerDto convertToDto(KuesionerBean bean) {
        KuesionerDto dto = new KuesionerDto();
        dto.setIdKuesioner(bean.getIdKuesioner());
        dto.setPertanyaan(bean.getPertanyaan());
        dto.setAspek(bean.getAspek());
        return dto;
    }
}
