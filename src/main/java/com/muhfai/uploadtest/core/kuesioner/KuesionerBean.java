/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.kuesioner;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class KuesionerBean {
    private Integer idKuesioner;
    @NotNull
    @NotEmpty
    private String pertanyaan;
    @NotNull
    private Integer aspek;
}
