/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.survey;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.customdto.SurveyCustomDto;
import com.muhfai.uploadtest.customrepository.SurveyCustomRepository;
import com.muhfai.uploadtest.dto.SurveyDto;
import com.muhfai.uploadtest.repository.survey.SurveyRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class SurveyManager extends MainResource{
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SurveyManager.class);
    private static final String TAG = "SURVEY_MGR";
    private final SurveyRepository surveyRepository = new SurveyRepository();
    private final SurveyCustomRepository surveyCustomRepo = new SurveyCustomRepository();
    
     public Integer insertSurvey(SurveyBean surveyBean) {
         Integer idSurvey=null;
        try {
            open();
            
            SurveyDto dto = convertToDto(surveyBean);
            Integer generatedId = surveyRepository.insertSurvey(dto);
            if (generatedId > 0) {
                idSurvey = generatedId;
                managerResult = new ManagerResult(true, "Insert survey success");
            } else {
                managerResult = new ManagerResult(false, "Insert survey failed");
            }
            
        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
        return idSurvey;
    }
     
     public SurveyBean findSurveyByMhsDosen(String noBp,String kodeDosen,Integer idPerkuliahan){
        SurveyBean result = new SurveyBean();
        try {
            open();
            SurveyDto dto = surveyRepository.findSurveyByMhsDosen(noBp, kodeDosen, idPerkuliahan);
            if(dto == null){
                managerResult = new ManagerResult(false, "Failed to load survey list");
            }else{
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG,e);
        } finally {
            close();
        }
        return result;
    }
     
      public Integer deleteSurveyById(Integer idSurvey) {
          Integer status = null;
        try {
            open();
            
            Integer generatedId = surveyRepository.deleteSurveyById(idSurvey);
            if (generatedId == 1) {
                status = generatedId;
                managerResult = new ManagerResult(true, "Delete survey success");
            } else {
                managerResult = new ManagerResult(false, "Delete survey failed");
            }
            
        } catch (Exception e) {
            LOGGER.error(TAG, e);
            managerResult = new ManagerResult(false, e.getMessage());
        } finally {
            close();
        }
        return status;
    }
      
      public List<SurveyBean> getSurveyByProdi(Integer idProdi) {
        List<SurveyBean> result = new ArrayList<SurveyBean>();
        try {
            open();
            List<SurveyCustomDto> listDto = surveyCustomRepo.getSurveyByProdi(idProdi);
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load survey list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto_(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }
      
       public List<SurveyBean> getAllSurvey() {
        List<SurveyBean> result = new ArrayList<SurveyBean>();
        try {
            open();
            List<SurveyCustomDto> listDto = surveyCustomRepo.getAllSurvey();
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load survey list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto_(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }
      
      public List<SurveyBean> getSurveyBySemester(Integer idProdi,Integer semester) {
        List<SurveyBean> result = new ArrayList<SurveyBean>();
        try {
            open();
            List<SurveyCustomDto> listDto = surveyCustomRepo.getSurveyBySemester(idProdi,semester);
            if (listDto == null) {
                managerResult = new ManagerResult(false, "Failed to load survey list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertFromDto_(listDto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    
    private List<SurveyBean> convertFromDto(List<SurveyDto> dtoList) {
        List<SurveyBean> beanList = new ArrayList<SurveyBean>();
        for (SurveyDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }
    
     private List<SurveyBean> convertFromDto_(List<SurveyCustomDto> dtoList) {
        List<SurveyBean> beanList = new ArrayList<SurveyBean>();
        for (SurveyCustomDto dto : dtoList) {
            beanList.add(convertToBean_(dto));
        }
        return beanList;
    }

    private SurveyBean convertToBean(SurveyDto dto) {
        SurveyBean bean = new SurveyBean();
        bean.setIdSurvey(dto.getIdSurvey());
        bean.setIdPerkuliahan(dto.getIdPerkuliahan());
        bean.setTanggal(dto.getTanggal());
        bean.setNoBp(dto.getNoBp());
        bean.setKodeDosen(dto.getKodeDosen());
        bean.setIdSemester(dto.getIdSemester());
        return bean;
    }
    private SurveyBean convertToBean_(SurveyCustomDto dto) {
        SurveyBean bean = new SurveyBean();
        bean.setIdSurvey(dto.getIdSurvey());
        bean.setIdPerkuliahan(dto.getIdPerkuliahan());
        bean.setTanggal(dto.getTanggal());
        bean.setNoBp(dto.getNoBp());
        bean.setKodeDosen(dto.getKodeDosen());
        bean.setIdSemester(dto.getSemester());
        return bean;
    }

    private SurveyDto convertToDto(SurveyBean bean) {
        SurveyDto dto = new SurveyDto();
        dto.setIdSurvey(bean.getIdSurvey());
        dto.setIdPerkuliahan(bean.getIdPerkuliahan());
        dto.setTanggal(bean.getTanggal());
        dto.setNoBp(bean.getNoBp());
        dto.setKodeDosen(bean.getKodeDosen());
        dto.setIdSemester(bean.getIdSemester());
        return dto;
    }
}
