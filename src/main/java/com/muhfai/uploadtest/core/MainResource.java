/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core;

import org.javalite.activejdbc.Base;

public abstract class MainResource {
    
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/kuesioner_new";
    private static final String USER = "root";
    private static final String PASS = "";
    
    protected ManagerResult managerResult;
    
    public void open() {
        Base.open(DRIVER, URL, USER, PASS);
    }
    
    public void close() {
        Base.close();
    }
    
    public ManagerResult getManagerResult() {
        return managerResult;
    }
}
