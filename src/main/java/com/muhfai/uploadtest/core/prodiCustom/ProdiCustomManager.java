/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.core.prodiCustom;

import com.muhfai.uploadtest.core.MainResource;
import com.muhfai.uploadtest.core.ManagerResult;
import com.muhfai.uploadtest.customdto.ProdiCustomDto;
import com.muhfai.uploadtest.customrepository.ProdiCustomRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MuhFai10
 */
public class ProdiCustomManager extends MainResource{
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ProdiCustomManager.class);
    private static final String TAG = "PRODI_CUSTOM_MGR";
    private final ProdiCustomRepository prodiRepository = new ProdiCustomRepository();
    
    public ProdiCustomBean findProdi(Integer idProdi) {
        ProdiCustomBean result = new ProdiCustomBean();
        try {
            open();
            ProdiCustomDto dto = prodiRepository.getProdi(idProdi);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load prodi list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    public ProdiCustomBean findProdiBySemester(Integer idProdi,Integer idSemester) {
        ProdiCustomBean result = new ProdiCustomBean();
        try {
            open();
            ProdiCustomDto dto = prodiRepository.getProdiBySemester(idProdi,idSemester);
            if (dto == null) {
                managerResult = new ManagerResult(false, "Failed to load prodi list");
            } else {
                managerResult = new ManagerResult(true, null);
                result = convertToBean(dto);
            }
        } catch (Exception e) {
            LOGGER.error(TAG, e);
        } finally {
            close();
        }
        return result;
    }

    
    
    private List<ProdiCustomBean> convertFromDto(List<ProdiCustomDto> dtoList) {
        List<ProdiCustomBean> beanList = new ArrayList<ProdiCustomBean>();
        for (ProdiCustomDto dto : dtoList) {
            beanList.add(convertToBean(dto));
        }
        return beanList;
    }

    private ProdiCustomBean convertToBean(ProdiCustomDto dto) {
        ProdiCustomBean bean = new ProdiCustomBean();
        bean.setProdi(dto.getProdi());
        bean.setSemester(dto.getSemester());
        bean.setTahunAjaran(dto.getTahunAjaran());

        return bean;
    }
    
}
