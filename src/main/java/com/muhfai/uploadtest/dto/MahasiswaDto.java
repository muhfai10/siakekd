/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.dto;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class MahasiswaDto {
    private String noBp;
    private String nama;
    private Integer kelas;
    private String lokal;
    private Integer status;
    //private Integer semester;
    //private Integer prodi;
}
