/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.dto;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class DosenDto {
    private String kodeDosen;
    private String nama;
    private String nip;
    private String nidn;
    private String noTelp;
    private String email;
}
