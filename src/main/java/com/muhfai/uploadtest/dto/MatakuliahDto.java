/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.dto;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class MatakuliahDto {
    private String kodeMk;
    private String mataKuliah;
    private Integer sks;
    private String keterangan;
}
