/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.dto;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class SemesterDto {
    private Integer idSemester;
    private String semester;
    private String tahunAjaran;
    private Integer status;
}
