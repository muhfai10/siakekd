/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.dto;

import java.util.Date;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class SurveyDto {
    private Integer idSurvey;
    private Integer idPerkuliahan;
    private Date tanggal;
    private String noBp;
    private String kodeDosen;
    private Integer idSemester;
}
