/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.controller.login;

import com.muhfai.uploadtest.core.user.UserBean;
import com.muhfai.uploadtest.service.login.LoginService;
import com.muhfai.uploadtest.service.login.LoginServiceInput;
import com.muhfai.uploadtest.service.login.LoginServiceOutput;
import com.muhfai.uploadtest.service.login.UserList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author MuhFai10
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    private static final Logger logger = LoggerFactory
            .getLogger(UserController.class);
    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/listUser", method = RequestMethod.GET)
    public String listUser(Model model,HttpSession session, @RequestParam(required = false) Integer page) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        LoginServiceOutput userOutput = loginService.findAllUser();
        List<UserBean> listUsrBean = userOutput.getListUserBean();
        List<UserList> listUser = new ArrayList<UserList>();
        for (UserBean userBean : listUsrBean) {
            UserList user = new UserList();
            LoginServiceInput userInput = new LoginServiceInput();
            userInput.setIdUser(userBean.getIdUser());
            LoginServiceOutput newOutput = loginService.findUserById(userInput);
            user.setIdUser(newOutput.getUserBean().getIdUser());
            user.setUsername(newOutput.getUserBean().getUsername());
            user.setPassword(newOutput.getUserBean().getPassword());
            user.setNama(newOutput.getUserBean().getNama());
            switch (newOutput.getUserBean().getRole()) {
                case 1:
                    user.setRole("Administrator");
                    break;
                case 2:
                    user.setRole("Mahasiswa");
                    break;
                default:
                    user.setRole("KAJUR");
                    break;
            }
            listUser.add(user);
        }
        PagedListHolder<UserList> pageListHolder = new PagedListHolder<UserList>(listUser);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }

        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listUser", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listUser", pageListHolder.getPageList());
        }
        return "listUser";
    }

    @RequestMapping(value = "/searchUser")
    public String searchUser(Model model,HttpSession session ,@RequestParam("freeText") String freeText, @RequestParam(required = false) Integer page) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        LoginServiceInput userInput = new LoginServiceInput();
        userInput.setFreeText(freeText);
        LoginServiceOutput userOutput = loginService.findUser(userInput);
        List<UserBean> listUsrBean = userOutput.getListUserBean();
        List<UserList> listUser = new ArrayList<UserList>();
        for (UserBean userBean : listUsrBean) {
            UserList user = new UserList();
            LoginServiceInput userInput2 = new LoginServiceInput();
            userInput2.setIdUser(userBean.getIdUser());
            LoginServiceOutput newOutput = loginService.findUserById(userInput2);
            user.setUsername(newOutput.getUserBean().getUsername());
            user.setPassword(newOutput.getUserBean().getPassword());
            user.setNama(newOutput.getUserBean().getNama());
            switch (newOutput.getUserBean().getRole()) {
                case 1:
                    user.setRole("Administrator");
                    break;
                case 2:
                    user.setRole("Mahasiswa");
                    break;
                default:
                    user.setRole("KAJUR");
                    break;
            }
            listUser.add(user);
        }
        PagedListHolder<UserList> pageListHolder = new PagedListHolder<UserList>(listUser);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }

        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listUser", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listUser", pageListHolder.getPageList());
        }
        return "listUser";
    }

    @RequestMapping(value = "/newUser", method = RequestMethod.GET)
    public String newUser(Model model,HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        UserBean user = new UserBean();
        Map<Integer, String> roleMap = new HashMap<Integer, String>();
        roleMap.put(1, "Administrator");
        roleMap.put(2, "Mahasiswa");
        roleMap.put(3, "KAJUR");
        model.addAttribute("mapRole", roleMap);
        model.addAttribute("user", user);
        return "newUser";
    }

    @RequestMapping(value = "/editUser/{idUser}", method = RequestMethod.GET)
    public String editUser(@PathVariable("idUser") Integer idUser,
            Model model) {
        LoginServiceInput input = new LoginServiceInput();
        input.setIdUser(idUser);
        Map<Integer, String> roleMap = new HashMap<Integer, String>();
        roleMap.put(1, "Administrator");
        roleMap.put(2, "Mahasiswa");
        roleMap.put(3, "KAJUR");
        LoginServiceOutput output = loginService.findUserById(input);
        model.addAttribute("mapRole", roleMap);
        model.addAttribute("user", output.getUserBean());
        return "newUser";
    }

    @RequestMapping(value = "/deleteUser/{idUser}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable("idUser") Integer idUser,
            RedirectAttributes ra) {
        LoginServiceInput input = new LoginServiceInput();
        input.setIdUser(idUser);
        LoginServiceOutput output = loginService.deleteUserById(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/user/listUser";
    }

    @RequestMapping(value = "/insertUser", method = RequestMethod.POST)
    public String insertUser(@ModelAttribute("user") @Valid UserBean userBean,
            BindingResult result,
            RedirectAttributes ra, Model model,HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        LoginServiceInput input = new LoginServiceInput();
        input.setUserBean(userBean);
        if (result.hasErrors()) {
            Map<Integer, String> roleMap = new HashMap<Integer, String>();
            roleMap.put(1, "Administrator");
            roleMap.put(2, "Mahasiswa");
            roleMap.put(3, "KAJUR");
            model.addAttribute("mapRole", roleMap);
            return "newUser";
        }
        LoginServiceOutput output = loginService.insertUser(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/user/listUser";
    }

}
