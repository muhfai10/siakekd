/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.controller.login;

import com.muhfai.uploadtest.core.dosen.DosenBean;
import com.muhfai.uploadtest.service.dosen.DosenService;
import com.muhfai.uploadtest.service.dosen.DosenServiceInput;
import com.muhfai.uploadtest.service.dosen.DosenServiceOutput;
import com.muhfai.uploadtest.service.login.LoginService;
import com.muhfai.uploadtest.service.login.LoginServiceInput;
import com.muhfai.uploadtest.service.login.LoginServiceOutput;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author MuhFai10
 */
@Controller
@RequestMapping(value = "/dosen")
public class DosenController {

    private static final Logger logger = LoggerFactory
            .getLogger(DosenController.class);
    @Autowired
    private DosenService dosenService;
    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/listDosen", method = RequestMethod.GET)
    public String listDosen(Model model, @RequestParam(required = false) Integer page, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        DosenServiceOutput dosenOutput = dosenService.findAllDosen();
        List<DosenBean> listDosen = dosenOutput.getListDosenBean();
        PagedListHolder<DosenBean> pageListHolder = new PagedListHolder<DosenBean>(listDosen);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }
        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listDosen", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listDosen", pageListHolder.getPageList());
        }
        //model.addAttribute("listDosen", dosenOutput.getListDosenBean());
        return "listDosen";
    }

    @RequestMapping(value = "/searchDosen")
    public String searchDosen(Model model, HttpSession session, @RequestParam("freeText") String freeText, @RequestParam(required = false) Integer page) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        DosenServiceInput dosenInput = new DosenServiceInput();
        dosenInput.setFreeText(freeText);
        DosenServiceOutput dosenOutput = dosenService.findDosen(dosenInput);
        List<DosenBean> listDosen = dosenOutput.getListDosenBean();
        PagedListHolder<DosenBean> pageListHolder = new PagedListHolder<DosenBean>(listDosen);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }
        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listDosen", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listDosen", pageListHolder.getPageList());
        }
        //model.addAttribute("listDosen", dosenOutput.getListDosenBean());
        return "listDosen";
    }

    @RequestMapping(value = "/newDosen", method = RequestMethod.GET)
    public String newDosen(Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        DosenBean dosen = new DosenBean();
        model.addAttribute("dosen", dosen);
        return "newDosen";
    }

    @RequestMapping(value = "/editDosen/{kodeDosen}", method = RequestMethod.GET)
    public String editDosen(@PathVariable("kodeDosen") String kodeDosen,
            Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        DosenServiceInput input = new DosenServiceInput();
        input.setKodeDosen(kodeDosen);
        DosenServiceOutput output = dosenService.findDosenById(input);
        model.addAttribute("dosen", output.getDosenBean());
        return "editDosen";
    }

    @RequestMapping(value = "/updateDosen", method = RequestMethod.POST)
    public String updateDosen(@ModelAttribute("dosen") @Valid DosenBean dosenBean, BindingResult result,
            RedirectAttributes ra, HttpSession session, Model model) {

        DosenServiceInput input = new DosenServiceInput();
        input.setDosenBean(dosenBean);
        if (result.hasErrors()) {
            String username = (String) session.getAttribute("username");
            LoginServiceInput loginInput = new LoginServiceInput();
            loginInput.setUsername(username);
            LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
            model.addAttribute("username", loginOutput.getUserBean().getUsername());
            model.addAttribute("nama", loginOutput.getUserBean().getNama());
            return "editDosen";
        }
        DosenServiceOutput output = dosenService.updateDosen(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/dosen/listDosen";
    }

    @RequestMapping(value = "/deleteDosen/{kodeDosen}", method = RequestMethod.GET)
    public String deleteDosen(@PathVariable("kodeDosen") String kodeDosen,
            RedirectAttributes ra, HttpSession session,Model model) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        DosenServiceInput input = new DosenServiceInput();
        input.setKodeDosen(kodeDosen);
        DosenServiceOutput output = dosenService.deleteDosenById(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/dosen/listDosen";
    }

    @RequestMapping(value = "/insertDosen", method = RequestMethod.POST)
    public String insertDosen(@ModelAttribute("dosen") @Valid DosenBean dosenBean, BindingResult result,
            RedirectAttributes ra,HttpSession session,Model model) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        DosenServiceInput input = new DosenServiceInput();
        input.setDosenBean(dosenBean);
        if (result.hasErrors()) {
            
            return "newDosen";
        }
        DosenServiceOutput output = dosenService.insertDosen(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/dosen/listDosen";
    }
}
