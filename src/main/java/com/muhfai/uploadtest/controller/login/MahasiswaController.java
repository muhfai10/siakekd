/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.controller.login;

import com.muhfai.uploadtest.core.kelas.KelasBean;
import com.muhfai.uploadtest.core.mahasiswa.MahasiswaBean;
import com.muhfai.uploadtest.core.user.UserBean;
import com.muhfai.uploadtest.service.kelas.KelasService;
import com.muhfai.uploadtest.service.kelas.KelasServiceInput;
import com.muhfai.uploadtest.service.kelas.KelasServiceOutput;
import com.muhfai.uploadtest.service.login.LoginService;
import com.muhfai.uploadtest.service.login.LoginServiceInput;
import com.muhfai.uploadtest.service.login.LoginServiceOutput;
import com.muhfai.uploadtest.service.mahasiswa.MahasiswaList;
import com.muhfai.uploadtest.service.mahasiswa.MahasiswaService;
import com.muhfai.uploadtest.service.mahasiswa.MahasiswaServiceInput;
import com.muhfai.uploadtest.service.mahasiswa.MahasiswaServiceOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author MuhFai10
 */
@Controller
@RequestMapping(value = "/mahasiswa")
public class MahasiswaController {

    private static final Logger logger = LoggerFactory
            .getLogger(MahasiswaController.class);
    @Autowired
    private MahasiswaService mahasiswaService;
    @Autowired
    private KelasService kelasService;
    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/listMahasiswa", method = RequestMethod.GET)
    public String listMahasiswa(Model model, @RequestParam(required = false) Integer page, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        MahasiswaServiceOutput mahasiswaOutput = mahasiswaService.findAllMahasiswa();
        List<MahasiswaBean> listMhsBean = mahasiswaOutput.getListMahasiswaBean();
        List<MahasiswaList> listMahasiswa = new ArrayList<MahasiswaList>();
        for (MahasiswaBean mahasiswa : listMhsBean) {
            MahasiswaList mhs = new MahasiswaList();
            mhs.setNoBp(mahasiswa.getNoBp());
            mhs.setNama(mahasiswa.getNama());
            KelasServiceInput kelasInput = new KelasServiceInput();
            kelasInput.setIdKelas(mahasiswa.getKelas());
            KelasServiceOutput kelasOutput = kelasService.findKelasById(kelasInput);
            mhs.setKelas(kelasOutput.getKelasBean().getKode());
            mhs.setLokal(mahasiswa.getLokal());
            mhs.setStatus(mahasiswa.getStatus());
            listMahasiswa.add(mhs);
        }
        PagedListHolder<MahasiswaList> pageListHolder = new PagedListHolder<MahasiswaList>(listMahasiswa);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }
        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listMahasiswa", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listMahasiswa", pageListHolder.getPageList());
        }

        //model.addAttribute("listMahasiswa", mahasiswaOutput.getListMahasiswaBean());
        return "listMahasiswa";
    }

    @RequestMapping(value = "/getExcel", method = RequestMethod.GET)
    public void getExcel(HttpServletResponse response) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        sheet.autoSizeColumn(1000000);
        // create style for header cells        
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style.setFont(font);
        XSSFRow header = sheet.createRow(0);
        header.createCell(0).setCellValue("No");
        header.getCell(0).setCellStyle(style);      
        header.createCell(1).setCellValue("Nama");
        header.getCell(1).setCellStyle(style);      
        header.createCell(2).setCellValue("Jenis Kelamin");
        header.getCell(2).setCellStyle(style);      
        header.createCell(3).setCellValue("No BP");
        header.getCell(3).setCellStyle(style);      
        header.createCell(4).setCellValue("Kelas");
        header.getCell(4).setCellStyle(style);      
        header.createCell(5).setCellValue("Status");
        header.getCell(5).setCellStyle(style);      
        response.addHeader("Content-Disposition", "attachment; filename=mahasiswa.xlsx");
        try {
            workbook.write(response.getOutputStream());
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(MahasiswaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @RequestMapping(value = "/getImport", method = RequestMethod.GET)
    public String getImport(HttpServletRequest request, Model model) {
        String username = (String) request.getSession().getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        Map<Integer, String> mapRole = new HashMap<Integer, String>();
        mapRole.put(loginOutput.getUserBean().getRole(), "Administrator");
        model.addAttribute("role", mapRole);
        return "uploadExcel";
    }

    @RequestMapping(value = "/searchMahasiswa")
    public String searchMahasiswa(Model model, @RequestParam("freeText") String freeText, @RequestParam(required = false) Integer page, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        MahasiswaServiceInput mhsInput = new MahasiswaServiceInput();
        mhsInput.setFreeText(freeText);
        MahasiswaServiceOutput mahasiswaOutput = mahasiswaService.findMahasiswa(mhsInput);
        List<MahasiswaBean> listMhsBean = mahasiswaOutput.getListMahasiswaBean();
        List<MahasiswaList> listMahasiswa = new ArrayList<MahasiswaList>();
        for (MahasiswaBean mahasiswa : listMhsBean) {
            MahasiswaList mhs = new MahasiswaList();
            mhs.setNoBp(mahasiswa.getNoBp());
            mhs.setNama(mahasiswa.getNama());
            KelasServiceInput kelasInput = new KelasServiceInput();
            kelasInput.setIdKelas(mahasiswa.getKelas());
            KelasServiceOutput kelasOutput = kelasService.findKelasById(kelasInput);
            mhs.setKelas(kelasOutput.getKelasBean().getKode());
            mhs.setLokal(mahasiswa.getLokal());
            mhs.setStatus(mahasiswa.getStatus());
            listMahasiswa.add(mhs);
        }
        PagedListHolder<MahasiswaList> pageListHolder = new PagedListHolder<MahasiswaList>(listMahasiswa);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }
        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listMahasiswa", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listMahasiswa", pageListHolder.getPageList());
        }
        //model.addAttribute("listDosen", dosenOutput.getListDosenBean());
        return "listMahasiswa";
    }

    @RequestMapping(value = "/newMahasiswa", method = RequestMethod.GET)
    public String newMahasiswa(Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        MahasiswaBean mahasiswa = new MahasiswaBean();
        KelasServiceOutput kelasOutput = kelasService.findAllKelas();
        List<KelasBean> listKelas = kelasOutput.getListKelasBean();
        Map<Integer, String> mapKelas = new HashMap<Integer, String>();
        for (KelasBean kelas : listKelas) {
            mapKelas.put(kelas.getIdKelas(), kelas.getKode());
        }
        model.addAttribute("mapKelas", mapKelas);
        model.addAttribute("mahasiswa", mahasiswa);
        return "newMahasiswa";
    }

    @RequestMapping(value = "/insertMahasiswa", method = RequestMethod.POST)
    public String insertMahasiswa(@ModelAttribute("mahasiswa") @Valid MahasiswaBean mahasiswaBean, BindingResult result,
            RedirectAttributes ra, Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        MahasiswaServiceInput input = new MahasiswaServiceInput();
        input.setMahasiswaBean(mahasiswaBean);
        if (result.hasErrors()) {
            KelasServiceOutput kelasOutput = kelasService.findAllKelas();
            List<KelasBean> listKelas = kelasOutput.getListKelasBean();
            Map<Integer, String> mapKelas = new HashMap<Integer, String>();
            for (KelasBean kelas : listKelas) {
                mapKelas.put(kelas.getIdKelas(), kelas.getKode());
            }
            model.addAttribute("mapKelas", mapKelas);
            return "newMahasiswa";
        }
        UserBean userBean = new UserBean();
        userBean.setNama(mahasiswaBean.getNama());
        userBean.setUsername(mahasiswaBean.getNoBp());
        userBean.setPassword("12345");
        userBean.setRole(2);
        LoginServiceInput userInput = new LoginServiceInput();
        MahasiswaServiceOutput output = mahasiswaService.insertMahasiswa(input);
        userInput.setUserBean(userBean);
        loginService.insertUser(userInput);

        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/mahasiswa/listMahasiswa";
    }

    @RequestMapping(value = "/deleteMahasiswa/{noBp}", method = RequestMethod.GET)
    public String deleteMahasiswa(@PathVariable("noBp") String noBp,
            RedirectAttributes ra, HttpSession session, Model model) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        MahasiswaServiceInput input = new MahasiswaServiceInput();
        input.setNoBp(noBp);
        MahasiswaServiceOutput output = mahasiswaService.deleteMahasiswaById(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/mahasiswa/listMahasiswa";
    }

    @RequestMapping(value = "/editMahasiswa/{noBp}", method = RequestMethod.GET)
    public String editMahasiswa(@PathVariable("noBp") String noBp,
            Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        MahasiswaServiceInput input = new MahasiswaServiceInput();
        input.setNoBp(noBp);
        KelasServiceOutput kelasOutput = kelasService.findAllKelas();
        List<KelasBean> listKelas = kelasOutput.getListKelasBean();
        Map<Integer, String> mapKelas = new HashMap<Integer, String>();
        for (KelasBean kelas : listKelas) {
            mapKelas.put(kelas.getIdKelas(), kelas.getKode());
        }
        model.addAttribute("mapKelas", mapKelas);
        MahasiswaServiceOutput output = mahasiswaService.findMahasiswaById(input);
        model.addAttribute("mahasiswa", output.getMahasiswaBean());
        return "editMahasiswa";
    }

    @RequestMapping(value = "/updateMahasiswa", method = RequestMethod.POST)
    public String updateMahasiswa(@ModelAttribute("mahasiswa") @Valid MahasiswaBean mahasiswaBean, BindingResult result,
            RedirectAttributes ra, Model model) {
        MahasiswaServiceInput input = new MahasiswaServiceInput();
        input.setMahasiswaBean(mahasiswaBean);
        if (result.hasErrors()) {
            KelasServiceOutput kelasOutput = kelasService.findAllKelas();
            List<KelasBean> listKelas = kelasOutput.getListKelasBean();
            Map<Integer, String> mapKelas = new HashMap<Integer, String>();
            for (KelasBean kelas : listKelas) {
                mapKelas.put(kelas.getIdKelas(), kelas.getKode());
            }
            model.addAttribute("mapKelas", mapKelas);
            return "editMahasiswa";
        }
        MahasiswaServiceOutput output = mahasiswaService.updateMahasiswa(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/mahasiswa/listMahasiswa";
    }

}
