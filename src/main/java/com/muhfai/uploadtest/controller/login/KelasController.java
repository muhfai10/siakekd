/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.controller.login;

import com.muhfai.uploadtest.core.kelas.KelasBean;
import com.muhfai.uploadtest.core.prodi.ProdiBean;
import com.muhfai.uploadtest.service.kelas.KelasList;
import com.muhfai.uploadtest.service.kelas.KelasService;
import com.muhfai.uploadtest.service.kelas.KelasServiceInput;
import com.muhfai.uploadtest.service.kelas.KelasServiceOutput;
import com.muhfai.uploadtest.service.login.LoginService;
import com.muhfai.uploadtest.service.login.LoginServiceInput;
import com.muhfai.uploadtest.service.login.LoginServiceOutput;
import com.muhfai.uploadtest.service.prodi.ProdiService;
import com.muhfai.uploadtest.service.prodi.ProdiServiceInput;
import com.muhfai.uploadtest.service.prodi.ProdiServiceOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author MuhFai10
 */
@Controller
@RequestMapping(value = "/kelas")
public class KelasController {

    private static final Logger logger = LoggerFactory
            .getLogger(KelasController.class);
    @Autowired
    private KelasService kelasService;
    @Autowired
    private ProdiService prodiService;
    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/listKelas", method = RequestMethod.GET)
    public String listKelas(Model model, HttpSession session, @RequestParam(required = false) Integer page) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        KelasServiceOutput kelasOutput = kelasService.findAllKelas();
        List<KelasBean> listKlsBean = kelasOutput.getListKelasBean();
        List<KelasList> listKelas = new ArrayList<KelasList>();
        for (KelasBean kelas : listKlsBean) {
            KelasList kls = new KelasList();
            kls.setIdKelas(kelas.getIdKelas());
            kls.setKelas(kelas.getKelas());
            ProdiServiceInput prodiInput = new ProdiServiceInput();
            prodiInput.setIdProdi(kelas.getProdi());
            ProdiServiceOutput prodiOutput = prodiService.findProdiById(prodiInput);
            kls.setProdi(prodiOutput.getProdiBean().getProdi());
            kls.setKode(prodiOutput.getProdiBean().getKode());
            listKelas.add(kls);
        }

        

        PagedListHolder<KelasList> pageListHolder = new PagedListHolder<KelasList>(listKelas);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }

        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listKelas", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listKelas", pageListHolder.getPageList());
        }
        //model.addAttribute("listPertanyaan", kuesionerOutput.getListKuesionerBean());
        return "listKelas";
    }
    @RequestMapping(value = "/newKelas", method = RequestMethod.GET)
    public String newKelas(Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        KelasBean kelas = new KelasBean();
        ProdiServiceOutput prodiOutput = prodiService.findAllProdi();
        List<ProdiBean> listProdi = prodiOutput.getListProdiBean();
        Map<Integer, String> mapProdi = new HashMap<Integer, String>();
        for (ProdiBean prodi : listProdi) {
            mapProdi.put(prodi.getIdProdi(), prodi.getProdi());
        }
        model.addAttribute("mapProdi", mapProdi);
        model.addAttribute("kelas", kelas);
        return "newKelas";
    }

    @RequestMapping(value = "/editKelas/{idKelas}", method = RequestMethod.GET)
    public String editKelas(@PathVariable("idKelas") Integer idKelas,
            Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        KelasServiceInput input = new KelasServiceInput();
        input.setIdKelas(idKelas);
        ProdiServiceOutput prodiOutput = prodiService.findAllProdi();
        List<ProdiBean> listProdi = prodiOutput.getListProdiBean();
        Map<Integer, String> mapProdi = new HashMap<Integer, String>();
        for (ProdiBean prodi : listProdi) {
            mapProdi.put(prodi.getIdProdi(), prodi.getProdi());
        }
        model.addAttribute("mapProdi", mapProdi);
        KelasServiceOutput output = kelasService.findKelasById(input);
        model.addAttribute("kelas", output.getKelasBean());
        return "newKelas";
    }

    @RequestMapping(value = "/deleteKelas/{idKelas}", method = RequestMethod.GET)
    public String deleteKelas(@PathVariable("idKelas") Integer idKelas,
            RedirectAttributes ra, HttpSession session, Model model) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        KelasServiceInput input = new KelasServiceInput();
        input.setIdKelas(idKelas);
        KelasServiceOutput output = kelasService.deleteKelasById(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/kelas/listKelas";
    }

    @RequestMapping(value = "/insertKelas", method = RequestMethod.POST)
    public String insertKelas(@ModelAttribute("kelas") @Valid KelasBean kelasBean,
            BindingResult result,
            RedirectAttributes ra, Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        KelasServiceInput input = new KelasServiceInput();
        input.setKelasBean(kelasBean);
        if (result.hasErrors()) {
            ProdiServiceOutput prodiOutput = prodiService.findAllProdi();
            List<ProdiBean> listProdi = prodiOutput.getListProdiBean();
            Map<Integer, String> mapProdi = new HashMap<Integer, String>();
            for (ProdiBean prodi : listProdi) {
                mapProdi.put(prodi.getIdProdi(), prodi.getProdi());
            }
            model.addAttribute("mapProdi", mapProdi);
            return "newProdi";
        }
        KelasServiceOutput output = kelasService.insertKelas(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/kelas/listKelas";
    }
}
