/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.controller.login;

import com.muhfai.uploadtest.service.surveydetail.SurveyForm;
import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.core.kuesioner.KuesionerBean;
import com.muhfai.uploadtest.core.login.LoginBean;
import com.muhfai.uploadtest.core.mahasiswa.MahasiswaBean;
import com.muhfai.uploadtest.core.perkuliahan.PerkuliahanBean;
import com.muhfai.uploadtest.core.perkuliahan.TampilKuliahBean;
import com.muhfai.uploadtest.core.survey.SurveyBean;
import com.muhfai.uploadtest.core.surveydetail.SurveyDetailBean;
import com.muhfai.uploadtest.core.surveydetail.TampilSurvey;
import com.muhfai.uploadtest.core.user.UserBean;
import com.muhfai.uploadtest.service.dosen.DosenService;
import com.muhfai.uploadtest.service.dosen.DosenServiceInput;
import com.muhfai.uploadtest.service.dosen.DosenServiceOutput;
import com.muhfai.uploadtest.service.kelas.KelasService;
import com.muhfai.uploadtest.service.kelas.KelasServiceInput;
import com.muhfai.uploadtest.service.kelas.KelasServiceOutput;
import com.muhfai.uploadtest.service.kuesioner.KuesionerService;
import com.muhfai.uploadtest.service.kuesioner.KuesionerServiceOutput;
import com.muhfai.uploadtest.service.login.BuktiList;
import com.muhfai.uploadtest.service.login.ChangeForm;
import com.muhfai.uploadtest.service.login.LoginService;
import com.muhfai.uploadtest.service.login.LoginServiceInput;
import com.muhfai.uploadtest.service.login.LoginServiceOutput;
import com.muhfai.uploadtest.service.mahasiswa.MahasiswaService;
import com.muhfai.uploadtest.service.mahasiswa.MahasiswaServiceInput;
import com.muhfai.uploadtest.service.mahasiswa.MahasiswaServiceOutput;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahService;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahServiceInput;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahServiceOutput;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanService;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanServiceInput;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanServiceOutput;
import com.muhfai.uploadtest.service.prodi.ProdiService;
import com.muhfai.uploadtest.service.survey.SurveyService;
import com.muhfai.uploadtest.service.survey.SurveyServiceInput;
import com.muhfai.uploadtest.service.survey.SurveyServiceOutput;
import com.muhfai.uploadtest.service.surveydetail.SurveyDetailService;
import com.muhfai.uploadtest.service.surveydetail.SurveyDetailServiceInput;
import com.muhfai.uploadtest.service.surveydetail.SurveyDetailServiceOutput;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author MuhFai10
 */
@Controller
@RequestMapping(value = "/login")
public class LoginController {

    private static final Logger logger = LoggerFactory
            .getLogger(LoginController.class);
    @Autowired
    private LoginService loginService;
    @Autowired
    private MahasiswaService mahasiswaService;
    @Autowired
    private ProdiService prodiService;
    @Autowired
    private PerkuliahanService perkuliahanService;
    @Autowired
    private MatakuliahService matkulService;
    @Autowired
    private DosenService dosenService;
    @Autowired
    private KuesionerService kuesionerService;
    @Autowired
    private SurveyService surveyService;
    @Autowired
    private SurveyDetailService svDetailService;
    @Autowired
    private KelasService kelasService;
    MahasiswaBean mhsBean = new MahasiswaBean();

    @RequestMapping(value = "/loginAccess", method = RequestMethod.POST)
    public String login(@RequestParam("username") String username,
            @RequestParam("password") String password, HttpServletRequest req, Model model) {
        LoginServiceInput request = new LoginServiceInput();

        request.setUsername(username);
        request.setPassword(password);

        try {
            LoginServiceOutput response = loginService.getLogin(request);
            LoginBean userBean = response.getLoginBean();
            if (response.getLoginBean() == null) {
                model.addAttribute("message", response.getMessage());
                return "newLogin";
            } else {
                if (response.getLoginBean().getRole() == 1) {
                    addUserSession(userBean, req);
                    return "redirect:/dashboard/adminDashboard";
                } else if (response.getLoginBean().getRole() == 2) {
                    addUserSession(userBean, req);
                    return "redirect:/login/successLoginMahasiswa";
                } else if (response.getLoginBean().getRole() == 3) {
                    addUserSession(userBean, req);
                    return "redirect:/dashboard/kajurDashboard";
                } else {
                    model.addAttribute("message", response.getMessage());
                    return "newLogin";
                }
            }
        } catch (Exception ex) {
            model.addAttribute("message", ex.getMessage());
            return "newLogin";
        }

    }

    private void addUserSession(LoginBean userBean, HttpServletRequest request) {
        request.getSession().setAttribute("user", userBean);
        request.getSession().setAttribute("username", userBean.getUsername());
        request.getSession().setAttribute("role", userBean.getRole());
    }

    @RequestMapping(value = "/successLoginMahasiswa", method = RequestMethod.GET)
    public String successLogin(Model model, HttpSession session) {
        showDataMahasiswa(model, session);
        showDataPerkuliahan(model);
        session.getAttribute("username");
        return "successLoginMahasiswa";
    }

    private void showDataMahasiswa(Model model, HttpSession session) {
        MahasiswaServiceInput mhsRequest = new MahasiswaServiceInput();
        String noBp = (String) session.getAttribute("username");
        mhsRequest.setNoBp(noBp);
        MahasiswaServiceOutput mhsResponse = mahasiswaService.findMahasiswaById(mhsRequest);
        mhsBean = mhsResponse.getMahasiswaBean();
        //ProdiServiceInput prodiRequest = new ProdiServiceInput();
        //prodiRequest.setIdProdi(mhsBean.getProdi());
        //ProdiServiceOutput prodiResponse = prodiService.findProdiById(prodiRequest);
        KelasServiceInput kelasInput = new KelasServiceInput();
        kelasInput.setIdKelas(mhsBean.getKelas());
        KelasServiceOutput kelasOutput = kelasService.findKelasById(kelasInput);

        model.addAttribute("noBp", mhsBean.getNoBp());
        model.addAttribute("nama", mhsBean.getNama());
        model.addAttribute("kelas", kelasOutput.getKelasBean().getKode());
        model.addAttribute("lokal", mhsBean.getLokal());
        //modelMap.addAttribute("prodi", prodiResponse.getProdiBean().getProdi());

    }

    private String getReport(List<BuktiList> listBukti, HttpSession session) {
        DateFormat df = new SimpleDateFormat("dd MMMMM yyyy");
        String path = "D:\\\\temp\\\\";
        String fileName = "bukti" + String.valueOf(df.format(new Date())) + ".pdf";
        MahasiswaServiceInput mhsInput = new MahasiswaServiceInput();
        String noBp = (String) session.getAttribute("username");
        mhsInput.setNoBp(noBp);

        try {
            InputStream is = getClass().getResourceAsStream("/jasper/buktiReport.jasper");
            //JasperReport jasperReport = (JasperReport)JRLoader.loadObject(is);
            JRDataSource ds = new JRBeanCollectionDataSource(listBukti);
            MahasiswaServiceOutput mhsOutput = mahasiswaService.getMahasiswa(mhsInput);
            Map<String, Object> parameters = new HashMap();
            parameters.put("ds", ds);
            parameters.put("noBp", mhsOutput.getNewMhsBean().getNoBp());
            parameters.put("nama", mhsOutput.getNewMhsBean().getNama());
            parameters.put("kelas", mhsOutput.getNewMhsBean().getLokal());
            parameters.put("semester", mhsOutput.getNewMhsBean().getSemester());
            parameters.put("tahunAkademik", mhsOutput.getNewMhsBean().getTahunAjaran());
            JasperPrint jasperPrint = JasperFillManager.fillReport(is, parameters, new JREmptyDataSource());
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + fileName);
        } catch (Exception ex) {
            logger.error("Penilaian Controller", ex);
        }
        return path + fileName;
    }

    @RequestMapping(value = "/downloadPdf", method = RequestMethod.GET)
    public void downloadPDF(HttpServletResponse response, HttpSession session) {
        PerkuliahanServiceOutput perkuliahanOutput = perkuliahanService.findAllPerkuliahan2();
        List<PerkuliahanBean> listPerkuliahan = perkuliahanOutput.getListPerkuliahanBean();
        List<BuktiList> listBukti = getBuktiList();
        String fileDir = getReport(listBukti, session);
        String[] fileSplit = fileDir.split("\\\\");
        String fileName = fileSplit[4];
        File file = new File(fileDir);
        file.getParentFile().mkdirs();
        try {
            InputStream is = new FileInputStream(file);
            response.setContentType("application/force-download");
            response.setHeader("Pragma", "public");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Content-type", "application-download");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            ServletOutputStream os = response.getOutputStream();
            byte[] bufferData = new byte[4096];
            int read = 0;
            while ((read = is.read(bufferData)) != -1) {
                os.write(bufferData, 0, read);
            }
            os.flush();
            os.close();
            is.close();
            file.delete();
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(PenilaianController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(PenilaianController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private List<BuktiList> getBuktiList() {
        PerkuliahanServiceInput perkuliahanRequest = new PerkuliahanServiceInput();
        perkuliahanRequest.setKelas((mhsBean.getKelas()));
        PerkuliahanServiceOutput perkuliahanResponse = perkuliahanService.findAllPerkuliahan(perkuliahanRequest);
        List<PerkuliahanBean> listPerkuliahan = perkuliahanResponse.getListPerkuliahanBean();
        List<BuktiList> listBukti = new ArrayList<BuktiList>();
        Integer i = 1;
        for (PerkuliahanBean perkuliahan : listPerkuliahan) {
            BuktiList bukti = new BuktiList();
            bukti.setNo(i);
            bukti.setKodeMk(perkuliahan.getKodeMk());
            MatakuliahServiceInput matkulRequest = new MatakuliahServiceInput();
            matkulRequest.setKodeMk(perkuliahan.getKodeMk());
            MatakuliahServiceOutput matkulResponse = matkulService.findMatkulById(matkulRequest);
            bukti.setMatakuliah(matkulResponse.getMatakuliahBean().getMataKuliah());
            //GET DOSEN 1
            DosenServiceInput dosenRequest = new DosenServiceInput();
            dosenRequest.setKodeDosen(perkuliahan.getKodeDosen());
            DosenServiceOutput dosenResponse = dosenService.findDosenById(dosenRequest);

            //GET DOSEN 2
            dosenRequest.setKodeDosen(perkuliahan.getKodeDosen2());
            DosenServiceOutput dosenResponse2 = dosenService.findDosenById(dosenRequest);

            //Validasi Dosen Tersedia
            SurveyServiceInput surveyInput = new SurveyServiceInput();
            surveyInput.setNoBp(mhsBean.getNoBp());
            surveyInput.setKodeDosen(perkuliahan.getKodeDosen());
            surveyInput.setIdPerkuliahan(perkuliahan.getIdPerkuliahan());
            SurveyServiceOutput surveyOutput = surveyService.findSurveyByMhsDosen(surveyInput);
            bukti.setNamaDosen(dosenResponse.getDosenBean().getNama());

            if (surveyOutput.getSurveyBean().getKodeDosen() == null) {

                bukti.setStatusDosen("Belum Diisi");
            } else {

                bukti.setStatusDosen("Telah Diisi");
            }
            //Validasi Dosen 2
            surveyInput.setNoBp(mhsBean.getNoBp());
            surveyInput.setKodeDosen(perkuliahan.getKodeDosen2());
            surveyInput.setIdPerkuliahan(perkuliahan.getIdPerkuliahan());
            SurveyServiceOutput surveyOutput2 = surveyService.findSurveyByMhsDosen(surveyInput);
            if (perkuliahan.getKodeDosen2() == null) {
                bukti.setNamaDosen2("");
                bukti.setStatusDosen2("");
            } else {
                if (surveyOutput2.getSurveyBean().getKodeDosen() == null) {
                    bukti.setStatusDosen2("Belum Diisi");
                } else {
                    bukti.setStatusDosen2("Telah Diisi");
                }
                bukti.setNamaDosen2(dosenResponse2.getDosenBean().getNama());
            }

            i++;
            listBukti.add(bukti);
        }
        return listBukti;
    }

    @RequestMapping(value = "/editPassword/{noBp}", method = RequestMethod.GET)
    public String editPassword(@PathVariable("noBp") String noBp,
            Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        ChangeForm changeForm = new ChangeForm();
        LoginServiceInput userInput = new LoginServiceInput();
        userInput.setUsername(username);
        LoginServiceOutput userOutput = loginService.findByUser(userInput);
        changeForm.setIdUser(userOutput.getUserBean().getIdUser());
        changeForm.setUsername(userOutput.getUserBean().getUsername());
        changeForm.setNama(userOutput.getUserBean().getNama());
        changeForm.setRole(userOutput.getUserBean().getRole());
        changeForm.setPassword("");
        changeForm.setKonfirmPassword("");
        changeForm.setNewPassword("");
        model.addAttribute("changeForm",changeForm);
        return "changePassword";
    }
    
    @RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
    public String updatePassword(@ModelAttribute("changeForm") @Valid ChangeForm changeForm,
            BindingResult result,
            RedirectAttributes ra,Model model,HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        if (result.hasErrors()) {
            return "changePassword";
        }
        if(!changeForm.getPassword().equals(changeForm.getKonfirmPassword())){
            model.addAttribute("message", "Konfirmasi Password Berbeda!");
            return "changePassword";
        }
        UserBean userBean = new UserBean();
        userBean.setIdUser(changeForm.getIdUser());
        userBean.setUsername(changeForm.getUsername());
        userBean.setNama(changeForm.getNama());
        userBean.setPassword(changeForm.getNewPassword());
        userBean.setRole(changeForm.getRole());
        loginInput.setUserBean(userBean);
        LoginServiceOutput userOutput = loginService.insertUser(loginInput);
        return "redirect:/login/toLogout";
    }
    
    private void showDataPerkuliahan(Model model) {
        PerkuliahanServiceInput perkuliahanRequest = new PerkuliahanServiceInput();
        perkuliahanRequest.setKelas((mhsBean.getKelas()));
        //perkuliahanRequest.setProdi(mhsBean.getProdi());
        //perkuliahanRequest.setSemester(mhsBean.getSemester());
        PerkuliahanServiceOutput perkuliahanResponse = perkuliahanService.findAllPerkuliahan(perkuliahanRequest);
        List<PerkuliahanBean> listPerkuliahan = perkuliahanResponse.getListPerkuliahanBean();
        List<TampilKuliahBean> kuliahList = new ArrayList<TampilKuliahBean>();
        for (PerkuliahanBean perkuliahan : listPerkuliahan) {
            TampilKuliahBean tampilKuliah = new TampilKuliahBean();
            tampilKuliah.setIdPerkuliahan(perkuliahan.getIdPerkuliahan());
            tampilKuliah.setKodeMatkul(perkuliahan.getKodeMk());
            tampilKuliah.setKodeDosen(perkuliahan.getKodeDosen());
            tampilKuliah.setKodeDosen2(perkuliahan.getKodeDosen2());
            MatakuliahServiceInput matkulRequest = new MatakuliahServiceInput();
            matkulRequest.setKodeMk(perkuliahan.getKodeMk());
            MatakuliahServiceOutput matkulResponse = matkulService.findMatkulById(matkulRequest);
            tampilKuliah.setMataKuliah(matkulResponse.getMatakuliahBean().getMataKuliah());
            //matakuliah.add(matkulResponse.getMatakuliahBean());
            //Validasi Dosen Tersedia
            SurveyServiceInput surveyInput = new SurveyServiceInput();
            surveyInput.setNoBp(mhsBean.getNoBp());
            surveyInput.setKodeDosen(perkuliahan.getKodeDosen());
            surveyInput.setIdPerkuliahan(perkuliahan.getIdPerkuliahan());
            SurveyServiceOutput surveyOutput = surveyService.findSurveyByMhsDosen(surveyInput);

            DosenServiceInput dosenRequest = new DosenServiceInput();
            if (surveyOutput.getSurveyBean().getKodeDosen() == null) {
                dosenRequest.setKodeDosen(perkuliahan.getKodeDosen());
                DosenServiceOutput dosenResponse = dosenService.findDosenById(dosenRequest);
                tampilKuliah.setNamaDosen(dosenResponse.getDosenBean().getNama());

            } else {
                surveyOutput.setMessage("Telah diisi");
                tampilKuliah.setNamaDosen(surveyOutput.getMessage());

            }
            //TampilDosen 2
            surveyInput.setNoBp(mhsBean.getNoBp());
            surveyInput.setKodeDosen(perkuliahan.getKodeDosen2());
            surveyInput.setIdPerkuliahan(perkuliahan.getIdPerkuliahan());
            SurveyServiceOutput surveyOutput2 = surveyService.findSurveyByMhsDosen(surveyInput);
            if (surveyOutput2.getSurveyBean().getKodeDosen() == null) {
                dosenRequest.setKodeDosen(perkuliahan.getKodeDosen2());
                DosenServiceOutput dosenResponse2 = dosenService.findDosenById(dosenRequest);
                tampilKuliah.setNamaDosen2(dosenResponse2.getDosenBean().getNama());

            } else {
                surveyOutput.setMessage("Telah diisi");
                if (perkuliahan.getKodeDosen2() == null) {
                    tampilKuliah.setNamaDosen2("");
                }
                tampilKuliah.setNamaDosen2(surveyOutput.getMessage());
            }

            //dosen.add(dosenResponse.getDosenBean());
            kuliahList.add(tampilKuliah);
        }
        model.addAttribute("kuliahList", kuliahList);

    }

    
    @RequestMapping(value = "/failedLogin", method = RequestMethod.GET)
    public String failedLogin() {
        return "redirect:/";
    }

    @RequestMapping(value = "/toLogout", method = RequestMethod.GET)
    public String toLogout(HttpSession session, SessionStatus sessionStatus) {
        session.removeAttribute("user");
        session.invalidate();
        sessionStatus.setComplete();
        return "redirect:/";
    }

    @RequestMapping(value = "{noBp}/toKuesioner/{kodeDosen}/{kodeMatkul}/{idPerkuliahan}", method = RequestMethod.GET)
    public String toKuesioner(@PathVariable String noBp, @PathVariable String kodeDosen,
            @PathVariable Integer idPerkuliahan) {

        return "redirect:/login/getKuesioner/{kodeDosen}/{kodeMatkul}/{idPerkuliahan}";
    }

    @RequestMapping(value = "getKuesioner/{kodeDosen}/{kodeMatkul}/{idPerkuliahan}", method = RequestMethod.GET)
    public String getKuesioner(HttpSession session, Model model, @PathVariable String kodeDosen,
            @PathVariable String kodeMatkul, @PathVariable Integer idPerkuliahan) {
        session.getAttribute("username");
        showDataMahasiswa(model, session);
        //Get Data Dosen
        DosenServiceInput dosenRequest = new DosenServiceInput();
        dosenRequest.setKodeDosen(kodeDosen);
        DosenServiceOutput dosenResponse = dosenService.findDosenById(dosenRequest);
        // Get Data Matakuliah
        MatakuliahServiceInput matkulRequest = new MatakuliahServiceInput();
        matkulRequest.setKodeMk(kodeMatkul);
        MatakuliahServiceOutput matkulResponse = matkulService.findMatkulById(matkulRequest);
         // Get Data Perkuliahan
        PerkuliahanServiceInput perkInput = new PerkuliahanServiceInput();
        perkInput.setIdPerkuliahan(idPerkuliahan);
        PerkuliahanServiceOutput perkOutput = perkuliahanService.findPerkuliahanById(perkInput);
        model.addAttribute("kodeDosen", kodeDosen);
        model.addAttribute("idPerkuliahan", idPerkuliahan);
        model.addAttribute("idSemester",perkOutput.getPerkuliahanBean().getIdSemester());
        model.addAttribute("namaDosen", dosenResponse.getDosenBean().getNama());
        model.addAttribute("nidn", dosenResponse.getDosenBean().getNidn());
        model.addAttribute("nip", dosenResponse.getDosenBean().getNip());
        model.addAttribute("mataKuliah", matkulResponse.getMatakuliahBean().getMataKuliah());

        //KuesionerServiceOutput kuesionerResponse = kuesionerService.findAllKuesioner();
        //model.addAttribute("listPertanyaan", kuesionerResponse.getListKuesionerBean());
        KuesionerServiceOutput kuesionerResponse = kuesionerService.findAllKuesioner();
        List<KuesionerBean> listKuesioner = kuesionerResponse.getListKuesionerBean();
        List<TampilSurvey> listTampilSurvey = new ArrayList<TampilSurvey>();
        for (KuesionerBean kuesioner : listKuesioner) {
            TampilSurvey tampilSurvey = new TampilSurvey();
            tampilSurvey.setIdKuesioner(kuesioner.getIdKuesioner());
            tampilSurvey.setPertanyaan(kuesioner.getPertanyaan());

            listTampilSurvey.add(tampilSurvey);
        }
        SurveyForm surveyForm = new SurveyForm();
        surveyForm.setListSurvey(listTampilSurvey);
        model.addAttribute("surveyForm", surveyForm);
        return "getKuesioner";
    }

    @RequestMapping(value = "{noBp}/toKuesioner_/{kodeDosen2}/{kodeMatkul}/{idPerkuliahan}", method = RequestMethod.GET)
    public String toKuesioner_(@PathVariable String noBp, @PathVariable String kodeDosen2,
            @PathVariable Integer idPerkuliahan) {

        return "redirect:/login/getKuesioner_/{kodeDosen2}/{kodeMatkul}/{idPerkuliahan}";
    }

    @RequestMapping(value = "getKuesioner_/{kodeDosen2}/{kodeMatkul}/{idPerkuliahan}", method = RequestMethod.GET)
    public String getKuesioner_(HttpSession session, Model model, @PathVariable String kodeDosen2,
            @PathVariable String kodeMatkul, @PathVariable Integer idPerkuliahan) {
        session.getAttribute("username");
        showDataMahasiswa(model, session);
        //Get Data Dosen
        DosenServiceInput dosenRequest = new DosenServiceInput();
        dosenRequest.setKodeDosen(kodeDosen2);
        DosenServiceOutput dosenResponse = dosenService.findDosenById(dosenRequest);
        // Get Data Matakuliah
        MatakuliahServiceInput matkulRequest = new MatakuliahServiceInput();
        matkulRequest.setKodeMk(kodeMatkul);
        MatakuliahServiceOutput matkulResponse = matkulService.findMatkulById(matkulRequest);
        // Get Data Perkuliahan
        PerkuliahanServiceInput perkInput = new PerkuliahanServiceInput();
        perkInput.setIdPerkuliahan(idPerkuliahan);
        PerkuliahanServiceOutput perkOutput = perkuliahanService.findPerkuliahanById(perkInput);
        model.addAttribute("kodeDosen", kodeDosen2);
        model.addAttribute("idPerkuliahan", idPerkuliahan);
        model.addAttribute("idSemester",perkOutput.getPerkuliahanBean().getIdSemester());
        model.addAttribute("namaDosen", dosenResponse.getDosenBean().getNama());
        model.addAttribute("nidn", dosenResponse.getDosenBean().getNidn());
        model.addAttribute("nip", dosenResponse.getDosenBean().getNip());
        model.addAttribute("mataKuliah", matkulResponse.getMatakuliahBean().getMataKuliah());

        KuesionerServiceOutput kuesionerResponse = kuesionerService.findAllKuesioner();
        List<KuesionerBean> listKuesioner = kuesionerResponse.getListKuesionerBean();
        List<TampilSurvey> listTampilSurvey = new ArrayList<TampilSurvey>();
        for (KuesionerBean kuesioner : listKuesioner) {
            TampilSurvey tampilSurvey = new TampilSurvey();
            tampilSurvey.setIdKuesioner(kuesioner.getIdKuesioner());
            tampilSurvey.setPertanyaan(kuesioner.getPertanyaan());

            listTampilSurvey.add(tampilSurvey);
        }
        SurveyForm surveyForm = new SurveyForm();
        surveyForm.setListSurvey(listTampilSurvey);
        model.addAttribute("surveyForm", surveyForm);
        //model.addAttribute("listPertanyaan", kuesionerResponse.getListKuesionerBean());
        return "getKuesioner";
    }

    @RequestMapping(value = "/saveSurvey", method = RequestMethod.POST)
    public String saveSurvey(@ModelAttribute("surveyForm") SurveyForm surveyForm,
            @RequestParam("idPerkuliahan") Integer idPerkuliahan, @RequestParam("kodeDosen") String kodeDosen,@RequestParam("idSemester") Integer idSemester,
            final RedirectAttributes ra
    ) {
        SurveyBean surveyBean = new SurveyBean();
        //Cara validasi method ini agar tdk terinput tabel survey

        surveyBean.setNoBp(mhsBean.getNoBp());
        surveyBean.setIdPerkuliahan(idPerkuliahan);
        surveyBean.setKodeDosen(kodeDosen);
        surveyBean.setIdSemester(idSemester);
        SurveyServiceInput surveyInput = new SurveyServiceInput();
        surveyInput.setSurveyBean(surveyBean);
        SurveyServiceOutput surveyOutput = surveyService.insertSurvey(surveyInput);
        List<TampilSurvey> listSurvey = surveyForm.getListSurvey();
        List<SurveyDetailBean> svDetailBeanList = new ArrayList<SurveyDetailBean>();
        for (TampilSurvey survey : listSurvey) {
            SurveyDetailBean svDetailBean = new SurveyDetailBean();
            svDetailBean.setIdSurvey(surveyOutput.getIdSurvey());
            svDetailBean.setIdKuesioner(survey.getIdKuesioner());
            svDetailBean.setNilai(survey.getNilai());

            svDetailBeanList.add(svDetailBean);
        }
        SurveyDetailServiceInput svDetailInput = new SurveyDetailServiceInput();
        svDetailInput.setSvDetailBeanList(svDetailBeanList);

        SurveyDetailServiceOutput svDetailOutput = svDetailService.insertSurveyDetail(svDetailInput);
        Integer idSurvey = surveyOutput.getIdSurvey();
        ra.addFlashAttribute("message", svDetailOutput.getMessage());
        if (!svDetailOutput.getMessage().equals(Message.SURVEY_SUCCESS)) {
            surveyInput.setIdSurvey(idSurvey);
            surveyService.deleteSurveyById(surveyInput);
            ra.addFlashAttribute("message", svDetailOutput.getMessage());
            return "redirect:/login/successLoginMahasiswa";
        }
        return "redirect:/login/successLoginMahasiswa";
    }

}
