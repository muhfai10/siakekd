/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.controller.login;

import com.muhfai.uploadtest.core.perkuliahan.PerkuliahanBean;
import com.muhfai.uploadtest.core.prodiCustom.ProdiCustomBean;
import com.muhfai.uploadtest.core.semester.SemesterBean;
import com.muhfai.uploadtest.core.survey.SurveyBean;
import com.muhfai.uploadtest.service.dosen.DosenService;
import com.muhfai.uploadtest.service.dosen.DosenServiceInput;
import com.muhfai.uploadtest.service.dosen.DosenServiceOutput;
import com.muhfai.uploadtest.service.login.LoginService;
import com.muhfai.uploadtest.service.login.LoginServiceInput;
import com.muhfai.uploadtest.service.login.LoginServiceOutput;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahService;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahServiceInput;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahServiceOutput;
import com.muhfai.uploadtest.service.penilaian.PenilaianList;
import com.muhfai.uploadtest.service.penilaian.PenilaianService;
import com.muhfai.uploadtest.service.penilaian.PenilaianServiceInput;
import com.muhfai.uploadtest.service.penilaian.PenilaianServiceOutput;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanService;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanServiceInput;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanServiceOutput;
import com.muhfai.uploadtest.service.prodiCustom.ProdiCustomService;
import com.muhfai.uploadtest.service.prodiCustom.ProdiCustomServiceInput;
import com.muhfai.uploadtest.service.prodiCustom.ProdiCustomServiceOutput;
import com.muhfai.uploadtest.service.semester.SemesterService;
import com.muhfai.uploadtest.service.semester.SemesterServiceOutput;
import com.muhfai.uploadtest.service.survey.SurveyService;
import com.muhfai.uploadtest.service.survey.SurveyServiceInput;
import com.muhfai.uploadtest.service.survey.SurveyServiceOutput;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author MuhFai10
 */
@Controller
@RequestMapping(value = "/penilaian")
public class PenilaianController {

    private static final Logger logger = LoggerFactory
            .getLogger(PenilaianController.class);
    @Autowired
    PenilaianService penilaianService;
    @Autowired
    PerkuliahanService perkuliahanService;
    @Autowired
    DosenService dosenService;
    @Autowired
    MatakuliahService matkulService;
    @Autowired
    LoginService loginService;
    @Autowired
    SemesterService semesterService;
    @Autowired
    SurveyService surveyService;
    @Autowired
    ProdiCustomService prodiCustomService;

    @RequestMapping(value = "/listPenilaian/{idProdi}", method = RequestMethod.GET)
    public String listPenilaian(Model model, HttpSession session, @RequestParam(required = false) Integer page,
            @PathVariable("idProdi") Integer idProdi) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        SemesterServiceOutput smtOutput = semesterService.findAllSemester();
        List<SemesterBean> listSmt = smtOutput.getListSemesterBean();
        Map<Integer, String> mapSemester = new HashMap<Integer, String>();
        for (SemesterBean smt : listSmt) {
            mapSemester.put(smt.getIdSemester(), smt.getSemester());
        }
        model.addAttribute("listSmt", listSmt);
        model.addAttribute("mapSemester", mapSemester);
        model.addAttribute("idProdi", idProdi);

        //Sort By and Grafik
        List<PenilaianList> listPenilaian = new ArrayList<PenilaianList>();
        listPenilaian = getListPenilaianByProdi(idProdi);
        Map<Object,Object> mapPenilaian = null;
        List<Map<Object,Object>> dataPenilaian = new ArrayList<Map<Object, Object>>();
        List<List<Map<Object,Object>>> list = new ArrayList<List<Map<Object, Object>>>();
        for(PenilaianList penilaian:listPenilaian){
            mapPenilaian = new HashMap<Object,Object>();
            mapPenilaian.put("label", penilaian.getNamaDosen());
            mapPenilaian.put("y",penilaian.getRerata());
            dataPenilaian.add(mapPenilaian);
            
        }
        list.add(dataPenilaian);
        model.addAttribute("dataPenilaianList",list);
        Collections.sort(listPenilaian, new Comparator<PenilaianList>() {
            @Override
            public int compare(PenilaianList u1, PenilaianList u2) {
                return u2.getRerata().compareTo(u1.getRerata());
            }
        });
        PagedListHolder<PenilaianList> pageListHolder = new PagedListHolder<PenilaianList>(listPenilaian);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }
        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listPenilaian", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listPenilaian", pageListHolder.getPageList());
        }
        //model.addAttribute("listDosen", dosenOutput.getListDosenBean());
        return "listPenilaian";
    }

    @RequestMapping(value = "/searchPenilaian/{idProdi}")
    public String searchPenilaian(Model model, @RequestParam("semester") Integer semester, @PathVariable("idProdi") Integer idProdi, @RequestParam(required = false) Integer page, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        SemesterServiceOutput smtOutput = semesterService.findAllSemester();
        List<SemesterBean> listSmt = smtOutput.getListSemesterBean();
        Map<Integer, String> mapSemester = new HashMap<Integer, String>();
        for (SemesterBean smt : listSmt) {
            mapSemester.put(smt.getIdSemester(), smt.getSemester());
        }
        model.addAttribute("listSmt", listSmt);
        model.addAttribute("mapSemester", mapSemester);
        model.addAttribute("idProdi", idProdi);
        model.addAttribute("idSemester", semester);

        //Sort By and Grafik
        List<PenilaianList> listPenilaian = getListPenilaianBySemester(idProdi, semester);
        Map<Object,Object> mapPenilaian = null;
        List<Map<Object,Object>> dataPenilaian = new ArrayList<Map<Object, Object>>();
        List<List<Map<Object,Object>>> list = new ArrayList<List<Map<Object, Object>>>();
        for(PenilaianList penilaian:listPenilaian){
            mapPenilaian = new HashMap<Object,Object>();
            mapPenilaian.put("label", penilaian.getNamaDosen());
            mapPenilaian.put("y",penilaian.getRerata());
            dataPenilaian.add(mapPenilaian);
            
        }
        list.add(dataPenilaian);
        model.addAttribute("dataPenilaianList",list);
        Collections.sort(listPenilaian, new Comparator<PenilaianList>() {
            @Override
            public int compare(PenilaianList u1, PenilaianList u2) {
                return u2.getRerata().compareTo(u1.getRerata());
            }
        });

        PagedListHolder<PenilaianList> pageListHolder = new PagedListHolder<PenilaianList>(listPenilaian);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }
        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listPenilaian", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listPenilaian", pageListHolder.getPageList());
        }
        return "listPenilaian";
    }

    @RequestMapping(value = "/downloadPdf/{idProdi}", method = RequestMethod.GET)
    public void downloadPDF(HttpServletResponse response, @RequestParam(required = false) Integer idSemester, @PathVariable("idProdi") Integer idProdi) {
        //PerkuliahanServiceOutput perkuliahanOutput = perkuliahanService.findAllPerkuliahan2();
        //List<PerkuliahanBean> listPerkuliahan = perkuliahanOutput.getListPerkuliahanBean();
        List<PenilaianList> listPenilaian = new ArrayList<PenilaianList>();
        if (idSemester == null) {
            listPenilaian = getListPenilaianByProdi(idProdi);
        } else {
            listPenilaian = getListPenilaianBySemester(idProdi, idSemester);
        }
        Collections.sort(listPenilaian, new Comparator<PenilaianList>() {
            @Override
            public int compare(PenilaianList u1, PenilaianList u2) {
                return u2.getRerata().compareTo(u1.getRerata());
            }
        });
        String fileDir = getReport(listPenilaian, idProdi, idSemester);
        String[] fileSplit = fileDir.split("\\\\");
        String fileName = fileSplit[4];
        File file = new File(fileDir);
        file.getParentFile().mkdirs();
        try {
            InputStream is = new FileInputStream(file);
            response.setContentType("application/force-download");
            response.setHeader("Pragma", "public");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Content-type", "application-download");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            ServletOutputStream os = response.getOutputStream();
            byte[] bufferData = new byte[4096];
            int read = 0;
            while ((read = is.read(bufferData)) != -1) {
                os.write(bufferData, 0, read);
            }
            os.flush();
            os.close();
            is.close();
            file.delete();
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(PenilaianController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(PenilaianController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 

    private String getReport(List<PenilaianList> listPenilaian, Integer idProdi, Integer idSemester) {
        ProdiCustomServiceInput input = new ProdiCustomServiceInput();
        input.setIdProdi(idProdi);
        input.setIdSemester(idSemester);
        ProdiCustomServiceOutput output;
        if (idSemester == null) {
            output = prodiCustomService.findProdi(input);
        } else {
            output = prodiCustomService.findProdiBySemester(input);
        }
        DateFormat df = new SimpleDateFormat("dd MMMMM yyyy");
        String path = "D:\\\\temp\\\\";
        String fileName = "report_" + String.valueOf(df.format(new Date())) + "_" + output.getProdiCustomBean().getProdi()
                + "_" + output.getProdiCustomBean().getTahunAjaran() + ".pdf";
        try {
            InputStream is = getClass().getResourceAsStream("/jasper/reports.jasper");
            //JasperReport jasperReport = (JasperReport)JRLoader.loadObject(is);
            JRDataSource ds = new JRBeanCollectionDataSource(listPenilaian);
            String semester = output.getProdiCustomBean().getTahunAjaran() + " / " + output.getProdiCustomBean().getSemester();
            String gambar = new ClassPathResource("/image/logo2.jpg").getPath();
            Map<String, Object> parameters = new HashMap();
            parameters.put("ds", ds);
            parameters.put("gambar", gambar);
            parameters.put("semester", semester);
            parameters.put("prodi", output.getProdiCustomBean().getProdi());
            JasperPrint jasperPrint = JasperFillManager.fillReport(is, parameters, new JREmptyDataSource());
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + fileName);
        } catch (Exception ex) {
            logger.error("Penilaian Controller", ex);
        }
        return path + fileName;
    }

    private List<PenilaianList> getListPenilaianByProdi(Integer idProdi) {
        SurveyServiceInput surveyInput = new SurveyServiceInput();
        surveyInput.setIdProdi(idProdi);
        SurveyServiceOutput surveyOutput = surveyService.getSurveyByProdi(surveyInput);
        List<SurveyBean> listSurvey = surveyOutput.getListSurveyBean();
        List<PenilaianList> listPenilaian = new ArrayList<PenilaianList>();
        PenilaianList penilaian;
        Integer no = 1;
        for (SurveyBean survey : listSurvey) {
            try {
                penilaian = new PenilaianList();
                DosenServiceInput dosenInput = new DosenServiceInput();
                dosenInput.setKodeDosen(survey.getKodeDosen());
                DosenServiceOutput dosenOutput = dosenService.findDosenById(dosenInput);
                penilaian.setNamaDosen(dosenOutput.getDosenBean().getNama());
                penilaian.setNo(no);
                PerkuliahanServiceInput perkInput = new PerkuliahanServiceInput();
                perkInput.setIdPerkuliahan(survey.getIdPerkuliahan());
                PerkuliahanServiceOutput perkOutput = perkuliahanService.findPerkuliahanById(perkInput);
                MatakuliahServiceInput matkulInput = new MatakuliahServiceInput();
                matkulInput.setKodeMk(perkOutput.getPerkuliahanBean().getKodeMk());
                MatakuliahServiceOutput matkulOutput = matkulService.findMatkulById(matkulInput);
                penilaian.setMatakuliah(matkulOutput.getMatakuliahBean().getMataKuliah());

                PenilaianServiceInput penilaianInput = new PenilaianServiceInput();
                penilaianInput.setKodeDosen(survey.getKodeDosen());
                penilaianInput.setIdPerkuliahan(survey.getIdPerkuliahan());
                PenilaianServiceOutput penilaianOutput = penilaianService.findReliability(penilaianInput);
                PenilaianServiceOutput penilaianOutput2 = penilaianService.findResponsiveness(penilaianInput);
                PenilaianServiceOutput penilaianOutput3 = penilaianService.findAssurance(penilaianInput);
                PenilaianServiceOutput penilaianOutput4 = penilaianService.findEmpathy(penilaianInput);
                PenilaianServiceOutput penilaianOutput5 = penilaianService.findTangible(penilaianInput);
                DecimalFormat df = new DecimalFormat("#.##");
                Double reliability;
                Double responsiveness;
                Double assurance;
                Double empathy;
                Double tangible;
                Double rerata;
                //Reliability
                if (penilaianOutput.getReliabilityBean().getNilai() == null) {
                    reliability = 0.0;
                } else {
                    Double nilai = penilaianOutput.getReliabilityBean().getNilai();
                    Integer jumlah = penilaianOutput.getReliabilityBean().getJumlah();
                    reliability = nilai / jumlah;
                }
                //Responsiveness
                if (penilaianOutput2.getResponsivenessBean().getNilai() == null) {
                    responsiveness = 0.0;
                } else {
                    Double nilai = penilaianOutput2.getResponsivenessBean().getNilai();
                    Integer jumlah = penilaianOutput2.getResponsivenessBean().getJumlah();
                    responsiveness = nilai / jumlah;
                }
                //Assurance
                if (penilaianOutput3.getAssuranceBean().getNilai() == null) {
                    assurance = 0.0;
                } else {
                    Double nilai = penilaianOutput3.getAssuranceBean().getNilai();
                    Integer jumlah = penilaianOutput3.getAssuranceBean().getJumlah();
                    assurance = nilai / jumlah;
                }
                //Empathy
                if (penilaianOutput4.getEmpathyBean().getNilai() == null) {
                    empathy = 0.0;
                } else {
                    Double nilai = penilaianOutput4.getEmpathyBean().getNilai();
                    Integer jumlah = penilaianOutput4.getEmpathyBean().getJumlah();
                    empathy = nilai / jumlah;
                }
                //Tangible
                if (penilaianOutput5.getTangibleBean().getNilai() == null) {
                    tangible = 0.0;
                } else {
                    Double nilai = penilaianOutput5.getTangibleBean().getNilai();
                    Integer jumlah = penilaianOutput5.getTangibleBean().getJumlah();
                    tangible = nilai / jumlah;
                }
                rerata = (reliability + responsiveness + assurance + empathy + tangible) / 5;
                penilaian.setReliability(df.format(reliability));
                penilaian.setResponsiveness(df.format(responsiveness));
                penilaian.setAssurance(df.format(assurance));
                penilaian.setEmpathy(df.format(empathy));
                penilaian.setTangible(df.format(tangible));
                penilaian.setRerata(df.format(rerata));

                listPenilaian.add(penilaian);

                no++;
            } catch (Exception e) {
                penilaian = new PenilaianList();
            }
        }

        return listPenilaian;
    }

    private List<PenilaianList> getListPenilaianBySemester(Integer idProdi, Integer semester) {
        SurveyServiceInput surveyInput = new SurveyServiceInput();
        surveyInput.setIdProdi(idProdi);
        surveyInput.setSemester(semester);
        SurveyServiceOutput surveyOutput = surveyService.getSurveyBySemester(surveyInput);
        List<SurveyBean> listSurvey = surveyOutput.getListSurveyBean();
        List<PenilaianList> listPenilaian = new ArrayList<PenilaianList>();
        Integer no = 1;
        PenilaianList penilaian;
        for (SurveyBean survey : listSurvey) {
            try {
                penilaian = new PenilaianList();
                DosenServiceInput dosenInput = new DosenServiceInput();
                dosenInput.setKodeDosen(survey.getKodeDosen());
                DosenServiceOutput dosenOutput = dosenService.findDosenById(dosenInput);
                penilaian.setNamaDosen(dosenOutput.getDosenBean().getNama());
                penilaian.setNo(no);

                PerkuliahanServiceInput perkInput = new PerkuliahanServiceInput();
                perkInput.setIdPerkuliahan(survey.getIdPerkuliahan());
                PerkuliahanServiceOutput perkOutput = perkuliahanService.findPerkuliahanById(perkInput);
                MatakuliahServiceInput matkulInput = new MatakuliahServiceInput();
                matkulInput.setKodeMk(perkOutput.getPerkuliahanBean().getKodeMk());
                MatakuliahServiceOutput matkulOutput = matkulService.findMatkulById(matkulInput);
                penilaian.setMatakuliah(matkulOutput.getMatakuliahBean().getMataKuliah());

                PenilaianServiceInput penilaianInput = new PenilaianServiceInput();
                penilaianInput.setKodeDosen(survey.getKodeDosen());
                penilaianInput.setIdPerkuliahan(survey.getIdPerkuliahan());
                penilaianInput.setSemester(survey.getIdSemester());
                PenilaianServiceOutput penilaianOutput = penilaianService.findReliabilityBySemester(penilaianInput);
                PenilaianServiceOutput penilaianOutput2 = penilaianService.findResponsivenessBySemester(penilaianInput);
                PenilaianServiceOutput penilaianOutput3 = penilaianService.findAssuranceBySemester(penilaianInput);
                PenilaianServiceOutput penilaianOutput4 = penilaianService.findEmpathyBySemester(penilaianInput);
                PenilaianServiceOutput penilaianOutput5 = penilaianService.findTangibleBySemester(penilaianInput);
                DecimalFormat df = new DecimalFormat("#.##");
                Double reliability;
                Double responsiveness;
                Double assurance;
                Double empathy;
                Double tangible;
                Double rerata;
                //Reliability
                if (penilaianOutput.getReliabilityBean().getNilai() == null) {
                    reliability = 0.0;
                } else {
                    Double nilai = penilaianOutput.getReliabilityBean().getNilai();
                    Integer jumlah = penilaianOutput.getReliabilityBean().getJumlah();
                    reliability = nilai / jumlah;
                }
                //Responsiveness
                if (penilaianOutput2.getResponsivenessBean().getNilai() == null) {
                    responsiveness = 0.0;
                } else {
                    Double nilai = penilaianOutput2.getResponsivenessBean().getNilai();
                    Integer jumlah = penilaianOutput2.getResponsivenessBean().getJumlah();
                    responsiveness = nilai / jumlah;
                }
                //Assurance
                if (penilaianOutput3.getAssuranceBean().getNilai() == null) {
                    assurance = 0.0;
                } else {
                    Double nilai = penilaianOutput3.getAssuranceBean().getNilai();
                    Integer jumlah = penilaianOutput3.getAssuranceBean().getJumlah();
                    assurance = nilai / jumlah;
                }
                //Empathy
                if (penilaianOutput4.getEmpathyBean().getNilai() == null) {
                    empathy = 0.0;
                } else {
                    Double nilai = penilaianOutput4.getEmpathyBean().getNilai();
                    Integer jumlah = penilaianOutput4.getEmpathyBean().getJumlah();
                    empathy = nilai / jumlah;
                }
                //Tangible
                if (penilaianOutput5.getTangibleBean().getNilai() == null) {
                    tangible = 0.0;
                } else {
                    Double nilai = penilaianOutput5.getTangibleBean().getNilai();
                    Integer jumlah = penilaianOutput5.getTangibleBean().getJumlah();
                    tangible = nilai / jumlah;
                }
                rerata = (reliability + responsiveness + assurance + empathy + tangible) / 5;
                penilaian.setReliability(df.format(reliability));
                penilaian.setResponsiveness(df.format(responsiveness));
                penilaian.setAssurance(df.format(assurance));
                penilaian.setEmpathy(df.format(empathy));
                penilaian.setTangible(df.format(tangible));
                penilaian.setRerata(df.format(rerata));
                listPenilaian.add(penilaian);
                no++;
            } catch (Exception e) {
                penilaian = new PenilaianList();
            }
        }
        return listPenilaian;
    }

    private List<PenilaianList> getListPenilaian(List<PerkuliahanBean> listPerkuliahan) {
        List<PenilaianList> listPenilaian = new ArrayList<PenilaianList>();
        PenilaianList penilaian;
        for (PerkuliahanBean perkuliahan : listPerkuliahan) {
            try {

                penilaian = new PenilaianList();
                DosenServiceInput dosenInput = new DosenServiceInput();
                dosenInput.setKodeDosen(perkuliahan.getKodeDosen());
                DosenServiceOutput dosenOutput = dosenService.findDosenById(dosenInput);
                penilaian.setNamaDosen(dosenOutput.getDosenBean().getNama());
                MatakuliahServiceInput matkulInput = new MatakuliahServiceInput();
                matkulInput.setKodeMk(perkuliahan.getKodeMk());
                MatakuliahServiceOutput matkulOutput = matkulService.findMatkulById(matkulInput);
                penilaian.setMatakuliah(matkulOutput.getMatakuliahBean().getMataKuliah());
                PenilaianServiceInput penilaianInput = new PenilaianServiceInput();
                penilaianInput.setKodeDosen(perkuliahan.getKodeDosen());
                penilaianInput.setIdPerkuliahan(perkuliahan.getIdPerkuliahan());
                PenilaianServiceOutput penilaianOutput = penilaianService.findReliability(penilaianInput);
                PenilaianServiceOutput penilaianOutput2 = penilaianService.findResponsiveness(penilaianInput);
                PenilaianServiceOutput penilaianOutput3 = penilaianService.findAssurance(penilaianInput);
                PenilaianServiceOutput penilaianOutput4 = penilaianService.findEmpathy(penilaianInput);
                PenilaianServiceOutput penilaianOutput5 = penilaianService.findTangible(penilaianInput);
                DecimalFormat df = new DecimalFormat("#.##");
                Double reliability;
                Double responsiveness;
                Double assurance;
                Double empathy;
                Double tangible;
                Double rerata;
                //Reliability
                if (penilaianOutput.getReliabilityBean().getNilai() == null) {
                    reliability = 0.0;
                } else {
                    Double nilai = penilaianOutput.getReliabilityBean().getNilai();
                    Integer jumlah = penilaianOutput.getReliabilityBean().getJumlah();
                    reliability = nilai / jumlah;
                }
                //Responsiveness
                if (penilaianOutput2.getResponsivenessBean().getNilai() == null) {
                    responsiveness = 0.0;
                } else {
                    Double nilai = penilaianOutput2.getResponsivenessBean().getNilai();
                    Integer jumlah = penilaianOutput2.getResponsivenessBean().getJumlah();
                    responsiveness = nilai / jumlah;
                }
                //Assurance
                if (penilaianOutput3.getAssuranceBean().getNilai() == null) {
                    assurance = 0.0;
                } else {
                    Double nilai = penilaianOutput3.getAssuranceBean().getNilai();
                    Integer jumlah = penilaianOutput3.getAssuranceBean().getJumlah();
                    assurance = nilai / jumlah;
                }
                //Empathy
                if (penilaianOutput4.getEmpathyBean().getNilai() == null) {
                    empathy = 0.0;
                } else {
                    Double nilai = penilaianOutput4.getEmpathyBean().getNilai();
                    Integer jumlah = penilaianOutput4.getEmpathyBean().getJumlah();
                    empathy = nilai / jumlah;
                }
                //Tangible
                if (penilaianOutput5.getTangibleBean().getNilai() == null) {
                    tangible = 0.0;
                } else {
                    Double nilai = penilaianOutput5.getTangibleBean().getNilai();
                    Integer jumlah = penilaianOutput5.getTangibleBean().getJumlah();
                    tangible = nilai / jumlah;
                }
                rerata = (reliability + responsiveness + assurance + empathy + tangible) / 5;
                penilaian.setReliability(df.format(reliability));
                penilaian.setResponsiveness(df.format(responsiveness));
                penilaian.setAssurance(df.format(assurance));
                penilaian.setEmpathy(df.format(empathy));
                penilaian.setTangible(df.format(tangible));
                penilaian.setRerata(df.format(rerata));
                listPenilaian.add(penilaian);
                if (perkuliahan.getKodeDosen2() != null || !perkuliahan.getKodeDosen2().isEmpty()) {
                    penilaian = new PenilaianList();
                    DosenServiceInput dosenInput2 = new DosenServiceInput();
                    dosenInput2.setKodeDosen(perkuliahan.getKodeDosen2());
                    DosenServiceOutput dosenOutput2 = dosenService.findDosenById(dosenInput2);
                    penilaian.setNamaDosen(dosenOutput2.getDosenBean().getNama());
                    MatakuliahServiceInput matkulInput2 = new MatakuliahServiceInput();
                    matkulInput2.setKodeMk(perkuliahan.getKodeMk());
                    MatakuliahServiceOutput matkulOutput2 = matkulService.findMatkulById(matkulInput2);
                    penilaian.setMatakuliah(matkulOutput2.getMatakuliahBean().getMataKuliah());
                    PenilaianServiceInput penilaianInput2 = new PenilaianServiceInput();
                    penilaianInput2.setKodeDosen(perkuliahan.getKodeDosen2());
                    penilaianInput2.setIdPerkuliahan(perkuliahan.getIdPerkuliahan());
                    PenilaianServiceOutput penilaianOutput_ = penilaianService.findReliability(penilaianInput2);
                    PenilaianServiceOutput penilaianOutput_2 = penilaianService.findResponsiveness(penilaianInput2);
                    PenilaianServiceOutput penilaianOutput_3 = penilaianService.findAssurance(penilaianInput2);
                    PenilaianServiceOutput penilaianOutput_4 = penilaianService.findEmpathy(penilaianInput2);
                    PenilaianServiceOutput penilaianOutput_5 = penilaianService.findTangible(penilaianInput2);
                    Double reliability2;
                    Double responsiveness2;
                    Double assurance2;
                    Double empathy2;
                    Double tangible2;
                    Double rerata2;
                    //Reliability
                    if (penilaianOutput_.getReliabilityBean().getNilai() == null) {
                        reliability2 = 0.0;
                    } else {
                        Double nilai2 = penilaianOutput_.getReliabilityBean().getNilai();
                        Integer jumlah2 = penilaianOutput_.getReliabilityBean().getJumlah();
                        reliability2 = nilai2 / jumlah2;
                    }
                    //Responsiveness
                    if (penilaianOutput_2.getResponsivenessBean().getNilai() == null) {
                        responsiveness2 = 0.0;
                    } else {
                        Double nilai2 = penilaianOutput_2.getResponsivenessBean().getNilai();
                        Integer jumlah2 = penilaianOutput_2.getResponsivenessBean().getJumlah();
                        responsiveness2 = nilai2 / jumlah2;
                    }
                    //Assurance
                    if (penilaianOutput_3.getAssuranceBean().getNilai() == null) {
                        assurance2 = 0.0;
                    } else {
                        Double nilai2 = penilaianOutput_3.getAssuranceBean().getNilai();
                        Integer jumlah2 = penilaianOutput_3.getAssuranceBean().getJumlah();
                        assurance2 = nilai2 / jumlah2;
                    }
                    //Empathy
                    if (penilaianOutput_4.getEmpathyBean().getNilai() == null) {
                        empathy2 = 0.0;
                    } else {
                        Double nilai2 = penilaianOutput_4.getEmpathyBean().getNilai();
                        Integer jumlah2 = penilaianOutput_4.getEmpathyBean().getJumlah();
                        empathy2 = nilai2 / jumlah2;
                    }
                    //Tangible
                    if (penilaianOutput_5.getTangibleBean().getNilai() == null) {
                        tangible2 = 0.0;
                    } else {
                        Double nilai2 = penilaianOutput_5.getTangibleBean().getNilai();
                        Integer jumlah2 = penilaianOutput_5.getTangibleBean().getJumlah();
                        tangible2 = nilai2 / jumlah2;
                    }
                    rerata2 = (reliability2 + responsiveness2 + assurance2 + empathy2 + tangible2) / 5;
                    penilaian.setReliability(df.format(reliability2));
                    penilaian.setResponsiveness(df.format(responsiveness2));
                    penilaian.setAssurance(df.format(assurance2));
                    penilaian.setEmpathy(df.format(empathy2));
                    penilaian.setTangible(df.format(tangible2));
                    penilaian.setRerata(df.format(rerata2));
                    listPenilaian.add(penilaian);
                }

            } catch (Exception e) {
                penilaian = new PenilaianList();
                //penilaian.setNamaDosen(null);
                //penilaian.setMatakuliah(null);
                //penilaian.setReliability(null);
                //listPenilaian.add(penilaian);
            }
        }
        return listPenilaian;
    }
}
