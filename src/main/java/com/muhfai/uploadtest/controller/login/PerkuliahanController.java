/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.controller.login;

import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.core.dosen.DosenBean;
import com.muhfai.uploadtest.core.kelas.KelasBean;
import com.muhfai.uploadtest.core.matakuliah.MatakuliahBean;
import com.muhfai.uploadtest.core.perkuliahan.PerkuliahanBean;
import com.muhfai.uploadtest.core.semester.SemesterBean;
import com.muhfai.uploadtest.service.dosen.DosenService;
import com.muhfai.uploadtest.service.dosen.DosenServiceInput;
import com.muhfai.uploadtest.service.dosen.DosenServiceOutput;
import com.muhfai.uploadtest.service.kelas.KelasService;
import com.muhfai.uploadtest.service.kelas.KelasServiceInput;
import com.muhfai.uploadtest.service.kelas.KelasServiceOutput;
import com.muhfai.uploadtest.service.login.LoginService;
import com.muhfai.uploadtest.service.login.LoginServiceInput;
import com.muhfai.uploadtest.service.login.LoginServiceOutput;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahService;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahServiceInput;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahServiceOutput;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanList;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanService;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanServiceInput;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanServiceOutput;
import com.muhfai.uploadtest.service.semester.SemesterService;
import com.muhfai.uploadtest.service.semester.SemesterServiceInput;
import com.muhfai.uploadtest.service.semester.SemesterServiceOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author MuhFai10
 */
@Controller
@RequestMapping(value = "/perkuliahan")
public class PerkuliahanController {

    private static final Logger logger = LoggerFactory
            .getLogger(PerkuliahanController.class);
    @Autowired
    private PerkuliahanService perkuliahanService;
    @Autowired
    private KelasService kelasService;
    @Autowired
    private DosenService dosenService;
    @Autowired
    private MatakuliahService matakuliahService;
    @Autowired
    private SemesterService semesterService;
    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/listPerkuliahan", method = RequestMethod.GET)
    public String listPerkuliahan(Model model, HttpSession session, @RequestParam(required = false) Integer page) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        PerkuliahanServiceOutput perkuliahanOutput = perkuliahanService.getAllPerkuliahan();
        List<PerkuliahanBean> listBean = perkuliahanOutput.getListPerkuliahanBean();
        List<PerkuliahanList> listPerkuliahan = new ArrayList<PerkuliahanList>();
        for (PerkuliahanBean perkuliahan : listBean) {
            PerkuliahanList perkuliahanList = new PerkuliahanList();
            perkuliahanList.setKodeMk(perkuliahan.getKodeMk());
            //Get Kelas
            perkuliahanList.setIdPerkuliahan(perkuliahan.getIdPerkuliahan());
            KelasServiceInput kelasInput = new KelasServiceInput();
            kelasInput.setIdKelas(perkuliahan.getIdKelas());
            KelasServiceOutput kelasOutput = kelasService.findKelasById(kelasInput);
            perkuliahanList.setKelas(kelasOutput.getKelasBean().getKode());

            // Get Dosen 1
            DosenServiceInput dosenInput = new DosenServiceInput();
            dosenInput.setKodeDosen(perkuliahan.getKodeDosen());
            DosenServiceOutput dosenOutput = dosenService.findDosenById(dosenInput);
            perkuliahanList.setNamaDosen(dosenOutput.getDosenBean().getNama());

            //Get Dosen 2
            dosenInput.setKodeDosen(perkuliahan.getKodeDosen2());
            DosenServiceOutput dosenOutput2 = dosenService.findDosenById(dosenInput);
            perkuliahanList.setNamaDosen2(dosenOutput2.getDosenBean().getNama());

            //Get Matakuliah
            MatakuliahServiceInput matkulInput = new MatakuliahServiceInput();
            matkulInput.setKodeMk(perkuliahan.getKodeMk());
            MatakuliahServiceOutput matkulOutput = matakuliahService.findMatkulById(matkulInput);
            perkuliahanList.setMatakuliah(matkulOutput.getMatakuliahBean().getMataKuliah());

            //Get Semester
            SemesterServiceInput semesterInput = new SemesterServiceInput();
            semesterInput.setIdSemester(perkuliahan.getIdSemester());
            SemesterServiceOutput semesterOutput = semesterService.findSemesterById(semesterInput);
            perkuliahanList.setSemester(semesterOutput.getSemesterBean().getSemester() + " / " + semesterOutput.getSemesterBean().getTahunAjaran());

            listPerkuliahan.add(perkuliahanList);
        }
        SemesterServiceOutput smtOutput = semesterService.getSemesterInPerkuliahan();
        List<SemesterBean> listSmt = smtOutput.getListSemesterBean();
        
        model.addAttribute("listSmt", listSmt);
        //model.addAttribute("mapSemester", listSemester);
        
        PagedListHolder<PerkuliahanList> pageListHolder = new PagedListHolder<PerkuliahanList>(listPerkuliahan);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }
        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listPerkuliahan", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listPerkuliahan", pageListHolder.getPageList());
        }
        //model.addAttribute("listPerkuliahan", listPerkuliahan);
        return "listPerkuliahan";
    }

    @RequestMapping(value = "/searchBySemester")
    public String searchBySemester(Model model, @RequestParam("semesterNow") Integer semester, @RequestParam(required = false) Integer page, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        SemesterServiceOutput smtOutput = semesterService.findAllSemester();
        List<SemesterBean> listSmtr = smtOutput.getListSemesterBean();
        model.addAttribute("listSmtr", listSmtr);
        model.addAttribute("idSemester",semester);
        
        SemesterServiceOutput smtOutput2 = semesterService.getSemesterInPerkuliahan();
        List<SemesterBean> listSmt = smtOutput2.getListSemesterBean();
        
        model.addAttribute("listSmt", listSmt);

        PerkuliahanServiceInput prkInput = new PerkuliahanServiceInput();
        prkInput.setIdSemester(semester);
        PerkuliahanServiceOutput prkOutput = perkuliahanService.getPerkuliahanBySemester(prkInput);
        List<PerkuliahanBean> listBean = prkOutput.getListPerkuliahanBean();
        //model.addAttribute("messageSearch",prkOutput.getMessage());
        if(prkOutput.getMessage().equals(Message.SEARCH_PERKULIAHAN_SUCCESS)){
            model.addAttribute("messageSearch",1);
        }else{
            model.addAttribute("messageSearch",0);
        }
        List<PerkuliahanList> listPerkuliahan = new ArrayList<PerkuliahanList>();
        for (PerkuliahanBean perkuliahan : listBean) {
            PerkuliahanList perkuliahanList = new PerkuliahanList();
            perkuliahanList.setKodeMk(perkuliahan.getKodeMk());
            //Get Kelas
            perkuliahanList.setIdPerkuliahan(perkuliahan.getIdPerkuliahan());
            KelasServiceInput kelasInput = new KelasServiceInput();
            kelasInput.setIdKelas(perkuliahan.getIdKelas());
            KelasServiceOutput kelasOutput = kelasService.findKelasById(kelasInput);
            perkuliahanList.setKelas(kelasOutput.getKelasBean().getKode());

            // Get Dosen 1
            DosenServiceInput dosenInput = new DosenServiceInput();
            dosenInput.setKodeDosen(perkuliahan.getKodeDosen());
            DosenServiceOutput dosenOutput = dosenService.findDosenById(dosenInput);
            perkuliahanList.setNamaDosen(dosenOutput.getDosenBean().getNama());

            //Get Dosen 2
            dosenInput.setKodeDosen(perkuliahan.getKodeDosen2());
            DosenServiceOutput dosenOutput2 = dosenService.findDosenById(dosenInput);
            perkuliahanList.setNamaDosen2(dosenOutput2.getDosenBean().getNama());

            //Get Matakuliah
            MatakuliahServiceInput matkulInput = new MatakuliahServiceInput();
            matkulInput.setKodeMk(perkuliahan.getKodeMk());
            MatakuliahServiceOutput matkulOutput = matakuliahService.findMatkulById(matkulInput);
            perkuliahanList.setMatakuliah(matkulOutput.getMatakuliahBean().getMataKuliah());

            //Get Semester
            SemesterServiceInput semesterInput = new SemesterServiceInput();
            semesterInput.setIdSemester(perkuliahan.getIdSemester());
            SemesterServiceOutput semesterOutput = semesterService.findSemesterById(semesterInput);
            perkuliahanList.setSemester(semesterOutput.getSemesterBean().getSemester() + " / " + semesterOutput.getSemesterBean().getTahunAjaran());

            listPerkuliahan.add(perkuliahanList);
        }

        PagedListHolder<PerkuliahanList> pageListHolder = new PagedListHolder<PerkuliahanList>(listPerkuliahan);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }
        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listPerkuliahan", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listPerkuliahan", pageListHolder.getPageList());
        }
        return "listPerkuliahan";
    }
    
    @RequestMapping(value = "/updateList", method = RequestMethod.POST)
    public String updateList(@RequestParam("semesterNew") Integer semester,
            @RequestParam(required=false) Integer semesterNow,
            RedirectAttributes ra, Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        PerkuliahanServiceInput prkInput = new PerkuliahanServiceInput();
        prkInput.setIdSemester(semesterNow);
        PerkuliahanServiceOutput prkOutput = perkuliahanService.getPerkuliahanBySemester(prkInput);
        List<PerkuliahanBean> listBean = prkOutput.getListPerkuliahanBean();
        prkInput.setListPerkuliahanBean(listBean);
        prkInput.setSemesterNew(semester);
        PerkuliahanServiceOutput prkOutput2 = perkuliahanService.updatePerkuliahanList(prkInput);
        ra.addFlashAttribute("message", prkOutput2.getMessage());
        return "redirect:/perkuliahan/listPerkuliahan";
        
    }
    
    @RequestMapping(value = "/searchPerkuliahan")
    public String searchPerkuliahan(Model model, HttpSession session, @RequestParam("freeText") String freeText, @RequestParam(required = false) Integer page) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        PerkuliahanServiceInput prkInput = new PerkuliahanServiceInput();
        prkInput.setFreeText(freeText);
        PerkuliahanServiceOutput prkOutput = perkuliahanService.findPerkuliahan(prkInput);
        List<PerkuliahanBean> listBean = prkOutput.getListPerkuliahanBean();
        List<PerkuliahanList> listPerkuliahan = new ArrayList<PerkuliahanList>();
        for (PerkuliahanBean perkuliahan : listBean) {
            PerkuliahanList perkuliahanList = new PerkuliahanList();
            perkuliahanList.setKodeMk(perkuliahan.getKodeMk());
            //Get Kelas
            perkuliahanList.setIdPerkuliahan(perkuliahan.getIdPerkuliahan());
            KelasServiceInput kelasInput = new KelasServiceInput();
            kelasInput.setIdKelas(perkuliahan.getIdKelas());
            KelasServiceOutput kelasOutput = kelasService.findKelasById(kelasInput);
            perkuliahanList.setKelas(kelasOutput.getKelasBean().getKode());

            // Get Dosen 1
            DosenServiceInput dosenInput = new DosenServiceInput();
            dosenInput.setKodeDosen(perkuliahan.getKodeDosen());
            DosenServiceOutput dosenOutput = dosenService.findDosenById(dosenInput);
            perkuliahanList.setNamaDosen(dosenOutput.getDosenBean().getNama());

            //Get Dosen 2
            dosenInput.setKodeDosen(perkuliahan.getKodeDosen2());
            DosenServiceOutput dosenOutput2 = dosenService.findDosenById(dosenInput);
            perkuliahanList.setNamaDosen2(dosenOutput2.getDosenBean().getNama());

            //Get Matakuliah
            MatakuliahServiceInput matkulInput = new MatakuliahServiceInput();
            matkulInput.setKodeMk(perkuliahan.getKodeMk());
            MatakuliahServiceOutput matkulOutput = matakuliahService.findMatkulById(matkulInput);
            perkuliahanList.setMatakuliah(matkulOutput.getMatakuliahBean().getMataKuliah());

            //Get Semester
            SemesterServiceInput semesterInput = new SemesterServiceInput();
            semesterInput.setIdSemester(perkuliahan.getIdSemester());
            SemesterServiceOutput semesterOutput = semesterService.findSemesterById(semesterInput);
            perkuliahanList.setSemester(semesterOutput.getSemesterBean().getSemester() + " / " + semesterOutput.getSemesterBean().getTahunAjaran());

            listPerkuliahan.add(perkuliahanList);
        }

        PagedListHolder<PerkuliahanList> pageListHolder = new PagedListHolder<PerkuliahanList>(listPerkuliahan);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }
        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listPerkuliahan", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listPerkuliahan", pageListHolder.getPageList());
        }
        return "listPerkuliahan";
    }

    @RequestMapping(value = "/newPerkuliahan", method = RequestMethod.GET)
    public String newPerkuliahan(Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        PerkuliahanBean perkuliahan = new PerkuliahanBean();

        getPerkuliahanList(model);
        model.addAttribute("perkuliahan", perkuliahan);
        return "newPerkuliahan";
    }

    private void getPerkuliahanList(Model model) {
        //GET Kelas
        KelasServiceOutput kelasOutput = kelasService.findAllKelas();
        List<KelasBean> listKelas = kelasOutput.getListKelasBean();
        Map<Integer, String> mapKelas = new HashMap<Integer, String>();
        for (KelasBean kelas : listKelas) {
            mapKelas.put(kelas.getIdKelas(), kelas.getKode());
        }
        model.addAttribute("mapKelas", mapKelas);
        //GET Matkul
        MatakuliahServiceOutput matkulOutput = matakuliahService.findAllMatakuliah();
        List<MatakuliahBean> listMatkul = matkulOutput.getListMatkulBean();
        Map<String, String> mapMatkul = new HashMap<String, String>();
        for (MatakuliahBean matkul : listMatkul) {
            mapMatkul.put(matkul.getKodeMk(), matkul.getMataKuliah());
        }
        model.addAttribute("mapMatkul", mapMatkul);
        //GET Dosen 1 List
        DosenServiceOutput dosenOutput = dosenService.findAllDosen();
        List<DosenBean> listDosen = dosenOutput.getListDosenBean();
        Map<String, String> mapDosen = new HashMap<String, String>();
        for (DosenBean dosen : listDosen) {
            mapDosen.put(dosen.getKodeDosen(), dosen.getNama());
        }
        model.addAttribute("mapDosen", mapDosen);
        //GET Dosen 2 List
        DosenServiceOutput dosenOutput2 = dosenService.findAllDosen();
        List<DosenBean> listDosen2 = dosenOutput2.getListDosenBean();
        Map<String, String> mapDosen2 = new HashMap<String, String>();
        for (DosenBean dosen : listDosen2) {
            mapDosen2.put(dosen.getKodeDosen(), dosen.getNama());
        }
        model.addAttribute("mapDosen2", mapDosen2);
        //GET Semester List
        SemesterServiceOutput semesterOutput = semesterService.findAllSemester();
        List<SemesterBean> listSemester = semesterOutput.getListSemesterBean();
        Map<Integer, String> mapSemester = new HashMap<Integer, String>();
        for (SemesterBean semester : listSemester) {
            mapSemester.put(semester.getIdSemester(), semester.getSemester());
        }
        model.addAttribute("mapSemester", mapSemester);
    }

    @RequestMapping(value = "/insertPerkuliahan", method = RequestMethod.POST)
    public String insertPerkuliahan(@ModelAttribute("perkuliahan") @Valid PerkuliahanBean perkuliahanBean, BindingResult result,
            RedirectAttributes ra, Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        PerkuliahanServiceInput input = new PerkuliahanServiceInput();
        input.setPerkuliahanBean(perkuliahanBean);
        if (result.hasErrors()) {
            getPerkuliahanList(model);
            return "newPerkuliahan";
        }
        PerkuliahanServiceOutput output = perkuliahanService.insertPerkuliahan(input);
        if (output.getMessage().equals(Message.MATAKULIAH_FAILED)) {
            getPerkuliahanList(model);
            model.addAttribute("message", output.getMessage());
            return "newPerkuliahan";
        }
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/perkuliahan/listPerkuliahan";
    }
    
    @RequestMapping(value = "/updatePerkuliahan", method = RequestMethod.POST)
    public String updatePerkuliahan(@ModelAttribute("perkuliahan") @Valid PerkuliahanBean perkuliahanBean, BindingResult result,
            RedirectAttributes ra, Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        PerkuliahanServiceInput input = new PerkuliahanServiceInput();
        input.setPerkuliahanBean(perkuliahanBean);
        if (result.hasErrors()) {
            getPerkuliahanList(model);
            return "editPerkuliahan";
        }
        PerkuliahanServiceOutput output = perkuliahanService.updatePerkuliahan(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/perkuliahan/listPerkuliahan";
    }
    @RequestMapping(value = "/editPerkuliahan/{idPerkuliahan}", method = RequestMethod.GET)
    public String editPerkuliahan(@PathVariable("idPerkuliahan") Integer idPerkuliahan,
            Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        PerkuliahanServiceInput input = new PerkuliahanServiceInput();
        input.setIdPerkuliahan(idPerkuliahan);
        //GET Kelas List
        KelasServiceOutput kelasOutput = kelasService.findAllKelas();
        List<KelasBean> listKelas = kelasOutput.getListKelasBean();
        Map<Integer, String> mapKelas = new HashMap<Integer, String>();
        for (KelasBean kelas : listKelas) {
            mapKelas.put(kelas.getIdKelas(), kelas.getKode());
        }
        model.addAttribute("mapKelas", mapKelas);

        //GET Matakuliah List
        MatakuliahServiceOutput matkulOutput = matakuliahService.findAllMatakuliah();
        List<MatakuliahBean> listMatkul = matkulOutput.getListMatkulBean();
        Map<String, String> mapMatkul = new HashMap<String, String>();
        for (MatakuliahBean matkul : listMatkul) {
            mapMatkul.put(matkul.getKodeMk(), matkul.getMataKuliah());
        }
        model.addAttribute("mapMatkul", mapMatkul);

        //GET Dosen 1 List
        DosenServiceOutput dosenOutput = dosenService.findAllDosen();
        List<DosenBean> listDosen = dosenOutput.getListDosenBean();
        Map<String, String> mapDosen = new HashMap<String, String>();
        for (DosenBean dosen : listDosen) {
            mapDosen.put(dosen.getKodeDosen(), dosen.getNama());
        }
        model.addAttribute("mapDosen", mapDosen);

        //GET Dosen 2 List
        DosenServiceOutput dosenOutput2 = dosenService.findAllDosen();
        List<DosenBean> listDosen2 = dosenOutput2.getListDosenBean();
        Map<String, String> mapDosen2 = new HashMap<String, String>();
        for (DosenBean dosen : listDosen2) {
            mapDosen2.put(dosen.getKodeDosen(), dosen.getNama());
        }
        model.addAttribute("mapDosen2", mapDosen2);

        //GET Semester List
        SemesterServiceOutput semesterOutput = semesterService.findAllSemester();
        List<SemesterBean> listSemester = semesterOutput.getListSemesterBean();
        Map<Integer, String> mapSemester = new HashMap<Integer, String>();
        for (SemesterBean semester : listSemester) {
            mapSemester.put(semester.getIdSemester(), semester.getSemester());
        }
        model.addAttribute("mapSemester", mapSemester);
        PerkuliahanServiceOutput output = perkuliahanService.findPerkuliahanById(input);
        model.addAttribute("perkuliahan", output.getPerkuliahanBean());
        return "editPerkuliahan";
    }

    @RequestMapping(value = "/deletePerkuliahan/{idPerkuliahan}", method = RequestMethod.GET)
    public String deletePerkuliahan(@PathVariable("idPerkuliahan") Integer idPerkuliahan,
            RedirectAttributes ra, Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        PerkuliahanServiceInput input = new PerkuliahanServiceInput();
        input.setIdPerkuliahan(idPerkuliahan);
        PerkuliahanServiceOutput output = perkuliahanService.deletePerkuliahanById(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/perkuliahan/listPerkuliahan";
    }
}
