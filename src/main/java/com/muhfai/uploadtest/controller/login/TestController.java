/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.controller.login;

import com.muhfai.uploadtest.core.dosen.DosenBean;
import com.muhfai.uploadtest.core.mahasiswa.MahasiswaBean;
import com.muhfai.uploadtest.core.perkuliahan.PerkuliahanBean;
import com.muhfai.uploadtest.core.survey.SurveyBean;
import com.muhfai.uploadtest.core.user.UserBean;
import com.muhfai.uploadtest.service.dosen.DosenService;
import com.muhfai.uploadtest.service.dosen.DosenServiceInput;
import com.muhfai.uploadtest.service.dosen.DosenServiceOutput;
import com.muhfai.uploadtest.service.login.LoginService;
import com.muhfai.uploadtest.service.login.LoginServiceInput;
import com.muhfai.uploadtest.service.login.LoginServiceOutput;
import com.muhfai.uploadtest.service.mahasiswa.MahasiswaService;
import com.muhfai.uploadtest.service.mahasiswa.MahasiswaServiceInput;
import com.muhfai.uploadtest.service.mahasiswa.MahasiswaServiceOutput;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahService;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahServiceInput;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahServiceOutput;
import com.muhfai.uploadtest.service.penilaian.PenilaianList;
import com.muhfai.uploadtest.service.penilaian.PenilaianService;
import com.muhfai.uploadtest.service.penilaian.PenilaianServiceInput;
import com.muhfai.uploadtest.service.penilaian.PenilaianServiceOutput;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanService;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanServiceInput;
import com.muhfai.uploadtest.service.perkuliahan.PerkuliahanServiceOutput;
import com.muhfai.uploadtest.service.survey.SurveyService;
import com.muhfai.uploadtest.service.survey.SurveyServiceOutput;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author MuhFai10
 */
@Controller
@RequestMapping(value = "/dashboard")
public class TestController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private MahasiswaService mahasiswaService;
    @Autowired
    MatakuliahService matkulService;
    @Autowired
    private DosenService dosenService;
    @Autowired
    private PerkuliahanService perkuliahanService;
    @Autowired
    PenilaianService penilaianService;
    @Autowired
    SurveyService surveyService;

    @RequestMapping(value = "/adminDashboard", method = RequestMethod.GET)
    public String dashboard(HttpServletRequest request, Model model) {
        String username = (String) request.getSession().getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        Map<Integer, String> mapRole = new HashMap<Integer, String>();
        mapRole.put(loginOutput.getUserBean().getRole(), "Administrator");
        model.addAttribute("role", mapRole);

        //GET Mahasiswa
        MahasiswaServiceOutput mhsOutput = mahasiswaService.findAllMahasiswa();
        List<MahasiswaBean> mhsBean = mhsOutput.getListMahasiswaBean();
        model.addAttribute("totalMhs", mhsBean.size());

        //GET Dosen
        DosenServiceOutput dosenOutput = dosenService.findAllDosen();
        List<DosenBean> dosenBean = dosenOutput.getListDosenBean();
        model.addAttribute("totalDosen", dosenBean.size());

        //GET User
        LoginServiceOutput userOutput = loginService.findAllUser();
        List<UserBean> userBean = userOutput.getListUserBean();
        model.addAttribute("totalUser", userBean.size());

        //GET Perkuliahan
        PerkuliahanServiceOutput perkOutput = perkuliahanService.getAllPerkuliahan();
        List<PerkuliahanBean> perkBean = perkOutput.getListPerkuliahanBean();
        model.addAttribute("totalPerk", perkBean.size());

        model.addAttribute("tanggal", new Date());
        
         
        return "dashboard";
    }

    private List<PenilaianList> getListPenilaian() {
        SurveyServiceOutput surveyOutput = surveyService.getAllSurvey();
        List<SurveyBean> listSurvey = surveyOutput.getListSurveyBean();
        List<PenilaianList> listPenilaian = new ArrayList<PenilaianList>();
        PenilaianList penilaian;
        Integer no = 1;
        for (SurveyBean survey : listSurvey) {
            try {
                penilaian = new PenilaianList();
                DosenServiceInput dosenInput = new DosenServiceInput();
                dosenInput.setKodeDosen(survey.getKodeDosen());
                DosenServiceOutput dosenOutput = dosenService.findDosenById(dosenInput);
                penilaian.setNamaDosen(dosenOutput.getDosenBean().getNama());
                penilaian.setNo(no);
                PerkuliahanServiceInput perkInput = new PerkuliahanServiceInput();
                perkInput.setIdPerkuliahan(survey.getIdPerkuliahan());
                PerkuliahanServiceOutput perkOutput = perkuliahanService.findPerkuliahanById(perkInput);
                MatakuliahServiceInput matkulInput = new MatakuliahServiceInput();
                matkulInput.setKodeMk(perkOutput.getPerkuliahanBean().getKodeMk());
                MatakuliahServiceOutput matkulOutput = matkulService.findMatkulById(matkulInput);
                penilaian.setMatakuliah(matkulOutput.getMatakuliahBean().getMataKuliah());

                PenilaianServiceInput penilaianInput = new PenilaianServiceInput();
                penilaianInput.setKodeDosen(survey.getKodeDosen());
                penilaianInput.setIdPerkuliahan(survey.getIdPerkuliahan());
                PenilaianServiceOutput penilaianOutput = penilaianService.findReliability(penilaianInput);
                PenilaianServiceOutput penilaianOutput2 = penilaianService.findResponsiveness(penilaianInput);
                PenilaianServiceOutput penilaianOutput3 = penilaianService.findAssurance(penilaianInput);
                PenilaianServiceOutput penilaianOutput4 = penilaianService.findEmpathy(penilaianInput);
                PenilaianServiceOutput penilaianOutput5 = penilaianService.findTangible(penilaianInput);
                DecimalFormat df = new DecimalFormat("#.##");
                Double reliability;
                Double responsiveness;
                Double assurance;
                Double empathy;
                Double tangible;
                Double rerata;
                //Reliability
                if (penilaianOutput.getReliabilityBean().getNilai() == null) {
                    reliability = 0.0;
                } else {
                    Double nilai = penilaianOutput.getReliabilityBean().getNilai();
                    Integer jumlah = penilaianOutput.getReliabilityBean().getJumlah();
                    reliability = nilai / jumlah;
                }
                //Responsiveness
                if (penilaianOutput2.getResponsivenessBean().getNilai() == null) {
                    responsiveness = 0.0;
                } else {
                    Double nilai = penilaianOutput2.getResponsivenessBean().getNilai();
                    Integer jumlah = penilaianOutput2.getResponsivenessBean().getJumlah();
                    responsiveness = nilai / jumlah;
                }
                //Assurance
                if (penilaianOutput3.getAssuranceBean().getNilai() == null) {
                    assurance = 0.0;
                } else {
                    Double nilai = penilaianOutput3.getAssuranceBean().getNilai();
                    Integer jumlah = penilaianOutput3.getAssuranceBean().getJumlah();
                    assurance = nilai / jumlah;
                }
                //Empathy
                if (penilaianOutput4.getEmpathyBean().getNilai() == null) {
                    empathy = 0.0;
                } else {
                    Double nilai = penilaianOutput4.getEmpathyBean().getNilai();
                    Integer jumlah = penilaianOutput4.getEmpathyBean().getJumlah();
                    empathy = nilai / jumlah;
                }
                //Tangible
                if (penilaianOutput5.getTangibleBean().getNilai() == null) {
                    tangible = 0.0;
                } else {
                    Double nilai = penilaianOutput5.getTangibleBean().getNilai();
                    Integer jumlah = penilaianOutput5.getTangibleBean().getJumlah();
                    tangible = nilai / jumlah;
                }
                rerata = (reliability + responsiveness + assurance + empathy + tangible) / 5;
                penilaian.setReliability(df.format(reliability));
                penilaian.setResponsiveness(df.format(responsiveness));
                penilaian.setAssurance(df.format(assurance));
                penilaian.setEmpathy(df.format(empathy));
                penilaian.setTangible(df.format(tangible));
                penilaian.setRerata(df.format(rerata));

                listPenilaian.add(penilaian);

                no++;
            } catch (Exception e) {
                penilaian = new PenilaianList();
            }
        }
        return listPenilaian;
    }

    @RequestMapping(value = "/kajurDashboard", method = RequestMethod.GET)
    public String dashboardKajur(HttpServletRequest request, Model model) {
        String username = (String) request.getSession().getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        Map<Integer, String> mapRole = new HashMap<Integer, String>();
        mapRole.put(loginOutput.getUserBean().getRole(), "Administrator");
        model.addAttribute("role", mapRole);

        //GET Mahasiswa
        MahasiswaServiceOutput mhsOutput = mahasiswaService.findAllMahasiswa();
        List<MahasiswaBean> mhsBean = mhsOutput.getListMahasiswaBean();
        model.addAttribute("totalMhs", mhsBean.size());

        //GET Dosen
        DosenServiceOutput dosenOutput = dosenService.findAllDosen();
        List<DosenBean> dosenBean = dosenOutput.getListDosenBean();
        model.addAttribute("totalDosen", dosenBean.size());

        //GET User
        LoginServiceOutput userOutput = loginService.findAllUser();
        List<UserBean> userBean = userOutput.getListUserBean();
        model.addAttribute("totalUser", userBean.size());

        //GET Perkuliahan
        PerkuliahanServiceOutput perkOutput = perkuliahanService.getAllPerkuliahan();
        List<PerkuliahanBean> perkBean = perkOutput.getListPerkuliahanBean();
        model.addAttribute("totalPerk", perkBean.size());

        model.addAttribute("tanggal", new Date());
        
        //Sort By and Grafik
        List<PenilaianList> listPenilaian = new ArrayList<PenilaianList>();
        listPenilaian = getListPenilaian();
        Map<Object,Object> mapPenilaian = null;
        List<Map<Object,Object>> dataPenilaian = new ArrayList<Map<Object, Object>>();
        List<List<Map<Object,Object>>> list = new ArrayList<List<Map<Object, Object>>>();
        for(PenilaianList penilaian:listPenilaian){
            mapPenilaian = new HashMap<Object,Object>();
            mapPenilaian.put("label", penilaian.getNamaDosen());
            mapPenilaian.put("y",penilaian.getRerata());
            //mapPenilaian.put("matkul",penilaian.getMatakuliah());
            dataPenilaian.add(mapPenilaian);
            
        }
        list.add(dataPenilaian);
        model.addAttribute("dataPenilaianList",list);
        Collections.sort(listPenilaian, new Comparator<PenilaianList>() {
            @Override
            public int compare(PenilaianList u1, PenilaianList u2) {
                return u2.getRerata().compareTo(u1.getRerata());
            }
        });
        return "kajurDashboard";
    }

    @RequestMapping(value = "/upload/2007", method = RequestMethod.POST)
    public String processExcel2007(Model model,
            @RequestParam("excelfile2007") MultipartFile excelfile, RedirectAttributes ra) {
        try {
            List<MahasiswaBean> listMhs = new ArrayList<MahasiswaBean>();
            int i = 1;
            // Creates a workbook object from the uploaded excelfile
            XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
            // Creates a worksheet object representing the first sheet
            XSSFSheet worksheet = workbook.getSheetAt(0);
            // Reads the data in excel file until last row is encountered
            Integer num = worksheet.getLastRowNum();
            while (i <= num) {
                MahasiswaBean bean = new MahasiswaBean();
                DataFormatter formatter = new DataFormatter();
                // Creates an object for the UserInfo Model

                // Creates an object representing a single row in excel
                XSSFRow row = worksheet.getRow(i++);
                // Sets the Read data to the model class
                //user.setId((int) row.getCell(0).getNumericCellValue());
                //user.setUsername(row.getCell(1).getStringCellValue());
                //user.setInputDate(row.getCell(2).getDateCellValue());
                bean.setNoBp(row.getCell(3).toString());
                bean.setNama(row.getCell(1).getStringCellValue());
                if (row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 1A") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 1B") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 1C") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 1D")) {
                    bean.setKelas(1);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 2A") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 2B") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 2C") ||
                         row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 2D")
                        ) {
                    bean.setKelas(2);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 3A") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 3B") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 3C") ||
                         row.getCell(4).getStringCellValue().equalsIgnoreCase("MI 3D")
                        ) {
                    bean.setKelas(3);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("TK 1A") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("TK 1B") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("TK 1C")
                        ) {
                    bean.setKelas(4);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("TK 2A") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("TK 2B") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("TK 2C")
                        ) {
                    bean.setKelas(5);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("TK 3A") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("TK 3B") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("TK 3C")
                        ) {
                    bean.setKelas(6);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 1A") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 1B") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 1C")
                        ) {
                    bean.setKelas(7);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 2A") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 2B") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 2C")
                        ) {
                    bean.setKelas(8);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 3A") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 3B") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 3C")
                        ) {
                    bean.setKelas(9);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 4A") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 4B") ||
                        row.getCell(4).getStringCellValue().equalsIgnoreCase("RPL 4C")
                        ) {
                    bean.setKelas(10);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("Alumni MI")) {
                    bean.setKelas(11);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("Alumni TK")) {
                    bean.setKelas(12);
                } else if (row.getCell(4).getStringCellValue().equalsIgnoreCase("Alumni RPL")) {
                    bean.setKelas(13);
                }
                //bean.setKelas((int)row.getCell(2).getNumericCellValue());
                bean.setLokal(row.getCell(4).getStringCellValue());
                if(row.getCell(5).getStringCellValue().equalsIgnoreCase("Terdaftar / Aktif") ||
                       row.getCell(5).getStringCellValue().equalsIgnoreCase("Aktif") ){
                     bean.setStatus(1);
                }else{
                    bean.setStatus(0);
                }
               
                // persist data into database in here
                listMhs.add(bean);
            }
            workbook.close();
            MahasiswaServiceInput input = new MahasiswaServiceInput();
            input.setListMhsBean(listMhs);
            MahasiswaServiceOutput output = mahasiswaService.insertListMhs(input);
            ra.addFlashAttribute("message", output.getMessage());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/mahasiswa/listMahasiswa";
    }
}
