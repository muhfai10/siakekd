/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.controller.login;

import com.muhfai.uploadtest.core.aspek.AspekBean;
import com.muhfai.uploadtest.core.kuesioner.KuesionerBean;
import com.muhfai.uploadtest.service.aspek.AspekService;
import com.muhfai.uploadtest.service.aspek.AspekServiceInput;
import com.muhfai.uploadtest.service.aspek.AspekServiceOutput;
import com.muhfai.uploadtest.service.kuesioner.KuesionerService;
import com.muhfai.uploadtest.service.kuesioner.KuesionerServiceInput;
import com.muhfai.uploadtest.service.kuesioner.KuesionerServiceOutput;
import com.muhfai.uploadtest.service.kuesioner.PertanyaanList;
import com.muhfai.uploadtest.service.login.LoginService;
import com.muhfai.uploadtest.service.login.LoginServiceInput;
import com.muhfai.uploadtest.service.login.LoginServiceOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author MuhFai10
 */
@Controller
@RequestMapping(value = "/pertanyaan")
public class PertanyaanController {

    private static final Logger logger = LoggerFactory
            .getLogger(PertanyaanController.class);
    @Autowired
    private KuesionerService kuesionerService;
    @Autowired
    private AspekService aspekService;
    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/listPertanyaan", method = RequestMethod.GET)
    public String listPertanyaan(Model model, HttpSession session, @RequestParam(required = false) Integer page) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        KuesionerServiceOutput kuesionerOutput = kuesionerService.findAllKuesioner();
        List<KuesionerBean> listPrtBean = kuesionerOutput.getListKuesionerBean();
        List<PertanyaanList> listPertanyaan = new ArrayList<PertanyaanList>();
        for (KuesionerBean kuesioner : listPrtBean) {
            PertanyaanList prt = new PertanyaanList();
            prt.setIdKuesioner(kuesioner.getIdKuesioner());
            prt.setPertanyaan(kuesioner.getPertanyaan());
            AspekServiceInput aspekInput = new AspekServiceInput();
            aspekInput.setIdAspek(kuesioner.getAspek());
            AspekServiceOutput aspekOutput = aspekService.findAspekById(aspekInput);
            prt.setAspek(aspekOutput.getAspekBean().getAspek());

            listPertanyaan.add(prt);
        }

        AspekServiceOutput aspekOutput = aspekService.findAllAspek();
        model.addAttribute("listAspek", aspekOutput.getListAspekBean());

        PagedListHolder<PertanyaanList> pageListHolder = new PagedListHolder<PertanyaanList>(listPertanyaan);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }

        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listPertanyaan", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listPertanyaan", pageListHolder.getPageList());
        }
        //model.addAttribute("listPertanyaan", kuesionerOutput.getListKuesionerBean());
        return "listPertanyaan";
    }

    @RequestMapping(value = "/searchByAspek")
    public String searchByAspek(Model model, @RequestParam("aspek") Integer aspek, @RequestParam(required = false) Integer page, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        AspekServiceOutput aspekOutput = aspekService.findAllAspek();
        model.addAttribute("listAspek", aspekOutput.getListAspekBean());

        KuesionerServiceInput kuesionerInput = new KuesionerServiceInput();
        kuesionerInput.setIdAspek(aspek);
        KuesionerServiceOutput kuesionerOutput = kuesionerService.findKuesionerByAspek(kuesionerInput);
        List<KuesionerBean> listPrtBean = kuesionerOutput.getListKuesionerBean();
        List<PertanyaanList> listPertanyaan = new ArrayList<PertanyaanList>();
        for (KuesionerBean kuesioner : listPrtBean) {
            PertanyaanList prt = new PertanyaanList();
            prt.setIdKuesioner(kuesioner.getIdKuesioner());
            prt.setPertanyaan(kuesioner.getPertanyaan());
            AspekServiceInput aspekInput = new AspekServiceInput();
            aspekInput.setIdAspek(kuesioner.getAspek());
            AspekServiceOutput aspekOutput2 = aspekService.findAspekById(aspekInput);
            prt.setAspek(aspekOutput2.getAspekBean().getAspek());

            listPertanyaan.add(prt);
        }

        PagedListHolder<PertanyaanList> pageListHolder = new PagedListHolder<PertanyaanList>(listPertanyaan);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }

        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listPertanyaan", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listPertanyaan", pageListHolder.getPageList());
        }
        //model.addAttribute("listPertanyaan", kuesionerOutput.getListKuesionerBean());
        return "listPertanyaan";

    }

    @RequestMapping(value = "/searchPertanyaan")
    public String searchPertanyaan(Model model, HttpSession session, @RequestParam("pertanyaan") String pertanyaan, @RequestParam(required = false) Integer page) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        AspekServiceOutput aspekOutput = aspekService.findAllAspek();
        model.addAttribute("listAspek", aspekOutput.getListAspekBean());

        KuesionerServiceInput kuesionerInput = new KuesionerServiceInput();
        kuesionerInput.setPertanyaan(pertanyaan);
        KuesionerServiceOutput kuesionerOutput = kuesionerService.findPertanyaan(kuesionerInput);
        List<KuesionerBean> listPrtBean = kuesionerOutput.getListKuesionerBean();
        List<PertanyaanList> listPertanyaan = new ArrayList<PertanyaanList>();
        for (KuesionerBean kuesioner : listPrtBean) {
            PertanyaanList prt = new PertanyaanList();
            prt.setIdKuesioner(kuesioner.getIdKuesioner());
            prt.setPertanyaan(kuesioner.getPertanyaan());
            AspekServiceInput aspekInput = new AspekServiceInput();
            aspekInput.setIdAspek(kuesioner.getAspek());
            AspekServiceOutput aspekOutput2 = aspekService.findAspekById(aspekInput);
            prt.setAspek(aspekOutput2.getAspekBean().getAspek());

            listPertanyaan.add(prt);
        }
        PagedListHolder<PertanyaanList> pageListHolder = new PagedListHolder<PertanyaanList>(listPertanyaan);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }

        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listPertanyaan", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listPertanyaan", pageListHolder.getPageList());
        }
        //model.addAttribute("listPertanyaan", kuesionerOutput.getListKuesionerBean());
        return "listPertanyaan";
    }

    @RequestMapping(value = "/newPertanyaan", method = RequestMethod.GET)
    public String newPertanyaan(Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        KuesionerBean pertanyaan = new KuesionerBean();
        AspekServiceOutput aspekOutput = aspekService.findAllAspek();
        List<AspekBean> listAspek = aspekOutput.getListAspekBean();
        Map<Integer, String> mapAspek = new HashMap<Integer, String>();
        for (AspekBean aspek : listAspek) {
            mapAspek.put(aspek.getIdAspek(), aspek.getAspek());
        }
        model.addAttribute("mapAspek", mapAspek);
        model.addAttribute("pertanyaan", pertanyaan);
        return "newPertanyaan";
    }

    @RequestMapping(value = "/editPertanyaan/{idKuesioner}", method = RequestMethod.GET)
    public String editPertanyaan(@PathVariable("idKuesioner") Integer idKuesioner,
            Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        KuesionerServiceInput input = new KuesionerServiceInput();
        input.setIdKuisioner(idKuesioner);
        AspekServiceOutput aspekOutput = aspekService.findAllAspek();
        List<AspekBean> listAspek = aspekOutput.getListAspekBean();
        Map<Integer, String> mapAspek = new HashMap<Integer, String>();
        for (AspekBean aspek : listAspek) {
            mapAspek.put(aspek.getIdAspek(), aspek.getAspek());
        }
        model.addAttribute("mapAspek", mapAspek);
        KuesionerServiceOutput output = kuesionerService.findKuesionerById(input);
        model.addAttribute("pertanyaan", output.getKuesionerBean());
        return "newPertanyaan";
    }

    @RequestMapping(value = "/deletePertanyaan/{idKuesioner}", method = RequestMethod.GET)
    public String deletePertanyaan(@PathVariable("idKuesioner") Integer idKuesioner,
            RedirectAttributes ra, HttpSession session, Model model) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        KuesionerServiceInput input = new KuesionerServiceInput();
        input.setIdKuisioner(idKuesioner);
        KuesionerServiceOutput output = kuesionerService.deleteKuesionerById(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/pertanyaan/listPertanyaan";
    }

    @RequestMapping(value = "/insertPertanyaan", method = RequestMethod.POST)
    public String insertPertanyaan(@ModelAttribute("pertanyaan") @Valid KuesionerBean kuesionerBean,
            BindingResult result,
            RedirectAttributes ra, Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        KuesionerServiceInput input = new KuesionerServiceInput();
        input.setKuesionerBean(kuesionerBean);
        if (result.hasErrors()) {
            AspekServiceOutput aspekOutput = aspekService.findAllAspek();
            List<AspekBean> listAspek = aspekOutput.getListAspekBean();
            Map<Integer, String> mapAspek = new HashMap<Integer, String>();
            for (AspekBean aspek : listAspek) {
                mapAspek.put(aspek.getIdAspek(), aspek.getAspek());
            }
            model.addAttribute("mapAspek", mapAspek);
            return "newPertanyaan";
        }
        KuesionerServiceOutput output = kuesionerService.insertKuesioner(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/pertanyaan/listPertanyaan";
    }
}
