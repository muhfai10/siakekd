/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.controller.login;

import com.muhfai.uploadtest.core.matakuliah.MatakuliahBean;
import com.muhfai.uploadtest.service.login.LoginService;
import com.muhfai.uploadtest.service.login.LoginServiceInput;
import com.muhfai.uploadtest.service.login.LoginServiceOutput;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahService;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahServiceInput;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahServiceOutput;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author MuhFai10
 */

@Controller
@RequestMapping(value = "/matakuliah")
public class MatakuliahController {
    private static final Logger logger = LoggerFactory
            .getLogger(MatakuliahController.class);
    @Autowired
    private MatakuliahService matkulService;
    @Autowired
    private LoginService loginService;
    
    @RequestMapping(value = "/listMatkul", method = RequestMethod.GET)
    public String listMatkul(Model model, @RequestParam(required = false) Integer page,HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        MatakuliahServiceOutput matkulOutput = matkulService.findAllMatakuliah();
        List<MatakuliahBean> listMatkul = matkulOutput.getListMatkulBean();
        PagedListHolder<MatakuliahBean> pageListHolder = new PagedListHolder<MatakuliahBean>(listMatkul);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }
        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listMatkul", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listMatkul", pageListHolder.getPageList());
        }
        //model.addAttribute("listMatkul", matkulOutput.getListMatkulBean());
        return "listMatkul";
    }
    
    @RequestMapping(value = "/searchMatkul")
    public String searchMatkul(Model model,HttpSession session ,@RequestParam("freeText") String freeText,@RequestParam(required = false) Integer page) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        MatakuliahServiceInput matkulInput = new MatakuliahServiceInput();
        matkulInput.setFreeText(freeText);
        MatakuliahServiceOutput matkulOutput = matkulService.findMatkul(matkulInput);
        List<MatakuliahBean> listMatkul = matkulOutput.getListMatkulBean();
        PagedListHolder<MatakuliahBean> pageListHolder = new PagedListHolder<MatakuliahBean>(listMatkul);
        pageListHolder.setPageSize(5);
        model.addAttribute("maxPages", pageListHolder.getPageCount());
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            page = 1;
        }
        model.addAttribute("page", page);
        if (page == null || page < 1 || page > pageListHolder.getPageCount()) {
            pageListHolder.setPage(0);
            model.addAttribute("listMatkul", pageListHolder.getPageList());
        } else if (page <= pageListHolder.getPageCount()) {
            pageListHolder.setPage(page - 1);
            model.addAttribute("listMatkul", pageListHolder.getPageList());
        }
        //model.addAttribute("listDosen", dosenOutput.getListDosenBean());
        return "listMatkul";
    }
    
    @RequestMapping(value = "/newMatkul", method = RequestMethod.GET)
    public String newMatkul(Model model,HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        MatakuliahBean matakuliah = new MatakuliahBean();
        model.addAttribute("matakuliah", matakuliah);
        return "newMatkul";
    }
    
    @RequestMapping(value = "/editMatkul/{kodeMk}", method = RequestMethod.GET)
    public String editMatkul(@PathVariable("kodeMk") String kodeMk,
            Model model,HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        MatakuliahServiceInput input = new MatakuliahServiceInput();
        input.setKodeMk(kodeMk);
        MatakuliahServiceOutput output = matkulService.findMatkulById(input);
        model.addAttribute("matakuliah", output.getMatakuliahBean());
        return "editMatkul";
    }
    
    @RequestMapping(value = "/updateMatkul", method = RequestMethod.POST)
    public String updateMatkul(@ModelAttribute("matakuliah") @Valid MatakuliahBean matakuliahBean, BindingResult result,
            RedirectAttributes ra,HttpSession session,Model model) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        MatakuliahServiceInput input = new MatakuliahServiceInput();
        input.setMatakuliahBean(matakuliahBean);
        if (result.hasErrors()) {
            return "editMatkul";
        }
        MatakuliahServiceOutput output = matkulService.updateMatakuliah(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/matakuliah/listMatkul";
    }
    
     @RequestMapping(value = "/deleteMatkul/{kodeMk}", method = RequestMethod.GET)
    public String deleteMatkul(@PathVariable("kodeMk") String kodeMk,
            RedirectAttributes ra,HttpSession session,Model model) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        MatakuliahServiceInput input = new MatakuliahServiceInput();
        input.setKodeMk(kodeMk);
        MatakuliahServiceOutput output = matkulService.deleteMatakuliahById(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/matakuliah/listMatkul";
    }
    
    @RequestMapping(value = "/insertMatkul", method = RequestMethod.POST)
    public String insertMatkul(@ModelAttribute("matakuliah") @Valid MatakuliahBean matakuliahBean,
             BindingResult result,RedirectAttributes ra,HttpSession session,Model model) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        MatakuliahServiceInput input = new MatakuliahServiceInput();
        input.setMatakuliahBean(matakuliahBean);
        if (result.hasErrors()) {
            return "newMatkul";
        }
        MatakuliahServiceOutput output = matkulService.insertMatakuliah(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/matakuliah/listMatkul";
    }
}
