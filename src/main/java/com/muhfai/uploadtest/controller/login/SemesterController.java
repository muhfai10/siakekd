/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.controller.login;

import com.muhfai.uploadtest.core.semester.SemesterBean;
import com.muhfai.uploadtest.service.login.LoginService;
import com.muhfai.uploadtest.service.login.LoginServiceInput;
import com.muhfai.uploadtest.service.login.LoginServiceOutput;
import com.muhfai.uploadtest.service.semester.SemesterList;
import com.muhfai.uploadtest.service.semester.SemesterService;
import com.muhfai.uploadtest.service.semester.SemesterServiceInput;
import com.muhfai.uploadtest.service.semester.SemesterServiceOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author MuhFai10
 */
@Controller
@RequestMapping(value = "/semester")
public class SemesterController {

    private static final Logger logger = LoggerFactory
            .getLogger(SemesterController.class);
    @Autowired
    private SemesterService semesterService;
    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/listSemester", method = RequestMethod.GET)
    public String listSemester(Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        SemesterServiceOutput semesterOutput = semesterService.findAllSemester();
        List<SemesterBean> listSmtBean = semesterOutput.getListSemesterBean();
        List<SemesterList> listSemester = new ArrayList<SemesterList>();
        for (SemesterBean smtBean : listSmtBean) {
            SemesterList semester = new SemesterList();
            SemesterServiceInput semesterInput = new SemesterServiceInput();
            semesterInput.setIdSemester(smtBean.getIdSemester());
            SemesterServiceOutput semesterOutput2 = semesterService.findSemesterById(semesterInput);
            semester.setIdSemester(semesterOutput2.getSemesterBean().getIdSemester());
            semester.setSemester(semesterOutput2.getSemesterBean().getSemester());
            semester.setTahunAjaran(semesterOutput2.getSemesterBean().getTahunAjaran());
            if (semesterOutput2.getSemesterBean().getStatus() == 0) {
                semester.setStatus("Not Active");
            } else if (semesterOutput2.getSemesterBean().getStatus() == 1) {
                semester.setStatus("Active");
            }
            listSemester.add(semester);
        }
        model.addAttribute("listSemester", listSemester);
        return "listSemester";
    }
    
    @RequestMapping(value = "/newSemester", method = RequestMethod.GET)
    public String newSemester(Model model, HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());

        Map<Integer, String> statusMap = new HashMap<Integer, String>();
        statusMap.put(1, "Active");
        statusMap.put(0, "Not Active");
        model.addAttribute("mapStatus", statusMap);
        SemesterBean semester = new SemesterBean();
        model.addAttribute("semester", semester);
        return "newSemester";
    }

    @RequestMapping(value = "/editSemester/{idSemester}", method = RequestMethod.GET)
    public String editSemester(@PathVariable("idSemester") Integer idSemester,
            Model model,HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        SemesterServiceInput input = new SemesterServiceInput();
        input.setIdSemester(idSemester);
        Map<Integer, String> statusMap = new HashMap<Integer, String>();
        statusMap.put(1, "Active");
        statusMap.put(0, "Not Active");
        SemesterServiceOutput output = semesterService.findSemesterById(input);
        model.addAttribute("mapStatus", statusMap);
        model.addAttribute("semester", output.getSemesterBean());
        return "editSemester";
    }
    
    @RequestMapping(value = "/insertSemester", method = RequestMethod.POST)
    public String insertSemester(@ModelAttribute("semester") @Valid SemesterBean semesterBean,
            BindingResult result,
            RedirectAttributes ra, Model model,HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        SemesterServiceInput input = new SemesterServiceInput();
        input.setSemesterBean(semesterBean);
        if (result.hasErrors()) {
            Map<Integer, String> statusMap = new HashMap<Integer, String>();
            statusMap.put(1, "Active");
            statusMap.put(0, "Not Active");
            model.addAttribute("mapStatus", statusMap);
            return "newSemester";
        }
        SemesterServiceOutput output = semesterService.insertSemester(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/semester/listSemester";
    }

    @RequestMapping(value = "/updateSemester", method = RequestMethod.POST)
    public String updateSemester(@ModelAttribute("semester") @Valid SemesterBean semesterBean,
            BindingResult result,
            RedirectAttributes ra, Model model,HttpSession session) {
        String username = (String) session.getAttribute("username");
        LoginServiceInput loginInput = new LoginServiceInput();
        loginInput.setUsername(username);
        LoginServiceOutput loginOutput = loginService.findByUser(loginInput);
        model.addAttribute("username", loginOutput.getUserBean().getUsername());
        model.addAttribute("nama", loginOutput.getUserBean().getNama());
        
        SemesterServiceInput input = new SemesterServiceInput();
        input.setSemesterBean(semesterBean);
        if (result.hasErrors()) {
            Map<Integer, String> statusMap = new HashMap<Integer, String>();
            statusMap.put(1, "Active");
            statusMap.put(0, "Not Active");
            model.addAttribute("mapStatus", statusMap);
            return "editSemester";
        }
        SemesterServiceOutput output = semesterService.updateSemester(input);
        ra.addFlashAttribute("message", output.getMessage());
        return "redirect:/semester/listSemester";
    }
}
