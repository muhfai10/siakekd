/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.customrepository;

import com.muhfai.uploadtest.customdto.SemesterCustomDto;
import com.muhfai.uploadtest.util.NullChecker;
import com.muhfai.uploadtest.util.ObjectConverter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.javalite.activejdbc.DB;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class SemesterCustomRepository implements AbstractCustomRepository<Map, SemesterCustomDto> {

    public List<SemesterCustomDto> getSemesterInPerkuliahan() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT semester.id_semester,semester.semester,semester.tahun_ajaran,semester.status ");
        sb.append("FROM perkuliahan ");
        sb.append("JOIN matakuliah ON perkuliahan.kode_mk = matakuliah.kode_mk ");
        sb.append("JOIN semester ON semester.id_semester = perkuliahan.id_semester ");
        sb.append("GROUP BY semester.semester;");

        List<Map> mapList = new DB().findAll(sb.toString());
        List<SemesterCustomDto> dtoList = convertToDto(mapList);
        return dtoList;
    }

    @Override
    public SemesterCustomDto convertToDto(Map map) {
        SemesterCustomDto dto = null;
        if (NullChecker.notNull(map)) {
            dto = new SemesterCustomDto();
            dto.setIdSemester(ObjectConverter.toInteger(map.get("id_semester")));
            dto.setSemester(ObjectConverter.toString(map.get("semester")));
            dto.setTahunAjaran(ObjectConverter.toString(map.get("tahun_ajaran")));
            dto.setStatus(ObjectConverter.toInteger(map.get("status")));
        }
        return dto;
    }

    @Override
    public List<SemesterCustomDto> convertToDto(List<Map> modelList) {
        List<SemesterCustomDto> result = null;
        if (NullChecker.notNull(modelList)) {
            result = new ArrayList<SemesterCustomDto>();
            for (Map map : modelList) {
                SemesterCustomDto dto = convertToDto(map);
                result.add(dto);
            }
        }
        return result;
    }

    @Override
    public Map convertFromDto(SemesterCustomDto dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Map> convertFromDto(List<SemesterCustomDto> dtoList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
