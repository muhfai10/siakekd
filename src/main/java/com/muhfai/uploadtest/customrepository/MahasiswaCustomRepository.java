/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.customrepository;

import com.muhfai.uploadtest.customdto.MhsCustomDto;
import com.muhfai.uploadtest.util.NullChecker;
import com.muhfai.uploadtest.util.ObjectConverter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.javalite.activejdbc.DB;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class MahasiswaCustomRepository implements AbstractCustomRepository<Map, MhsCustomDto> {

    public MhsCustomDto getMahasiswa(String noBp) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT mahasiswa.no_bp,mahasiswa.nama,mahasiswa.lokal,semester.semester,semester.tahun_ajaran ");
        sb.append("FROM mahasiswa ");
        sb.append("JOIN perkuliahan ON mahasiswa.kelas = perkuliahan.id_kelas ");
        sb.append("JOIN semester ON perkuliahan.id_semester = semester.id_semester ");
        sb.append("WHERE mahasiswa.no_bp = '").append(noBp).append("';");
        
        List<Map> mapList = new DB().findAll(sb.toString());  
        List<MhsCustomDto> dtoList = convertToDto(mapList);
        return dtoList.get(0);
    }

    @Override
    public MhsCustomDto convertToDto(Map map) {
        MhsCustomDto dto = null;
        if (NullChecker.notNull(map)) {
            dto = new MhsCustomDto();
            dto.setNoBp(ObjectConverter.toString(map.get("no_bp")));
            dto.setNama(ObjectConverter.toString(map.get("nama")));
            dto.setLokal(ObjectConverter.toString(map.get("lokal")));
            dto.setSemester(ObjectConverter.toString(map.get("semester")));
            dto.setTahunAjaran(ObjectConverter.toString(map.get("tahun_ajaran")));
        }
        return dto;
    }

    @Override
    public List<MhsCustomDto> convertToDto(List<Map> modelList) {
        List<MhsCustomDto> result = null;
        if (NullChecker.notNull(modelList)) {
            result = new ArrayList<MhsCustomDto>();
            for (Map map : modelList) {
                MhsCustomDto dto = convertToDto(map);
                result.add(dto);
            }
        }
        return result;
    }

    @Override
    public Map convertFromDto(MhsCustomDto dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Map> convertFromDto(List<MhsCustomDto> dtoList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
