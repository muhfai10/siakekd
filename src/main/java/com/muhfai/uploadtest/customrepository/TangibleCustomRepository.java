/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.customrepository;

import com.muhfai.uploadtest.customdto.TangibleDto;
import com.muhfai.uploadtest.util.NullChecker;
import com.muhfai.uploadtest.util.ObjectConverter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.javalite.activejdbc.DB;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class TangibleCustomRepository implements AbstractCustomRepository<Map, TangibleDto>{

    public TangibleDto getTangible(String kodeDosen,Integer idPerkuliahan){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT SUM(survey_detail.nilai) nilai ,COUNT(survey_detail.nilai) jumlah FROM survey_detail ");
        sb.append("JOIN survey ON survey_detail.id_survey = survey.id_survey ");
        sb.append("JOIN kuesioner ON survey_detail.id_kuesioner = kuesioner.id_kuesioner ");
        sb.append("JOIN aspek ON kuesioner.aspek = aspek.id_aspek ");
        sb.append("JOIN semester ON survey.semester = semester.id_semester ");
        sb.append("WHERE aspek.id_aspek = 5 AND survey.kode_dosen = '").append(kodeDosen).append("' AND survey.id_perkuliahan = ").append(idPerkuliahan).append(" AND semester.status=1;");
        
        //List<Map> mapList = Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/kuesioner_new", "root", "").findAll(sb.toString());
        List<Map> mapList = new DB().findAll(sb.toString());  
        List<TangibleDto> dtoList = convertToDto(mapList);
        return dtoList.get(0);
    }
    
    public TangibleDto getTangibleBySemester(String kodeDosen,Integer idPerkuliahan,Integer semester){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT SUM(survey_detail.nilai) nilai ,COUNT(survey_detail.nilai) jumlah FROM survey_detail ");
        sb.append("JOIN survey ON survey_detail.id_survey = survey.id_survey ");
        sb.append("JOIN kuesioner ON survey_detail.id_kuesioner = kuesioner.id_kuesioner ");
        sb.append("JOIN aspek ON kuesioner.aspek = aspek.id_aspek ");
        sb.append("WHERE aspek.id_aspek = 5 AND survey.kode_dosen = '").append(kodeDosen).append("' AND survey.id_perkuliahan = ").
                append(idPerkuliahan).append(" AND survey.semester = ").append(semester).append(";");
        
        //List<Map> mapList = Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/kuesioner_new", "root", "").findAll(sb.toString());
        List<Map> mapList = new DB().findAll(sb.toString());  
        List<TangibleDto> dtoList = convertToDto(mapList);
        return dtoList.get(0);
    }
    
    @Override
    public TangibleDto convertToDto(Map map) {
        TangibleDto dto = null;
        if(NullChecker.notNull(map)){
            dto = new TangibleDto();
            dto.setNilai(ObjectConverter.toDouble(map.get("nilai")));
            dto.setJumlah(ObjectConverter.toInteger(map.get("jumlah")));
        }
        return dto;
    }

    @Override
    public List<TangibleDto> convertToDto(List<Map> modelList) {
         List<TangibleDto> result = null;
        if (NullChecker.notNull(modelList)) {
            result = new ArrayList<TangibleDto>();
            for (Map map : modelList) {
                TangibleDto dto = convertToDto(map);
                result.add(dto);
            }
        }
        return result;
    }

    @Override
    public Map convertFromDto(TangibleDto dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Map> convertFromDto(List<TangibleDto> dtoList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
