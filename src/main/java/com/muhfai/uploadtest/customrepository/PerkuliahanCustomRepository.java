/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.customrepository;

import com.muhfai.uploadtest.customdto.NewPerkuliahanDto;
import com.muhfai.uploadtest.util.NullChecker;
import com.muhfai.uploadtest.util.ObjectConverter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.DB;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class PerkuliahanCustomRepository implements AbstractCustomRepository<Map, NewPerkuliahanDto> {

    public List<NewPerkuliahanDto> getAllPerkuliahan(Integer idKelas){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT perkuliahan.id_perkuliahan,perkuliahan.id_kelas, ");
        sb.append("perkuliahan.kode_mk,perkuliahan.kode_dosen,perkuliahan.kode_dosen2, ");
        sb.append("perkuliahan.id_semester FROM perkuliahan JOIN ");
        sb.append("semester ON perkuliahan.id_semester = semester.id_semester ");
        sb.append("WHERE semester.status = 1 AND perkuliahan.id_kelas=").append(idKelas).append(";");
        
        //List<Map> mapList = Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/kuesioner_new", "root", "").findAll(sb.toString());
        List<Map> mapList = new DB().findAll(sb.toString());
        
        List<NewPerkuliahanDto> dtoList = convertToDto(mapList);
        return dtoList;
        
    }
    
    public List<NewPerkuliahanDto> getAllPerkuliahan2(){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT perkuliahan.id_perkuliahan,perkuliahan.id_kelas, ");
        sb.append("perkuliahan.kode_mk,perkuliahan.kode_dosen,perkuliahan.kode_dosen2, ");
        sb.append("perkuliahan.id_semester FROM perkuliahan JOIN ");
        sb.append("semester ON perkuliahan.id_semester = semester.id_semester ");
        sb.append("WHERE semester.status = 1");
        
        //List<Map> mapList = Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/kuesioner_new", "root", "").findAll(sb.toString());
        List<Map> mapList = new DB().findAll(sb.toString());
        
        List<NewPerkuliahanDto> dtoList = convertToDto(mapList);
        return dtoList;
        
    }
    
    public List<NewPerkuliahanDto> getAllPerkuliahan3(Integer idProdi){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT perkuliahan.id_perkuliahan,perkuliahan.id_kelas, ");
        sb.append("perkuliahan.kode_mk,perkuliahan.kode_dosen,perkuliahan.kode_dosen2, ");
        sb.append("perkuliahan.id_semester FROM perkuliahan JOIN ");
        sb.append("semester ON perkuliahan.id_semester = semester.id_semester ");
        sb.append("JOIN kelas ON perkuliahan.id_kelas = kelas.id_kelas ");
        sb.append("JOIN prodi ON kelas.prodi = prodi.id_prodi ");
        sb.append("WHERE prodi.id_prodi = ").append(idProdi).append(";");
        
        //List<Map> mapList = Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/kuesioner_new", "root", "").findAll(sb.toString());
        List<Map> mapList = new DB().findAll(sb.toString());
        
        List<NewPerkuliahanDto> dtoList = convertToDto(mapList);
        return dtoList;
        
    }
    
    public List<NewPerkuliahanDto> getPerkuliahanBySemester(Integer idSemester){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT perkuliahan.id_perkuliahan,perkuliahan.id_kelas,perkuliahan.kode_mk, ");
        sb.append("perkuliahan.kode_dosen,perkuliahan.kode_dosen2,perkuliahan.id_semester ");
        sb.append("FROM perkuliahan ");
        sb.append("JOIN semester ON perkuliahan.id_semester = semester.id_semester ");
        sb.append("WHERE semester.id_semester = ").append(idSemester).append(";");
        
        //List<Map> mapList = Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/kuesioner_new", "root", "").findAll(sb.toString());
        List<Map> mapList = new DB().findAll(sb.toString());
        List<NewPerkuliahanDto> dtoList = convertToDto(mapList);
        return dtoList;
        
    }
    
    @Override
    public NewPerkuliahanDto convertToDto(Map map) {
        NewPerkuliahanDto dto = null;
        if(NullChecker.notNull(map)){
            dto = new NewPerkuliahanDto();
            dto.setIdPerkuliahan(ObjectConverter.toInteger(map.get("id_perkuliahan")));
            dto.setIdKelas(ObjectConverter.toInteger(map.get("id_kelas")));
            dto.setKodeMk(ObjectConverter.toString(map.get("kode_mk")));
            dto.setKodeDosen(ObjectConverter.toString(map.get("kode_dosen")));
            dto.setKodeDosen2(ObjectConverter.toString(map.get("kode_dosen2")));
            dto.setIdSemester(ObjectConverter.toInteger(map.get("id_semester")));
        }
        return dto;
    }

    @Override
    public List<NewPerkuliahanDto> convertToDto(List<Map> modelList) {
        List<NewPerkuliahanDto> result = null;
        if (NullChecker.notNull(modelList)) {
            result = new ArrayList<NewPerkuliahanDto>();
            for (Map map : modelList) {
                NewPerkuliahanDto dto = convertToDto(map);
                result.add(dto);
            }
        }
        return result;
    }

    @Override
    public Map convertFromDto(NewPerkuliahanDto dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Map> convertFromDto(List<NewPerkuliahanDto> dtoList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
