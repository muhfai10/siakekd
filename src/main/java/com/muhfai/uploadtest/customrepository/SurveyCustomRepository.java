/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.customrepository;

import com.muhfai.uploadtest.customdto.SurveyCustomDto;
import com.muhfai.uploadtest.util.NullChecker;
import com.muhfai.uploadtest.util.ObjectConverter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.javalite.activejdbc.DB;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class SurveyCustomRepository implements AbstractCustomRepository<Map, SurveyCustomDto>{

    public List<SurveyCustomDto> getSurveyByProdi(Integer idProdi){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT survey.kode_dosen,survey.id_perkuliahan,survey.no_bp,survey.id_survey,survey.semester,survey.tanggal ");
        sb.append("FROM survey ");
        sb.append("JOIN perkuliahan ON survey.id_perkuliahan = perkuliahan.id_perkuliahan ");
        sb.append("JOIN kelas ON perkuliahan.id_kelas = kelas.id_kelas ");
        sb.append("JOIN prodi ON kelas.prodi = prodi.id_prodi ");
        sb.append("JOIN semester ON survey.semester = semester.id_semester ");
        sb.append("WHERE prodi.id_prodi = ").append(idProdi).append(" AND semester.status = 1 GROUP BY survey.kode_dosen,survey.id_perkuliahan;");
        
        //List<Map> mapList = Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/kuesioner_new", "root", "").findAll(sb.toString());
        List<Map> mapList = new DB().findAll(sb.toString());
        
        List<SurveyCustomDto> dtoList = convertToDto(mapList);
        return dtoList;
        
    }
    
    public List<SurveyCustomDto> getSurveyBySemester(Integer idProdi,Integer semester){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT survey.kode_dosen,survey.id_perkuliahan,survey.no_bp,survey.id_survey,survey.semester,survey.tanggal ");
        sb.append("FROM survey ");
        sb.append("JOIN perkuliahan ON survey.id_perkuliahan = perkuliahan.id_perkuliahan ");
        sb.append("JOIN kelas ON perkuliahan.id_kelas = kelas.id_kelas ");
        sb.append("JOIN prodi ON kelas.prodi = prodi.id_prodi ");
        sb.append("JOIN semester ON survey.semester = semester.id_semester ");
        sb.append("WHERE prodi.id_prodi = ").append(idProdi).append(" AND semester.id_semester = ").append(semester).append(" GROUP BY survey.kode_dosen,survey.id_perkuliahan;");
        
        //List<Map> mapList = Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/kuesioner_new", "root", "").findAll(sb.toString());
        List<Map> mapList = new DB().findAll(sb.toString());
        
        List<SurveyCustomDto> dtoList = convertToDto(mapList);
        return dtoList;
        
    }
    public List<SurveyCustomDto> getAllSurvey(){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT survey.kode_dosen,survey.id_perkuliahan,survey.no_bp,survey.id_survey,survey.semester,survey.tanggal ");
        sb.append("FROM survey ");
        sb.append("JOIN perkuliahan ON survey.id_perkuliahan = perkuliahan.id_perkuliahan ");
        sb.append("JOIN kelas ON perkuliahan.id_kelas = kelas.id_kelas ");
        sb.append("JOIN prodi ON kelas.prodi = prodi.id_prodi ");
        sb.append("JOIN semester ON survey.semester = semester.id_semester ");
        sb.append("WHERE semester.status=1 GROUP BY survey.kode_dosen,survey.id_perkuliahan");
        
        //List<Map> mapList = Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/kuesioner_new", "root", "").findAll(sb.toString());
        List<Map> mapList = new DB().findAll(sb.toString());
        
        List<SurveyCustomDto> dtoList = convertToDto(mapList);
        return dtoList;
    }
    @Override
    public SurveyCustomDto convertToDto(Map map) {
        SurveyCustomDto dto = null;
        if(NullChecker.notNull(map)){
            dto = new SurveyCustomDto();
            dto.setIdPerkuliahan(ObjectConverter.toInteger(map.get("id_perkuliahan")));
            dto.setIdSurvey(ObjectConverter.toInteger(map.get("id_survey")));
            dto.setKodeDosen(ObjectConverter.toString(map.get("kode_dosen")));
            dto.setTanggal(ObjectConverter.toDate(map.get("tanggal")));
            dto.setNoBp(ObjectConverter.toString(map.get("no_bp")));
            dto.setSemester(ObjectConverter.toInteger(map.get("semester")));
        }
        return dto;
    }

    @Override
    public List<SurveyCustomDto> convertToDto(List<Map> modelList) {
         List<SurveyCustomDto> result = null;
        if (NullChecker.notNull(modelList)) {
            result = new ArrayList<SurveyCustomDto>();
            for (Map map : modelList) {
                SurveyCustomDto dto = convertToDto(map);
                result.add(dto);
            }
        }
        return result;
    }

    @Override
    public Map convertFromDto(SurveyCustomDto dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Map> convertFromDto(List<SurveyCustomDto> dtoList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
