/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.customrepository;

import com.muhfai.uploadtest.customdto.ProdiCustomDto;
import com.muhfai.uploadtest.util.NullChecker;
import com.muhfai.uploadtest.util.ObjectConverter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.javalite.activejdbc.DB;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class ProdiCustomRepository implements AbstractCustomRepository<Map, ProdiCustomDto>{

    public ProdiCustomDto getProdi(Integer idProdi){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT prodi.prodi, semester.semester, semester.tahun_ajaran FROM perkuliahan ");
        sb.append("JOIN semester ON perkuliahan.id_semester = semester.id_semester ");
        sb.append("JOIN kelas ON perkuliahan.id_kelas = kelas.id_kelas ");
        sb.append("JOIN prodi ON prodi.id_prodi = kelas.prodi ");
        sb.append("WHERE semester.status = 1 AND prodi.id_prodi = ").append(idProdi).append(" GROUP BY prodi.prodi;");
        
        //List<Map> mapList = Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/kuesioner_new", "root", "").findAll(sb.toString());
        List<Map> mapList = new DB().findAll(sb.toString());  
        List<ProdiCustomDto> dtoList = convertToDto(mapList);
        return dtoList.get(0);
    }
    
    public ProdiCustomDto getProdiBySemester(Integer idProdi,Integer idSemester){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT prodi.prodi, semester.semester, semester.tahun_ajaran FROM perkuliahan ");
        sb.append("JOIN semester ON perkuliahan.id_semester = semester.id_semester ");
        sb.append("JOIN kelas ON perkuliahan.id_kelas = kelas.id_kelas ");
        sb.append("JOIN prodi ON prodi.id_prodi = kelas.prodi ");
        sb.append("WHERE prodi.id_prodi = ").append(idProdi).append(" AND semester.id_semester = ").append(idSemester).append(" GROUP BY prodi.prodi;");
        
        //List<Map> mapList = Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/kuesioner_new", "root", "").findAll(sb.toString());
        List<Map> mapList = new DB().findAll(sb.toString());  
        List<ProdiCustomDto> dtoList = convertToDto(mapList);
        return dtoList.get(0);
    }
   
    @Override
    public ProdiCustomDto convertToDto(Map map) {
        ProdiCustomDto dto = null;
        if(NullChecker.notNull(map)){
            dto = new ProdiCustomDto();
            dto.setProdi(ObjectConverter.toString(map.get("prodi")));
            dto.setSemester(ObjectConverter.toString(map.get("semester")));
            dto.setTahunAjaran(ObjectConverter.toString(map.get("tahun_ajaran")));
        }
        return dto;
    }

    @Override
    public List<ProdiCustomDto> convertToDto(List<Map> modelList) {
        List<ProdiCustomDto> result = null;
        if (NullChecker.notNull(modelList)) {
            result = new ArrayList<ProdiCustomDto>();
            for (Map map : modelList) {
                ProdiCustomDto dto = convertToDto(map);
                result.add(dto);
            }
        }
        return result;
    }

    @Override
    public Map convertFromDto(ProdiCustomDto dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Map> convertFromDto(List<ProdiCustomDto> dtoList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
