/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muhfai.uploadtest.customrepository;

import java.util.List;

/**
 *
 * @author Mohammad.Alghifari
 * @param <Map>
 * @param <CustomDto>
 */
public interface AbstractCustomRepository<Map, CustomDto> {
    public CustomDto convertToDto(Map map);
    public List<CustomDto> convertToDto(List<Map> modelList);
    public Map convertFromDto(CustomDto dto);
    public List<Map> convertFromDto(List<CustomDto> dtoList);
}
