/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest;

import com.muhfai.uploadtest.core.login.LoginBean;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author MuhFai10
 */
public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = LoggerFactory.getLogger(AuthenticationInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("Interceptor: Pre-handle");

        // Avoid a redirect loop for some urls
        if (!request.getRequestURI().equals("/uploadTest/") &&
                !request.getRequestURI().equals("/uploadTest/login/loginAccess")) {
            String userData = (String) request.getSession().getAttribute("username");
            if (userData == null) {
                response.sendRedirect("/uploadTest/");
                return false;
            }
        }
        return true;

    }

    @Override
    public void postHandle(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, ModelAndView mav) throws Exception {
        log.debug("Post-handle");
    }

    @Override
    public void afterCompletion(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, Exception excptn) throws Exception {
        log.debug("After-completion");
    }

}
