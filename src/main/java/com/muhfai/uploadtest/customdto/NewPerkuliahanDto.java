/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.customdto;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class NewPerkuliahanDto {
    private Integer idPerkuliahan;
    private Integer idKelas;
    private String kodeMk;
    private String kodeDosen;
    private String kodeDosen2;
    private Integer idSemester;
}
