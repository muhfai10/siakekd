/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.customdto;

import java.util.Date;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class SurveyCustomDto {
    private String kodeDosen;
    private Integer idPerkuliahan;
    private String noBp;
    private Date tanggal;
    private Integer idSurvey;
    private Integer semester;
}
