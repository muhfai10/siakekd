/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.kuesioner;

import com.muhfai.uploadtest.core.kuesioner.KuesionerBean;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class KuesionerServiceInput {
    private KuesionerBean kuesionerBean;
    private String pertanyaan;
    private Integer idKuisioner;
    private Integer idAspek;
}
