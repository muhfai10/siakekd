/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.kuesioner;

import com.muhfai.uploadtest.core.kuesioner.KuesionerBean;
import com.muhfai.uploadtest.core.kuesioner.KuesionerManager;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("kuesionerService")
public class KuesionerService {

    private final KuesionerManager kuesionerManager = new KuesionerManager();

    public KuesionerServiceOutput findAllKuesioner() {
        KuesionerServiceOutput kuesionerServiceOutput = new KuesionerServiceOutput();
        List<KuesionerBean> listKuesionerBean = kuesionerManager.findAllKuesioner();
        kuesionerServiceOutput.setListKuesionerBean(listKuesionerBean);

        return kuesionerServiceOutput;
    }

    public KuesionerServiceOutput findPertanyaan(KuesionerServiceInput input) {
        KuesionerServiceOutput output = new KuesionerServiceOutput();
        List<KuesionerBean> listKuesionerBean = kuesionerManager.findPertanyaan(input.getPertanyaan());
        output.setListKuesionerBean(listKuesionerBean);

        return output;
    }

    public KuesionerServiceOutput findKuesionerByAspek(KuesionerServiceInput input) {
        KuesionerServiceOutput output = new KuesionerServiceOutput();
        List<KuesionerBean> listKuesionerBean = kuesionerManager.findKuesionerByAspek(input.getIdAspek());
        output.setListKuesionerBean(listKuesionerBean);

        return output;
    }

    public KuesionerServiceOutput findKuesionerById(KuesionerServiceInput kuesionerServiceInput) {
        KuesionerServiceOutput kuesionerServiceOutput = new KuesionerServiceOutput();
        KuesionerBean kuesionerBean = kuesionerManager.findKuesionerById(kuesionerServiceInput.getIdKuisioner());
        kuesionerServiceOutput.setKuesionerBean(kuesionerBean);

        return kuesionerServiceOutput;
    }

    public KuesionerServiceOutput insertKuesioner(KuesionerServiceInput input) {
        KuesionerServiceOutput output = new KuesionerServiceOutput();

        // Validation
        KuesionerBean kuesionerBean = input.getKuesionerBean();
        if (kuesionerBean.getPertanyaan() == null || kuesionerBean.getPertanyaan().isEmpty()) {
            output.setMessage("Pertanyaan is mandatory");
            return output;
        }
        kuesionerManager.insertKuesioner(kuesionerBean);
        output.setMessage(kuesionerManager.getManagerResult().getMessage());

        return output;
    }

    public KuesionerServiceOutput deleteKuesionerById(KuesionerServiceInput input) {
        KuesionerServiceOutput output = new KuesionerServiceOutput();
        kuesionerManager.deleteKuesionerById(input.getIdKuisioner());
        output.setMessage(kuesionerManager.getManagerResult().getMessage());

        return output;
    }
}
