/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.kuesioner;

import com.muhfai.uploadtest.core.kuesioner.KuesionerBean;
import java.util.List;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class KuesionerServiceOutput {
    private String message;
    private KuesionerBean kuesionerBean;
    private List<KuesionerBean> listKuesionerBean;
}
