/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.prodi;

import com.muhfai.uploadtest.core.prodi.ProdiBean;
import com.muhfai.uploadtest.core.prodi.ProdiManager;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("prodiService")
public class ProdiService {
    private final ProdiManager prodiManager = new ProdiManager();
    public ProdiServiceOutput findAllProdi(){
        ProdiServiceOutput prodiServiceOutput = new ProdiServiceOutput();
        List<ProdiBean> listProdiBean = prodiManager.findAllProdi();
        prodiServiceOutput.setListProdiBean(listProdiBean);
       
        return prodiServiceOutput;
    }
    public ProdiServiceOutput findProdiById(ProdiServiceInput prodiServiceInput){
        ProdiServiceOutput prodiServiceOutput = new ProdiServiceOutput();
        ProdiBean prodiBean = prodiManager.findProdiById(prodiServiceInput.getIdProdi());
        prodiServiceOutput.setProdiBean(prodiBean);
       
        return prodiServiceOutput;
    }
}
