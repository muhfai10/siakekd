/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.prodi;

import com.muhfai.uploadtest.core.prodi.ProdiBean;
import java.util.List;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class ProdiServiceOutput {
    private String message;
    private ProdiBean prodiBean;
    private List<ProdiBean> listProdiBean;
}
