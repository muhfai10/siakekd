/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.aspek;

import com.muhfai.uploadtest.core.aspek.AspekBean;
import java.util.List;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class AspekServiceOutput {
    private String message;
    private AspekBean aspekBean;
    private List<AspekBean> listAspekBean;
}
