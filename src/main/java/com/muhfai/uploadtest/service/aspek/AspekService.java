/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.aspek;

import com.muhfai.uploadtest.core.aspek.AspekBean;
import com.muhfai.uploadtest.core.aspek.AspekManager;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("aspekService")
public class AspekService {
    private final AspekManager aspekManager = new AspekManager();
    public AspekServiceOutput findAllAspek(){
        AspekServiceOutput aspekServiceOutput = new AspekServiceOutput();
        List<AspekBean> listAspekBean = aspekManager.findAllAspek();
        aspekServiceOutput.setListAspekBean(listAspekBean);
       
        return aspekServiceOutput;
    }
    public AspekServiceOutput findAspekById(AspekServiceInput aspekServiceInput){
        AspekServiceOutput aspekServiceOutput = new AspekServiceOutput();
        AspekBean aspekBean = aspekManager.findAspekById(aspekServiceInput.getIdAspek());
        aspekServiceOutput.setAspekBean(aspekBean);
       
        return aspekServiceOutput;
    }
}
