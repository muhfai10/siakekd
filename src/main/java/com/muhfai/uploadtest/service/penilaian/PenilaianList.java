/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.penilaian;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class PenilaianList {
    private Integer no;
    private String namaDosen;
    private String matakuliah;
    private String reliability;
    private String responsiveness;
    private String assurance;
    private String empathy;
    private String tangible;
    private String rerata;
}
