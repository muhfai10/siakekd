/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.penilaian;

import com.muhfai.uploadtest.core.penilaian.AssuranceBean;
import com.muhfai.uploadtest.core.penilaian.EmpathyBean;
import com.muhfai.uploadtest.core.penilaian.PenilaianManager;
import com.muhfai.uploadtest.core.penilaian.ReliabilityBean;
import com.muhfai.uploadtest.core.penilaian.ResponsivenessBean;
import com.muhfai.uploadtest.core.penilaian.TangibleBean;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("penilaianService")
public class PenilaianService {

    private final PenilaianManager penilaianManager = new PenilaianManager();

    public PenilaianServiceOutput findReliability(PenilaianServiceInput input) {
        PenilaianServiceOutput output = new PenilaianServiceOutput();
        ReliabilityBean reliabilityBean = penilaianManager.findReliability(input.getKodeDosen(), input.getIdPerkuliahan());
        output.setReliabilityBean(reliabilityBean);

        return output;
    }

    public PenilaianServiceOutput findReliabilityBySemester(PenilaianServiceInput input) {
        PenilaianServiceOutput output = new PenilaianServiceOutput();
        ReliabilityBean reliabilityBean = penilaianManager.findReliabilityBySemester(input.getKodeDosen(), input.getIdPerkuliahan(), input.getSemester());
        output.setReliabilityBean(reliabilityBean);

        return output;
    }

    public PenilaianServiceOutput findResponsiveness(PenilaianServiceInput input) {
        PenilaianServiceOutput output = new PenilaianServiceOutput();
        ResponsivenessBean responsivenessBean = penilaianManager.findResponsiveness(input.getKodeDosen(), input.getIdPerkuliahan());
        output.setResponsivenessBean(responsivenessBean);

        return output;
    }

    public PenilaianServiceOutput findResponsivenessBySemester(PenilaianServiceInput input) {
        PenilaianServiceOutput output = new PenilaianServiceOutput();
        ResponsivenessBean responsivenessBean = penilaianManager.findResponsivenessBySemester(input.getKodeDosen(), input.getIdPerkuliahan(), input.getSemester());
        output.setResponsivenessBean(responsivenessBean);

        return output;
    }

    public PenilaianServiceOutput findAssurance(PenilaianServiceInput input) {
        PenilaianServiceOutput output = new PenilaianServiceOutput();
        AssuranceBean assuranceBean = penilaianManager.findAssurance(input.getKodeDosen(), input.getIdPerkuliahan());
        output.setAssuranceBean(assuranceBean);

        return output;
    }

    public PenilaianServiceOutput findAssuranceBySemester(PenilaianServiceInput input) {
        PenilaianServiceOutput output = new PenilaianServiceOutput();
        AssuranceBean assuranceBean = penilaianManager.findAssuranceBySemester(input.getKodeDosen(), input.getIdPerkuliahan(), input.getSemester());
        output.setAssuranceBean(assuranceBean);

        return output;
    }

    public PenilaianServiceOutput findEmpathy(PenilaianServiceInput input) {
        PenilaianServiceOutput output = new PenilaianServiceOutput();
        EmpathyBean empathyBean = penilaianManager.findEmpathy(input.getKodeDosen(), input.getIdPerkuliahan());
        output.setEmpathyBean(empathyBean);

        return output;
    }

    public PenilaianServiceOutput findEmpathyBySemester(PenilaianServiceInput input) {
        PenilaianServiceOutput output = new PenilaianServiceOutput();
        EmpathyBean empathyBean = penilaianManager.findEmpathyBySemester(input.getKodeDosen(), input.getIdPerkuliahan(), input.getSemester());
        output.setEmpathyBean(empathyBean);

        return output;
    }

    public PenilaianServiceOutput findTangible(PenilaianServiceInput input) {
        PenilaianServiceOutput output = new PenilaianServiceOutput();
        TangibleBean tangibleBean = penilaianManager.findTangible(input.getKodeDosen(), input.getIdPerkuliahan());
        output.setTangibleBean(tangibleBean);

        return output;
    }

    public PenilaianServiceOutput findTangibleBySemester(PenilaianServiceInput input) {
        PenilaianServiceOutput output = new PenilaianServiceOutput();
        TangibleBean tangibleBean = penilaianManager.findTangibleBySemester(input.getKodeDosen(), input.getIdPerkuliahan(), input.getSemester());
        output.setTangibleBean(tangibleBean);

        return output;
    }
}
