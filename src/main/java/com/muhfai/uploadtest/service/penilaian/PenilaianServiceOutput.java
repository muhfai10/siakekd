/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.penilaian;

import com.muhfai.uploadtest.core.penilaian.AssuranceBean;
import com.muhfai.uploadtest.core.penilaian.EmpathyBean;
import com.muhfai.uploadtest.core.penilaian.ReliabilityBean;
import com.muhfai.uploadtest.core.penilaian.ResponsivenessBean;
import com.muhfai.uploadtest.core.penilaian.TangibleBean;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class PenilaianServiceOutput {
    private String message;
    private ReliabilityBean reliabilityBean;
    private ResponsivenessBean responsivenessBean;
    private AssuranceBean assuranceBean;
    private EmpathyBean empathyBean;
    private TangibleBean tangibleBean;
}
