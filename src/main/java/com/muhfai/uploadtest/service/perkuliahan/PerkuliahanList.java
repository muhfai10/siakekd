/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.perkuliahan;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class PerkuliahanList {
    private Integer idPerkuliahan;
    private String kelas;
    private String kodeMk;
    private String matakuliah;
    private String namaDosen;
    private String namaDosen2;
    private String semester;
}
