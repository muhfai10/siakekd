/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.perkuliahan;

import com.muhfai.uploadtest.core.perkuliahan.PerkuliahanBean;
import java.util.List;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class PerkuliahanServiceOutput {
    private String message;
    private PerkuliahanBean perkuliahanBean;
    private List<PerkuliahanBean> listPerkuliahanBean;
}
