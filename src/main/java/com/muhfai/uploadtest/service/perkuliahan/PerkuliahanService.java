/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.perkuliahan;

import com.muhfai.uploadtest.core.Message;
import com.muhfai.uploadtest.core.matakuliah.MatakuliahBean;
import com.muhfai.uploadtest.core.perkuliahan.PerkuliahanBean;
import com.muhfai.uploadtest.core.perkuliahan.PerkuliahanManager;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahService;
import com.muhfai.uploadtest.service.matakuliah.MatakuliahServiceOutput;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("perkuliahanService")
public class PerkuliahanService {

    private final PerkuliahanManager perkuliahanManager = new PerkuliahanManager();
    @Autowired
    MatakuliahService matakuliahService;

    public PerkuliahanServiceOutput findAllPerkuliahan(PerkuliahanServiceInput perkuliahanServiceInput) {
        PerkuliahanServiceOutput perkuliahanServiceOutput = new PerkuliahanServiceOutput();
        List<PerkuliahanBean> listPerkuliahanBean = perkuliahanManager.findAllPerkuliahan(perkuliahanServiceInput.getKelas());
        perkuliahanServiceOutput.setListPerkuliahanBean(listPerkuliahanBean);

        return perkuliahanServiceOutput;
    }

    //Tidak dipakai
    public PerkuliahanServiceOutput findAllPerkuliahan2() {
        PerkuliahanServiceOutput perkuliahanServiceOutput = new PerkuliahanServiceOutput();
        List<PerkuliahanBean> listPerkuliahanBean = perkuliahanManager.findAllPerkuliahan2();
        perkuliahanServiceOutput.setListPerkuliahanBean(listPerkuliahanBean);

        return perkuliahanServiceOutput;
    }

    public PerkuliahanServiceOutput findAllPerkuliahan3(PerkuliahanServiceInput input) {
        PerkuliahanServiceOutput perkuliahanServiceOutput = new PerkuliahanServiceOutput();
        List<PerkuliahanBean> listPerkuliahanBean = perkuliahanManager.findAllPerkuliahan3(input.getIdProdi());
        perkuliahanServiceOutput.setListPerkuliahanBean(listPerkuliahanBean);

        return perkuliahanServiceOutput;
    }

    public PerkuliahanServiceOutput getPerkuliahanBySemester(PerkuliahanServiceInput input) {
        PerkuliahanServiceOutput perkuliahanServiceOutput = new PerkuliahanServiceOutput();
        List<PerkuliahanBean> listPerkuliahanBean = perkuliahanManager.getPerkuliahanBySemester(input.getIdSemester());
        perkuliahanServiceOutput.setListPerkuliahanBean(listPerkuliahanBean);
        perkuliahanServiceOutput.setMessage(perkuliahanManager.getManagerResult().getMessage());
        return perkuliahanServiceOutput;
    }

    public PerkuliahanServiceOutput findPerkuliahan(PerkuliahanServiceInput input) {
        PerkuliahanServiceOutput output = new PerkuliahanServiceOutput();
        List<PerkuliahanBean> listPerkuliahanBean = perkuliahanManager.findPerkuliahan(input.getFreeText());
        output.setListPerkuliahanBean(listPerkuliahanBean);

        return output;
    }

    public PerkuliahanServiceOutput getAllPerkuliahan() {
        PerkuliahanServiceOutput perkuliahanServiceOutput = new PerkuliahanServiceOutput();
        List<PerkuliahanBean> listPerkuliahanBean = perkuliahanManager.getAllPerkuliahan();
        perkuliahanServiceOutput.setListPerkuliahanBean(listPerkuliahanBean);

        return perkuliahanServiceOutput;
    }

    public PerkuliahanServiceOutput findPerkuliahanById(PerkuliahanServiceInput perkuliahanServiceInput) {
        PerkuliahanServiceOutput perkuliahanServiceOutput = new PerkuliahanServiceOutput();
        PerkuliahanBean perkuliahanBean = perkuliahanManager.findPerkuliahanById(perkuliahanServiceInput.getIdPerkuliahan());
        perkuliahanServiceOutput.setPerkuliahanBean(perkuliahanBean);

        return perkuliahanServiceOutput;
    }

    public PerkuliahanServiceOutput insertPerkuliahan(PerkuliahanServiceInput input) {
        PerkuliahanServiceOutput output = new PerkuliahanServiceOutput();

        // Validation
        PerkuliahanBean perkuliahanBean = input.getPerkuliahanBean();
        if (perkuliahanBean.getIdKelas() == null) {
            output.setMessage("Kelas is mandatory");
            return output;
        }
        if (perkuliahanBean.getKodeMk() == null || perkuliahanBean.getKodeMk().isEmpty()) {
            output.setMessage("Matakuliah is mandatory");
            return output;
        }
        if (perkuliahanBean.getKodeDosen() == null || perkuliahanBean.getKodeDosen().isEmpty()) {
            output.setMessage("Dosen 1 is mandatory");
            return output;
        }
        if (perkuliahanBean.getIdSemester() == null) {
            output.setMessage("Semester is mandatory");
            return output;
        }
        PerkuliahanServiceOutput perkOutput = getAllPerkuliahan();
        List<PerkuliahanBean> listMatkul = perkOutput.getListPerkuliahanBean();
        for (PerkuliahanBean matkul : listMatkul) {
            if (perkuliahanBean.getKodeMk().equals(matkul.getKodeMk())
                    && perkuliahanBean.getIdSemester().equals(matkul.getIdSemester())
                    && perkuliahanBean.getIdKelas().equals(matkul.getIdKelas())) {
                output.setMessage(Message.MATAKULIAH_FAILED);
                return output;
            }
        }
        perkuliahanManager.insertPerkuliahan(perkuliahanBean);
        output.setMessage(perkuliahanManager.getManagerResult().getMessage());

        return output;
    }

    public PerkuliahanServiceOutput updatePerkuliahan(PerkuliahanServiceInput input) {
        PerkuliahanServiceOutput output = new PerkuliahanServiceOutput();

        // Validation
        PerkuliahanBean perkuliahanBean = input.getPerkuliahanBean();
        if (perkuliahanBean.getIdKelas() == null) {
            output.setMessage("Kelas is mandatory");
            return output;
        }
        if (perkuliahanBean.getKodeMk() == null || perkuliahanBean.getKodeMk().isEmpty()) {
            output.setMessage("Matakuliah is mandatory");
            return output;
        }
        if (perkuliahanBean.getKodeDosen() == null || perkuliahanBean.getKodeDosen().isEmpty()) {
            output.setMessage("Dosen 1 is mandatory");
            return output;
        }
        if (perkuliahanBean.getIdSemester() == null) {
            output.setMessage("Semester is mandatory");
            return output;
        }
        perkuliahanManager.updatePerkuliahan(perkuliahanBean);
        output.setMessage(perkuliahanManager.getManagerResult().getMessage());

        return output;
    }

    public PerkuliahanServiceOutput updatePerkuliahanList(PerkuliahanServiceInput input) {
        PerkuliahanServiceOutput output = new PerkuliahanServiceOutput();

        // Validation
        List<PerkuliahanBean> listPerkBean = input.getListPerkuliahanBean();
        try {
            for (PerkuliahanBean perkuliahanBean : listPerkBean) {
                if (perkuliahanBean.getIdKelas() == null) {
                    output.setMessage("Kelas is mandatory");
                    return output;
                }
                if (perkuliahanBean.getKodeMk() == null || perkuliahanBean.getKodeMk().isEmpty()) {
                    output.setMessage("Matakuliah is mandatory");
                    return output;
                }
                if (perkuliahanBean.getKodeDosen() == null || perkuliahanBean.getKodeDosen().isEmpty()) {
                    output.setMessage("Dosen 1 is mandatory");
                    return output;
                }
                if (perkuliahanBean.getIdSemester() == null) {
                    output.setMessage("Semester is mandatory");
                    return output;
                }
                perkuliahanBean.setIdSemester(input.getSemesterNew());
                perkuliahanManager.updatePerkuliahan(perkuliahanBean);
                output.setMessage(perkuliahanManager.getManagerResult().getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }

    public PerkuliahanServiceOutput deletePerkuliahanById(PerkuliahanServiceInput input) {
        PerkuliahanServiceOutput output = new PerkuliahanServiceOutput();
        perkuliahanManager.deletePerkuliahanById(input.getIdPerkuliahan());
        output.setMessage(perkuliahanManager.getManagerResult().getMessage());

        return output;
    }
}
