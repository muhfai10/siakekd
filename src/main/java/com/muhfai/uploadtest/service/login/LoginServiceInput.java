/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.login;

import com.muhfai.uploadtest.core.login.LoginBean;
import com.muhfai.uploadtest.core.user.UserBean;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class LoginServiceInput {
    private String username;
    private String password;
    private UserBean userBean;
    private String freeText;
    private Integer idUser;
}
