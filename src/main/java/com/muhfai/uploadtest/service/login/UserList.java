/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.login;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class UserList {
    private Integer idUser;
    private String username;
    private String password;
    private String nama;
    private String role;
}
