/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.login;

import com.muhfai.uploadtest.core.login.LoginBean;
import com.muhfai.uploadtest.core.user.UserBean;
import java.util.List;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class LoginServiceOutput {
    private String message;
    private LoginBean loginBean;
    private UserBean userBean;
    private List<UserBean> listUserBean;
}
