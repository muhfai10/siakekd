/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.login;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class ChangeForm {
    private Integer idUser;
    private String username;
    @NotNull
    @NotEmpty
    private String password;
    private String nama;
    private Integer role;
    @NotNull
    @NotEmpty
    private String konfirmPassword;
    @NotNull
    @NotEmpty
    private String newPassword;
}
