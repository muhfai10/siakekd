/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.login;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class BuktiList {
    private Integer no;
    private String kodeMk;
    private String matakuliah;
    private String namaDosen;
    private String statusDosen;
    private String namaDosen2;
    private String statusDosen2;
}
