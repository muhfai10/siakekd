/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.login;

import com.muhfai.uploadtest.core.login.LoginManager;
import com.muhfai.uploadtest.core.login.LoginBean;
import com.muhfai.uploadtest.core.user.UserBean;
import com.muhfai.uploadtest.core.user.UserManager;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("loginService")
public class LoginService {
    private final LoginManager loginManager = new LoginManager();
    private final UserManager userManager = new UserManager();
    public LoginServiceOutput getLogin(LoginServiceInput loginServiceInput){
        LoginServiceOutput loginServiceOutput = new LoginServiceOutput();
        
        if(loginServiceInput.getUsername() == null){
            loginServiceOutput.setMessage("Username belum terisi");
            return loginServiceOutput;
        }
        if(loginServiceInput.getPassword() == null){
            loginServiceOutput.setMessage("Password belum terisi");
            return loginServiceOutput;
        }
        LoginBean userBean = loginManager.getLogin(loginServiceInput.getUsername(),loginServiceInput.getPassword());
        loginServiceOutput.setLoginBean(userBean);
        loginServiceOutput.setMessage(loginManager.getManagerResult().getMessage());
        return loginServiceOutput;
    }
    
    public LoginServiceOutput findAllUser(){
        LoginServiceOutput output = new LoginServiceOutput();
        List<UserBean> listUserBean = userManager.findAllUser();
        output.setListUserBean(listUserBean);
       
        return output;
    }
     
    public LoginServiceOutput findUser(LoginServiceInput input){
        LoginServiceOutput output = new LoginServiceOutput();
        List<UserBean> listUserBean = userManager.findUser(input.getFreeText());
        output.setListUserBean(listUserBean);
       
        return output;
    }
    
     public LoginServiceOutput findUserById(LoginServiceInput input){
        LoginServiceOutput output = new LoginServiceOutput();
        UserBean userBean = userManager.findUserById(input.getIdUser());
        output.setUserBean(userBean);
       
        return output;
    }
     public LoginServiceOutput findByUser(LoginServiceInput input){
        LoginServiceOutput output = new LoginServiceOutput();
        UserBean userBean = userManager.findByUser(input.getUsername());
        output.setUserBean(userBean);
       
        return output;
    }
     
     public LoginServiceOutput insertUser(LoginServiceInput input) {
        LoginServiceOutput output = new LoginServiceOutput();
                
        // Validation
        UserBean userBean = input.getUserBean();
        if (userBean.getUsername()== null || userBean.getUsername().isEmpty()) {
            output.setMessage("Username is mandatory");
            return output;
        }
        if (userBean.getPassword()== null || userBean.getPassword().isEmpty()) {
            output.setMessage("Password is mandatory");
            return output;
        }
        if (userBean.getNama()== null || userBean.getNama().isEmpty()) {
            output.setMessage("Nama is mandatory");
            return output;
        }
        if (userBean.getRole()== null) {
            output.setMessage("Role is mandatory");
            return output;
        }
        userManager.insertUser(userBean);
        output.setMessage(userManager.getManagerResult().getMessage());
        
        return output;
    }
     public LoginServiceOutput deleteUserById(LoginServiceInput input) {
        LoginServiceOutput output = new LoginServiceOutput();
        userManager.deleteUserById(input.getIdUser());
        output.setMessage(userManager.getManagerResult().getMessage());
        
        return output;
    }
    
}
