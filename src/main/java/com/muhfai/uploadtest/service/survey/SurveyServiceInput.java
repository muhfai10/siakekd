/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.survey;

import com.muhfai.uploadtest.core.survey.SurveyBean;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class SurveyServiceInput {
    private Integer idSurvey;
    private String kodeDosen;
    private String noBp;
    private Integer idPerkuliahan;
    private SurveyBean surveyBean;
    private Integer idProdi;
    private Integer semester;
    
}
