/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.survey;

import com.muhfai.uploadtest.core.survey.SurveyBean;
import com.muhfai.uploadtest.core.survey.SurveyManager;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("surveyService")
public class SurveyService {
    private final SurveyManager surveyManager = new SurveyManager();
    public SurveyServiceOutput insertSurvey(SurveyServiceInput input) {
        SurveyServiceOutput output = new SurveyServiceOutput();
                
        // Validation
        SurveyBean surveyBean = input.getSurveyBean();
        if (surveyBean.getIdPerkuliahan()== null) {
            output.setMessage("Survey id_perkuliahan is mandatory");
            return output;
        }
        if (surveyBean.getNoBp()== null || surveyBean.getNoBp().isEmpty()) {
            output.setMessage("Survey no_bp is mandatory");
            return output;
        }
        if (surveyBean.getKodeDosen()== null || surveyBean.getKodeDosen().isEmpty()) {
            output.setMessage("Survey kode_dosen is mandatory");
            return output;
        }
        
        Integer idSurvey=surveyManager.insertSurvey(surveyBean);
        output.setMessage(surveyManager.getManagerResult().getMessage());
        output.setIdSurvey(idSurvey);
        
        return output;
    }
    
    public SurveyServiceOutput getSurveyByProdi(SurveyServiceInput input) {
        SurveyServiceOutput output = new SurveyServiceOutput();
        List<SurveyBean> listSurveyBean = surveyManager.getSurveyByProdi(input.getIdProdi());
        output.setListSurveyBean(listSurveyBean);

        return output;
    }
    
    public SurveyServiceOutput getSurveyBySemester(SurveyServiceInput input) {
        SurveyServiceOutput output = new SurveyServiceOutput();
        List<SurveyBean> listSurveyBean = surveyManager.getSurveyBySemester(input.getIdProdi(),input.getSemester());
        output.setListSurveyBean(listSurveyBean);

        return output;
    }
    
    public SurveyServiceOutput getAllSurvey() {
        SurveyServiceOutput output = new SurveyServiceOutput();
        List<SurveyBean> listSurveyBean = surveyManager.getAllSurvey();
        output.setListSurveyBean(listSurveyBean);

        return output;
    }
    
    public SurveyServiceOutput findSurveyByMhsDosen(SurveyServiceInput input){
        SurveyServiceOutput output = new SurveyServiceOutput();
        SurveyBean surveyBean = surveyManager.findSurveyByMhsDosen(input.getNoBp(),input.getKodeDosen(),input.getIdPerkuliahan());
        output.setSurveyBean(surveyBean);
       
        return output;
    }
    
    public SurveyServiceOutput deleteSurveyById(SurveyServiceInput surveyServiceInput){
        SurveyServiceOutput surveyServiceOutput = new SurveyServiceOutput();
        //Integer idSurvey = surveyServiceInput.getIdSurvey();
        if(surveyServiceInput.getIdSurvey() != null){
            surveyManager.deleteSurveyById(surveyServiceInput.getIdSurvey());
            surveyServiceOutput.setMessage("Survey has been deleted by id");
            return surveyServiceOutput;
        }
        return surveyServiceOutput;
    }
}
