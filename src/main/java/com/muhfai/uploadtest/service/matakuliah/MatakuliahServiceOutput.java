/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.matakuliah;

import com.muhfai.uploadtest.core.matakuliah.MatakuliahBean;
import java.util.List;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class MatakuliahServiceOutput {
    private String message;
    private MatakuliahBean matakuliahBean;
    private List<MatakuliahBean> listMatkulBean;
}
