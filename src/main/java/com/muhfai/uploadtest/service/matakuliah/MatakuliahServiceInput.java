/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.matakuliah;

import com.muhfai.uploadtest.core.matakuliah.MatakuliahBean;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class MatakuliahServiceInput {
    private MatakuliahBean matakuliahBean;
    private String freeText;
    private String kodeMk;
}
