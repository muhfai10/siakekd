/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.matakuliah;

import com.muhfai.uploadtest.core.matakuliah.MatakuliahBean;
import com.muhfai.uploadtest.core.matakuliah.MatakuliahManager;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("matakuliahService")
public class MatakuliahService {
     private final MatakuliahManager matakuliahManager = new MatakuliahManager();
     public MatakuliahServiceOutput findAllMatakuliah(){
        MatakuliahServiceOutput matakuliahServiceOutput = new MatakuliahServiceOutput();
        List<MatakuliahBean> listMatkulBean = matakuliahManager.findAllMatakuliah();
        matakuliahServiceOutput.setListMatkulBean(listMatkulBean);
       
        return matakuliahServiceOutput;
    }
    public MatakuliahServiceOutput findMatkulById(MatakuliahServiceInput matakuliahServiceInput){
        MatakuliahServiceOutput matakuliahServiceOutput = new MatakuliahServiceOutput();
        MatakuliahBean matkulBean = matakuliahManager.findMatakuliahById(matakuliahServiceInput.getKodeMk());
        matakuliahServiceOutput.setMatakuliahBean(matkulBean);
       
        return matakuliahServiceOutput;
    }
    
     public MatakuliahServiceOutput findMatkul(MatakuliahServiceInput input) {
        MatakuliahServiceOutput output = new MatakuliahServiceOutput();
        List<MatakuliahBean> listMatkulBean = matakuliahManager.findMatkul(input.getFreeText());
        output.setListMatkulBean(listMatkulBean);

        return output;
    }
    
    public MatakuliahServiceOutput insertMatakuliah(MatakuliahServiceInput input) {
        MatakuliahServiceOutput output = new MatakuliahServiceOutput();
                
        // Validation
        MatakuliahBean matakuliahBean = input.getMatakuliahBean();
        if (matakuliahBean.getKodeMk()== null) {
            output.setMessage("Kode matakuliah is mandatory");
            return output;
        }
        if (matakuliahBean.getMataKuliah()== null || matakuliahBean.getMataKuliah().isEmpty()) {
            output.setMessage("Matakuliah is mandatory");
            return output;
        }
        if (matakuliahBean.getSks()== null) {
            output.setMessage("SKS is mandatory");
            return output;
        }
        
        matakuliahManager.insertMatakuliah(matakuliahBean);
        output.setMessage(matakuliahManager.getManagerResult().getMessage());
        
        return output;
    }
    
    public MatakuliahServiceOutput updateMatakuliah(MatakuliahServiceInput input) {
        MatakuliahServiceOutput output = new MatakuliahServiceOutput();
                
        // Validation
        MatakuliahBean matakuliahBean = input.getMatakuliahBean();
        if (matakuliahBean.getKodeMk()== null) {
            output.setMessage("Kode matakuliah is mandatory");
            return output;
        }
        if (matakuliahBean.getMataKuliah()== null || matakuliahBean.getMataKuliah().isEmpty()) {
            output.setMessage("Matakuliah is mandatory");
            return output;
        }
        if (matakuliahBean.getSks()== null) {
            output.setMessage("SKS is mandatory");
            return output;
        }
        
        matakuliahManager.updateMatakuliah(matakuliahBean);
        output.setMessage(matakuliahManager.getManagerResult().getMessage());
        
        return output;
    }
    public MatakuliahServiceOutput deleteMatakuliahById(MatakuliahServiceInput input) {
        MatakuliahServiceOutput output = new MatakuliahServiceOutput();
        matakuliahManager.deleteMatakuliahById(input.getKodeMk());
        output.setMessage(matakuliahManager.getManagerResult().getMessage());
        
        return output;
    }
}
