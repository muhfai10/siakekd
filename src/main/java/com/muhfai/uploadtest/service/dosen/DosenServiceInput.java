/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.dosen;

import com.muhfai.uploadtest.core.dosen.DosenBean;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class DosenServiceInput {
    private DosenBean dosenBean;
    private String freeText;
    private String kodeDosen;
}
