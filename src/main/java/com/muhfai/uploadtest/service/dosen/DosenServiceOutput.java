/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.dosen;

import com.muhfai.uploadtest.core.dosen.DosenBean;
import java.util.List;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class DosenServiceOutput {
    private String message;
    private DosenBean dosenBean;
    private List<DosenBean> listDosenBean;
}
