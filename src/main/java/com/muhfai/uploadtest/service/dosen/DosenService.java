/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.dosen;

import com.muhfai.uploadtest.core.dosen.DosenBean;
import com.muhfai.uploadtest.core.dosen.DosenManager;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("dosenService")
public class DosenService {

    private final DosenManager dosenManager = new DosenManager();

    public DosenServiceOutput findAllDosen() {
        DosenServiceOutput dosenServiceOutput = new DosenServiceOutput();
        List<DosenBean> listDosenBean = dosenManager.findAllDosen();
        dosenServiceOutput.setListDosenBean(listDosenBean);

        return dosenServiceOutput;
    }

    public DosenServiceOutput findDosenById(DosenServiceInput dosenServiceInput) {
        DosenServiceOutput dosenServiceOutput = new DosenServiceOutput();
        DosenBean dosenBean = dosenManager.findDosenById(dosenServiceInput.getKodeDosen());
        dosenServiceOutput.setDosenBean(dosenBean);

        return dosenServiceOutput;
    }

    public DosenServiceOutput findDosen(DosenServiceInput input) {
        DosenServiceOutput output = new DosenServiceOutput();
        List<DosenBean> listDosenBean = dosenManager.findDosen(input.getFreeText());
        output.setListDosenBean(listDosenBean);

        return output;
    }

    public DosenServiceOutput insertDosen(DosenServiceInput input) {
        DosenServiceOutput output = new DosenServiceOutput();

        // Validation
        DosenBean dosenBean = input.getDosenBean();
        if (dosenBean.getKodeDosen() == null) {
            output.setMessage("Kode dosen is mandatory");
            return output;
        }
        if (dosenBean.getNama() == null || dosenBean.getNama().isEmpty()) {
            output.setMessage("Nama dosen is mandatory");
            return output;
        }
        if (dosenBean.getNip() == null || dosenBean.getNip().isEmpty()) {
            output.setMessage("NIP is mandatory");
            return output;
        }
        if (dosenBean.getNidn() == null || dosenBean.getNidn().isEmpty()) {
            output.setMessage("NIDN is mandatory");
            return output;
        }
        if (dosenBean.getNoTelp() == null || dosenBean.getNoTelp().isEmpty()) {
            output.setMessage("No.Telepon is mandatory");
            return output;
        }
        if (dosenBean.getEmail() == null || dosenBean.getEmail().isEmpty()) {
            output.setMessage("Email is mandatory");
            return output;
        }

        dosenManager.insertDosen(dosenBean);
        output.setMessage(dosenManager.getManagerResult().getMessage());

        return output;
    }

    public DosenServiceOutput updateDosen(DosenServiceInput input) {
        DosenServiceOutput output = new DosenServiceOutput();

        // Validation
        DosenBean dosenBean = input.getDosenBean();
        if (dosenBean.getKodeDosen() == null) {
            output.setMessage("Kode dosen is mandatory");
            return output;
        }
        if (dosenBean.getNama() == null || dosenBean.getNama().isEmpty()) {
            output.setMessage("Nama dosen is mandatory");
            return output;
        }
        if (dosenBean.getNip() == null || dosenBean.getNip().isEmpty()) {
            output.setMessage("NIP is mandatory");
            return output;
        }
        if (dosenBean.getNidn() == null || dosenBean.getNidn().isEmpty()) {
            output.setMessage("NIDN is mandatory");
            return output;
        }
        if (dosenBean.getNoTelp() == null || dosenBean.getNoTelp().isEmpty()) {
            output.setMessage("No.Telepon is mandatory");
            return output;
        }
        if (dosenBean.getEmail() == null || dosenBean.getEmail().isEmpty()) {
            output.setMessage("Email is mandatory");
            return output;
        }

        dosenManager.updateDosen(dosenBean);
        output.setMessage(dosenManager.getManagerResult().getMessage());

        return output;
    }

    public DosenServiceOutput deleteDosenById(DosenServiceInput input) {
        DosenServiceOutput output = new DosenServiceOutput();
        dosenManager.deleteDosenById(input.getKodeDosen());
        output.setMessage(dosenManager.getManagerResult().getMessage());

        return output;
    }
}
