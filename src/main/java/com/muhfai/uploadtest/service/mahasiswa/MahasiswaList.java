/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.mahasiswa;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class MahasiswaList {
    private String noBp;
    private String nama;
    private String kelas;
    private String lokal;
    private Integer status;
}
