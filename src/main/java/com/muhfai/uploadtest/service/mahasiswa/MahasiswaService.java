/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.mahasiswa;

import com.muhfai.uploadtest.core.mahasiswa.MahasiswaBean;
import com.muhfai.uploadtest.core.mahasiswa.MahasiswaManager;
import com.muhfai.uploadtest.core.mahasiswa.NewMhsBean;
import com.muhfai.uploadtest.core.user.UserBean;
import com.muhfai.uploadtest.core.user.UserManager;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("mahasiswaService")
public class MahasiswaService {

    private final MahasiswaManager mahasiswaManager = new MahasiswaManager();
    private final UserManager userManager = new UserManager();

    public MahasiswaServiceOutput findMahasiswaById(MahasiswaServiceInput mahasiswaServiceInput) {
        MahasiswaServiceOutput mahasiswaServiceOutput = new MahasiswaServiceOutput();
        MahasiswaBean mahasiswaBean = mahasiswaManager.findMahasiswaById(mahasiswaServiceInput.getNoBp());
        mahasiswaServiceOutput.setMahasiswaBean(mahasiswaBean);

        return mahasiswaServiceOutput;
    }

    public MahasiswaServiceOutput getMahasiswa(MahasiswaServiceInput input) {
        MahasiswaServiceOutput output = new MahasiswaServiceOutput();
        NewMhsBean newMhsBean = mahasiswaManager.getMahasiswa(input.getNoBp());
        output.setNewMhsBean(newMhsBean);

        return output;
    }

    public MahasiswaServiceOutput findAllMahasiswa() {
        MahasiswaServiceOutput mahasiswaServiceOutput = new MahasiswaServiceOutput();
        List<MahasiswaBean> listMahasiswaBean = mahasiswaManager.findAllMahasiswa();
        mahasiswaServiceOutput.setListMahasiswaBean(listMahasiswaBean);

        return mahasiswaServiceOutput;
    }

    public MahasiswaServiceOutput findMahasiswa(MahasiswaServiceInput input) {
        MahasiswaServiceOutput output = new MahasiswaServiceOutput();
        List<MahasiswaBean> listMhsBean = mahasiswaManager.findMahasiswa(input.getFreeText());
        output.setListMahasiswaBean(listMhsBean);

        return output;
    }

    public MahasiswaServiceOutput insertMahasiswa(MahasiswaServiceInput input) {
        MahasiswaServiceOutput output = new MahasiswaServiceOutput();

        // Validation
        MahasiswaBean mahasiswaBean = input.getMahasiswaBean();
        if (mahasiswaBean.getNoBp() == null || mahasiswaBean.getNoBp().isEmpty()) {
            output.setMessage("No BP is mandatory");
            return output;
        }
        if (mahasiswaBean.getNama() == null || mahasiswaBean.getNama().isEmpty()) {
            output.setMessage("Nama mahasiswa is mandatory");
            return output;
        }
        if (mahasiswaBean.getLokal() == null || mahasiswaBean.getLokal().isEmpty()) {
            output.setMessage("Kelas is mandatory");
            return output;
        }
        if (mahasiswaBean.getKelas() == null) {
            output.setMessage("Tingkat is mandatory");
            return output;
        }

        mahasiswaManager.insertMahasiswa(mahasiswaBean);
        output.setMessage(mahasiswaManager.getManagerResult().getMessage());

        return output;
    }

    public MahasiswaServiceOutput insertListMhs(MahasiswaServiceInput input) {
        MahasiswaServiceOutput output = new MahasiswaServiceOutput();
        List<MahasiswaBean> listMhs = input.getListMhsBean();
        try {
            for (MahasiswaBean mahasiswaBean : listMhs) {
                MahasiswaBean mhsChecker = mahasiswaManager.findMahasiswaById(mahasiswaBean.getNoBp());
                //String tester = "";
                if (mhsChecker.getNoBp() == null) {
                    mahasiswaManager.insertMahasiswa(mahasiswaBean);
                    UserBean userBean = new UserBean();
                    userBean.setUsername(mahasiswaBean.getNoBp());
                    userBean.setNama(mahasiswaBean.getNama());
                    userBean.setPassword("12345");
                    userBean.setRole(2);
                    userManager.insertUser(userBean);
                    output.setMessage(mahasiswaManager.getManagerResult().getMessage());
                } else {
                    mahasiswaManager.updateMahasiswa(mahasiswaBean);
                    output.setMessage(mahasiswaManager.getManagerResult().getMessage());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }

    public MahasiswaServiceOutput updateMahasiswa(MahasiswaServiceInput input) {
        MahasiswaServiceOutput output = new MahasiswaServiceOutput();

        // Validation
        MahasiswaBean mahasiswaBean = input.getMahasiswaBean();
        if (mahasiswaBean.getNoBp() == null || mahasiswaBean.getNoBp().isEmpty()) {
            output.setMessage("No BP is mandatory");
            return output;
        }
        if (mahasiswaBean.getNama() == null || mahasiswaBean.getNama().isEmpty()) {
            output.setMessage("Nama mahasiswa is mandatory");
            return output;
        }
        if (mahasiswaBean.getLokal() == null || mahasiswaBean.getLokal().isEmpty()) {
            output.setMessage("Kelas is mandatory");
            return output;
        }
        if (mahasiswaBean.getKelas() == null) {
            output.setMessage("Tingkat is mandatory");
            return output;
        }
        mahasiswaManager.updateMahasiswa(mahasiswaBean);
        output.setMessage(mahasiswaManager.getManagerResult().getMessage());

        return output;
    }

    public MahasiswaServiceOutput deleteMahasiswaById(MahasiswaServiceInput input) {
        MahasiswaServiceOutput output = new MahasiswaServiceOutput();
        mahasiswaManager.deleteMahasiswaById(input.getNoBp());
        output.setMessage(mahasiswaManager.getManagerResult().getMessage());

        return output;
    }
}
