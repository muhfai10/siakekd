/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.mahasiswa;

import com.muhfai.uploadtest.core.mahasiswa.MahasiswaBean;
import com.muhfai.uploadtest.core.mahasiswa.NewMhsBean;
import java.util.List;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class MahasiswaServiceInput {
    private String noBp;
    private String freeText;
    private MahasiswaBean mahasiswaBean;
    private List<MahasiswaBean> listMhsBean;
}
