/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.kelas;

import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class KelasList {
    private Integer idKelas;
    private String kelas;
    private String prodi;
    private String kode;
}
