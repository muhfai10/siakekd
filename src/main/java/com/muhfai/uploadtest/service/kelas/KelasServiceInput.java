/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.kelas;

import com.muhfai.uploadtest.core.kelas.KelasBean;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class KelasServiceInput {
    private Integer idKelas;
    private KelasBean kelasBean;
}
