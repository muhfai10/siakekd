/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.kelas;

import com.muhfai.uploadtest.core.kelas.KelasBean;
import java.util.List;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class KelasServiceOutput {
    private String message;
    private KelasBean kelasBean;
    private List<KelasBean> listKelasBean;
}
