/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.kelas;

import com.muhfai.uploadtest.core.kelas.KelasBean;
import com.muhfai.uploadtest.core.kelas.KelasManager;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("kelasService")
public class KelasService {
     private final KelasManager kelasManager = new KelasManager();
     public KelasServiceOutput findAllKelas(){
        KelasServiceOutput kelasServiceOutput = new KelasServiceOutput();
        List<KelasBean> listKelasBean = kelasManager.findAllKelas();
        kelasServiceOutput.setListKelasBean(listKelasBean);
       
        return kelasServiceOutput;
    }
    public KelasServiceOutput findKelasById(KelasServiceInput kelasServiceInput){
        KelasServiceOutput kelasServiceOutput = new KelasServiceOutput();
        KelasBean matkulBean = kelasManager.findKelasById(kelasServiceInput.getIdKelas());
        kelasServiceOutput.setKelasBean(matkulBean);
       
        return kelasServiceOutput;
    }
     public KelasServiceOutput insertKelas(KelasServiceInput input) {
        KelasServiceOutput output = new KelasServiceOutput();

        // Validation
        KelasBean kelasBean = input.getKelasBean();
        if (kelasBean.getKelas()== null || kelasBean.getKelas().isEmpty()) {
            output.setMessage("Kelas is mandatory");
            return output;
        }
        kelasManager.insertKelas(kelasBean);
        output.setMessage(kelasManager.getManagerResult().getMessage());

        return output;
    }

    public KelasServiceOutput deleteKelasById(KelasServiceInput input) {
        KelasServiceOutput output = new KelasServiceOutput();
        kelasManager.deleteKelasById(input.getIdKelas());
        output.setMessage(kelasManager.getManagerResult().getMessage());

        return output;
    }
}
