/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.prodiCustom;

import com.muhfai.uploadtest.core.prodiCustom.ProdiCustomBean;
import com.muhfai.uploadtest.core.prodiCustom.ProdiCustomManager;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("prodiCustomService")
public class ProdiCustomService {
    private final ProdiCustomManager prodiManager = new ProdiCustomManager();
    
    public ProdiCustomServiceOutput findProdi(ProdiCustomServiceInput input) {
        ProdiCustomServiceOutput output = new ProdiCustomServiceOutput();
        ProdiCustomBean prodiCustomBean = prodiManager.findProdi(input.getIdProdi());
        output.setProdiCustomBean(prodiCustomBean);

        return output;
    }
    
    public ProdiCustomServiceOutput findProdiBySemester(ProdiCustomServiceInput input) {
        ProdiCustomServiceOutput output = new ProdiCustomServiceOutput();
        ProdiCustomBean prodiCustomBean = prodiManager.findProdiBySemester(input.getIdProdi(),input.getIdSemester());
        output.setProdiCustomBean(prodiCustomBean);

        return output;
    }

    
}
