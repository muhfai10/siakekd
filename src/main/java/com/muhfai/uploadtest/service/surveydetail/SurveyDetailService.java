/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.surveydetail;

import com.muhfai.uploadtest.core.surveydetail.SurveyDetailBean;
import com.muhfai.uploadtest.core.surveydetail.SurveyDetailManager;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("surveyDetailService")
public class SurveyDetailService {

    private final SurveyDetailManager svDetailManager = new SurveyDetailManager();

    public SurveyDetailServiceOutput insertSurveyDetail(SurveyDetailServiceInput input) {
        SurveyDetailServiceOutput output = new SurveyDetailServiceOutput();

        // Validation
        List<SurveyDetailBean> svDetailBeanList = input.getSvDetailBeanList();
        for (SurveyDetailBean surveyDetailBean : svDetailBeanList) {
            if (surveyDetailBean.getIdSurvey() == null) {
                output.setMessage("Survey id_survey is mandatory");
                return output;
            }
            if (surveyDetailBean.getIdKuesioner() == null) {
                output.setMessage("Survey id_kuesioner is mandatory");
                return output;
            }
            if (surveyDetailBean.getNilai() == null) {
                output.setMessage("Input Kuesioner Gagal");
                return output;
            }
        }
        svDetailManager.insertSurveyDetail(svDetailBeanList);
        output.setMessage(svDetailManager.getManagerResult().getMessage());

        return output;
    }
}
