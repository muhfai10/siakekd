/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.surveydetail;

import com.muhfai.uploadtest.core.surveydetail.SurveyDetailBean;
import java.util.List;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class SurveyDetailServiceInput {
    private List<SurveyDetailBean> svDetailBeanList;
}
