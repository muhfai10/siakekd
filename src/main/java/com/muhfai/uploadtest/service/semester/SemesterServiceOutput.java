/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.semester;

import com.muhfai.uploadtest.core.semester.SemesterBean;
import java.util.List;
import lombok.Data;

/**
 *
 * @author MuhFai10
 */
@Data
public class SemesterServiceOutput {
    private String message;
    private SemesterBean semesterBean;
    private List<SemesterBean> listSemesterBean;
}
