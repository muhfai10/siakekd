/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.service.semester;

import com.muhfai.uploadtest.core.semester.SemesterBean;
import com.muhfai.uploadtest.core.semester.SemesterManager;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author MuhFai10
 */
@Service("semesterService")
public class SemesterService {
    private final SemesterManager semesterManager = new SemesterManager();
     public SemesterServiceOutput findAllSemester(){
        SemesterServiceOutput semesterServiceOutput = new SemesterServiceOutput();
        List<SemesterBean> listSemesterBean = semesterManager.findAllSemester();
        semesterServiceOutput.setListSemesterBean(listSemesterBean);
       
        return semesterServiceOutput;
    }
     public SemesterServiceOutput getSemesterInPerkuliahan(){
        SemesterServiceOutput semesterServiceOutput = new SemesterServiceOutput();
        List<SemesterBean> listSemesterBean = semesterManager.getSemesterInPerkuliahan();
        semesterServiceOutput.setListSemesterBean(listSemesterBean);
       
        return semesterServiceOutput;
    }
    public SemesterServiceOutput findSemesterById(SemesterServiceInput semesterServiceInput){
        SemesterServiceOutput semesterServiceOutput = new SemesterServiceOutput();
        SemesterBean semesterBean = semesterManager.findSemesterById(semesterServiceInput.getIdSemester());
        semesterServiceOutput.setSemesterBean(semesterBean);
       
        return semesterServiceOutput;
    }
    
     public SemesterServiceOutput updateSemester(SemesterServiceInput input) {
        SemesterServiceOutput output = new SemesterServiceOutput();
                
        // Validation
        SemesterBean semesterBean = input.getSemesterBean();
       
        if (semesterBean.getTahunAjaran()== null || semesterBean.getTahunAjaran().isEmpty()) {
            output.setMessage("Tahun Ajaran is mandatory");
            return output;
        }
        semesterManager.updateSemester(semesterBean);
        output.setMessage(semesterManager.getManagerResult().getMessage());
        
        return output;
    }
     
     public SemesterServiceOutput insertSemester(SemesterServiceInput input) {
        SemesterServiceOutput output = new SemesterServiceOutput();
                
        // Validation
        SemesterBean semesterBean = input.getSemesterBean();
       
        if (semesterBean.getTahunAjaran()== null || semesterBean.getTahunAjaran().isEmpty()) {
            output.setMessage("Tahun Ajaran is mandatory");
            return output;
        }
        if (semesterBean.getSemester()== null || semesterBean.getSemester().isEmpty()) {
            output.setMessage("Semester is mandatory");
            return output;
        }
        semesterManager.insertSemester(semesterBean);
        output.setMessage(semesterManager.getManagerResult().getMessage());
        
        return output;
    }
}
