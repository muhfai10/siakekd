package com.muhfai.uploadtest.repository;

import java.util.List;

public interface AbstractRepository<Model,Dto> {
	public Dto convertToDto(Model model);
    public List<Dto> convertToDto(List<Model> modelList);
    public Model convertFromDto(Dto dto);
    public List<Model> convertFromDto(List<Dto> dtoList);
}
