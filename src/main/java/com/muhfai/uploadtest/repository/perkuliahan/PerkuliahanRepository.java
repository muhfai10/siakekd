/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.perkuliahan;

import com.muhfai.uploadtest.dto.PerkuliahanDto;
import com.muhfai.uploadtest.model.Perkuliahan;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class PerkuliahanRepository implements AbstractRepository<Perkuliahan,PerkuliahanDto> {

    public List<PerkuliahanDto> findAllPerkuliahan(Integer kelas){
           List<Perkuliahan> listPerkuliahan = Perkuliahan.find("id_semester=1 AND id_kelas = ?",kelas);
           return convertToDto(listPerkuliahan);
    }
    
    public List<PerkuliahanDto> getAllPerkuliahan() {
        List<Perkuliahan> listPerkuliahan = Perkuliahan.findAll();
        return convertToDto(listPerkuliahan);
    }
    
    public List<PerkuliahanDto> findPerkuliahan(String freeText){
        List<Perkuliahan> perkuliahanList = Perkuliahan.where("kode_mk LIKE '%"+freeText+"%'");
        return convertToDto(perkuliahanList);
    }


    public PerkuliahanDto findPerkuliahanById(Integer idPerkuliahan) {
        Perkuliahan perkuliahan = Perkuliahan.findById(idPerkuliahan);
        return convertToDto(perkuliahan);
    }

    public Integer insertPerkuliahan(PerkuliahanDto dto) {
        Perkuliahan model = convertFromDto(dto);
        model.insert();
        return model.getInteger("id_perkuliahan");
    }
    
    public Integer insertPerkuliahan_(PerkuliahanDto dto) {
        Perkuliahan model = new Perkuliahan();
        model.set("id_perkuliahan",dto.getIdPerkuliahan());
        model.set("id_kelas",dto.getIdKelas());
        model.set("kode_mk",dto.getKodeMk());
        model.set("kode_dosen",dto.getKodeDosen());
        model.set("id_semester",dto.getIdSemester());
        model.insert();
        return model.getInteger("id_perkuliahan");
    }

    public boolean updatePerkuliahan(PerkuliahanDto dto) {
        Perkuliahan model = convertFromDto(dto);
        return model.saveIt();
    }
    public boolean updatePerkuliahan_(PerkuliahanDto dto) {
        Perkuliahan model = new Perkuliahan();
        model.set("id_perkuliahan",dto.getIdPerkuliahan());
        model.set("id_kelas",dto.getIdKelas());
        model.set("kode_mk",dto.getKodeMk());
        model.set("kode_dosen",dto.getKodeDosen());
        model.set("kode_dosen2",null);
        model.set("id_semester",dto.getIdSemester());
        return model.saveIt();
    }

    public Integer deletePerkuliahanById(Integer idPerkuliahan) {
        Perkuliahan model = Perkuliahan.findFirst("id_perkuliahan = ?", idPerkuliahan);
        model.delete();
        return 1;
    }
    
    @Override
    public PerkuliahanDto convertToDto(Perkuliahan model) {
        PerkuliahanDto dto = null;
		if(model!=null){
			dto = new PerkuliahanDto();
			dto.setIdPerkuliahan(model.getInteger("id_perkuliahan"));
			dto.setIdKelas(model.getInteger("id_kelas"));
                        dto.setKodeMk(model.getString("kode_mk"));
                        dto.setKodeDosen(model.getString("kode_dosen"));
                        dto.setKodeDosen2(model.getString("kode_dosen2"));
                        dto.setIdSemester(model.getInteger("id_semester"));
                        //dto.setProdi(model.getInteger("prodi"));
		}
		return dto;
    }

    @Override
    public List<PerkuliahanDto> convertToDto(List<Perkuliahan> modelList) {
        List<PerkuliahanDto> dtoList = null;	
        if(modelList != null){
			dtoList = new ArrayList<PerkuliahanDto>();
			for(Perkuliahan model:modelList){
				dtoList.add(convertToDto(model));
			}
		}
		return dtoList;
    }

    @Override
    public Perkuliahan convertFromDto(PerkuliahanDto dto) {
        Perkuliahan model = null;
	        if (dto != null) {
	            model = new Perkuliahan();
	            model.setInteger("id_perkuliahan", dto.getIdPerkuliahan());
	            model.setInteger("id_kelas",dto.getIdKelas());
                    model.setString("kode_mk",dto.getKodeMk());
                    model.setString("kode_dosen",dto.getKodeDosen());
                    model.setString("kode_dosen2",dto.getKodeDosen2());
                    model.setInteger("id_semester",dto.getIdSemester());
                    //model.setInteger("prodi",dto.getProdi());
	        }
	        return model;
    }

    @Override
    public List<Perkuliahan> convertFromDto(List<PerkuliahanDto> dtoList) {
        List<Perkuliahan> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<Perkuliahan>();
            for (PerkuliahanDto dto: dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }
    
}
