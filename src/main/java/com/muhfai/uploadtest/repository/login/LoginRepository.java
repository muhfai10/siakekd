/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.login;

import com.muhfai.uploadtest.dto.UserDto;
import com.muhfai.uploadtest.model.User;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class LoginRepository implements AbstractRepository<User, UserDto> {

    public UserDto getLogin(String username, String password) {
        List<User> listUser = User.where("username='" + username + "' AND password='" + password + "';");
        return convertToDto(listUser.get(0));
    }
    
    public UserDto findByUser(String username){
        List<User> listUser = User.where("username='" + username + "';");
        return convertToDto(listUser.get(0));
    }
    
     public UserDto findUserById(Integer idUser) {
        User user = User.findById(idUser);
        return convertToDto(user);
    }

    public List<UserDto> findAllUser() {
        List<User> listUser = User.findAll();
        return convertToDto(listUser);

    }
    
     public List<UserDto> findUser(String freeText){
        List<User> userList = User.where("username LIKE '%"+freeText+"%' OR nama LIKE '%"+freeText
                + "%'");
        return convertToDto(userList);
    }

    public Integer insertUser(UserDto dto) {
        User model = convertFromDto(dto);
        model.insert();
        return model.getInteger("id_user");
    }

    public boolean updateUser(UserDto dto) {
        User model = convertFromDto(dto);
        return model.saveIt();
    }

    public Integer deleteUserById(Integer idUser) {
        User model = User.findFirst("id_user = ?", idUser);
        model.delete();
        return 1;
    }

    @Override
    public UserDto convertToDto(User model) {
        UserDto dto = null;
        if (model != null) {
            dto = new UserDto();
            dto.setIdUser(model.getInteger("id_user"));
            dto.setUsername(model.getString("username"));
            dto.setPassword(model.getString("password"));
            dto.setNama(model.getString("nama"));
            dto.setRole(model.getInteger("role"));
        }
        return dto;
    }

    @Override
    public List<UserDto> convertToDto(List<User> modelList) {
        List<UserDto> dtoList = null;
        if (modelList != null) {
            dtoList = new ArrayList<UserDto>();
            for (User model : modelList) {
                dtoList.add(convertToDto(model));
            }
        }
        return dtoList;
    }

    @Override
    public User convertFromDto(UserDto dto) {
        User model = null;
        if (dto != null) {
            model = new User();
            model.setInteger("id_user", dto.getIdUser());
            model.setString("username", dto.getUsername());
            model.setString("password", dto.getPassword());
            model.setString("nama", dto.getNama());
            model.setInteger("role", dto.getRole());
        }
        return model;
    }

    @Override
    public List<User> convertFromDto(List<UserDto> dtoList) {
        List<User> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<User>();
            for (UserDto dto : dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }

}
