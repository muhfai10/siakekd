/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.semester;

import com.muhfai.uploadtest.dto.SemesterDto;
import com.muhfai.uploadtest.model.Semester;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class SemesterRepository implements AbstractRepository<Semester,SemesterDto> {

    public List<SemesterDto> findAllSemester(){
        List<Semester> listSemester = Semester.findAll();
        return convertToDto(listSemester);
    }
    public SemesterDto findSemesterById(Integer id){
        List<Semester> listSemester = Semester.find("id_semester = ?",id);
            return convertToDto(listSemester.get(0));
    }
    public boolean updateSemester(SemesterDto dto) {
        Semester model = convertFromDto(dto);
        return model.saveIt();
    }
    public Integer insertSemester(SemesterDto dto) {
        Semester model = convertFromDto(dto);
        model.insert();
        return model.getInteger("id_semester");
    }
    
    @Override
    public SemesterDto convertToDto(Semester model) {
        SemesterDto dto = null;
		if(model!=null){
			dto = new SemesterDto();
			dto.setIdSemester(model.getInteger("id_semester"));
			dto.setSemester(model.getString("semester"));
                        dto.setTahunAjaran(model.getString("tahun_ajaran"));
                        dto.setStatus(model.getInteger("status"));
		}
		return dto;
    }

    @Override
    public List<SemesterDto> convertToDto(List<Semester> modelList) {
         List<SemesterDto> dtoList = null;
		if(modelList != null){
			dtoList = new ArrayList<SemesterDto>();
			for(Semester model:modelList){
				dtoList.add(convertToDto(model));
			}
		}
		return dtoList;
    }

    @Override
    public Semester convertFromDto(SemesterDto dto) {
         Semester model = null;
	        if (dto != null) {
	            model = new Semester();
	            model.setInteger("id_semester", dto.getIdSemester());
	            model.setString("semester",dto.getSemester());
                    model.setString("tahun_ajaran",dto.getTahunAjaran());
                    model.setInteger("status",dto.getStatus());
	        }
	        return model;
    }

    @Override
    public List<Semester> convertFromDto(List<SemesterDto> dtoList) {
        List<Semester> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<Semester>();
            for (SemesterDto dto: dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }
    
}
