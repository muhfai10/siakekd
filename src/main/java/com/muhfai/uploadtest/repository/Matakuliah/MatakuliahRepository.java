/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.Matakuliah;

import com.muhfai.uploadtest.dto.MatakuliahDto;
import com.muhfai.uploadtest.model.Matakuliah;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class MatakuliahRepository implements AbstractRepository<Matakuliah,MatakuliahDto> {

    public List<MatakuliahDto> findAllMatkul(){
        List<Matakuliah> listMatkul = Matakuliah.findAll();
        return convertToDto(listMatkul);
    }
    public MatakuliahDto findMatkulById(String kodeMk){
        List<Matakuliah> listMatkul = Matakuliah.find("kode_mk = ?",kodeMk);
            return convertToDto(listMatkul.get(0));
    }
    
    public List<MatakuliahDto> findMatkul(String freeText){
        List<Matakuliah> matkulList = Matakuliah.where("kode_mk LIKE '%"+freeText+"%' OR mata_kuliah LIKE '%"+freeText
                + "%'");
        return convertToDto(matkulList);
    }
    
    public String insertMatakuliah(MatakuliahDto dto) {
        Matakuliah model = convertFromDto(dto);
        model.insert();
        return model.getString("kode_mk");
    }
    public boolean updateMatakuliah(MatakuliahDto dto){
        Matakuliah model = convertFromDto(dto);
        return model.saveIt();
    }
    public Integer deleteMatakuliahById(String kodeMk){
        Matakuliah model = Matakuliah.findFirst("kode_mk = ?", kodeMk);
        model.delete();
        return 1;
    }
    
    @Override
    public MatakuliahDto convertToDto(Matakuliah model) {
        MatakuliahDto dto = null;
		if(model!=null){
			dto = new MatakuliahDto();
			dto.setKodeMk(model.getString("kode_mk"));
			dto.setMataKuliah(model.getString("mata_kuliah"));
                        dto.setSks(model.getInteger("sks"));
                        dto.setKeterangan(model.getString("keterangan"));
		}
		return dto;
    }

    @Override
    public List<MatakuliahDto> convertToDto(List<Matakuliah> modelList) {
        List<MatakuliahDto> dtoList = null;
		if(modelList != null){
			dtoList = new ArrayList<MatakuliahDto>();
			for(Matakuliah model:modelList){
				dtoList.add(convertToDto(model));
			}
		}
		return dtoList;
    }

    @Override
    public Matakuliah convertFromDto(MatakuliahDto dto) {
        Matakuliah model = null;
	        if (dto != null) {
	            model = new Matakuliah();
	            model.setString("kode_mk", dto.getKodeMk());
	            model.setString("mata_kuliah",dto.getMataKuliah());
                    model.setInteger("sks",dto.getSks());
                    model.setString("keterangan",dto.getKeterangan());
	        }
	        return model;
    }

    @Override
    public List<Matakuliah> convertFromDto(List<MatakuliahDto> dtoList) {
        List<Matakuliah> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<Matakuliah>();
            for (MatakuliahDto dto: dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }
    
}
