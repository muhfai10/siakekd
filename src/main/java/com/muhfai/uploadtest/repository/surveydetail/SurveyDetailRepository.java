/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.surveydetail;

import com.muhfai.uploadtest.dto.SurveyDetailDto;
import com.muhfai.uploadtest.model.SurveyDetail;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class SurveyDetailRepository implements AbstractRepository<SurveyDetail,SurveyDetailDto>{

    
    public Integer insertSurveyDetail(List<SurveyDetailDto> dtoList) {
        List<SurveyDetail> modelList = convertFromDto(dtoList);
        //SurveyDetail model;
        Integer idSurveyDetail = null;
        for(SurveyDetail model:modelList){
            model.insert();
            idSurveyDetail = model.getInteger("id_surveydetail");
        }
        
        return idSurveyDetail;
    }
    
    @Override
    public SurveyDetailDto convertToDto(SurveyDetail model) {
         SurveyDetailDto dto = null;
		if(model!=null){
			dto = new SurveyDetailDto();
			dto.setIdSurveyDetail(model.getInteger("id_surveydetail"));
			dto.setIdSurvey(model.getInteger("id_survey"));
                        dto.setIdKuesioner(model.getInteger("id_kuesioner"));
                        dto.setNilai(model.getInteger("nilai"));
		}
		return dto;
    }

    @Override
    public List<SurveyDetailDto> convertToDto(List<SurveyDetail> modelList) {
        List<SurveyDetailDto> dtoList = null;
		if(modelList != null){
			dtoList = new ArrayList<SurveyDetailDto>();
			for(SurveyDetail model:modelList){
				dtoList.add(convertToDto(model));
			}
		}
		return dtoList;
    }

    @Override
    public SurveyDetail convertFromDto(SurveyDetailDto dto) {
        SurveyDetail model = null;
	        if (dto != null) {
	            model = new SurveyDetail();
	            model.setInteger("id_surveydetail", dto.getIdSurveyDetail());
	            model.setInteger("id_survey",dto.getIdSurvey());
                    model.setInteger("id_kuesioner",dto.getIdKuesioner());
                    model.setInteger("nilai",dto.getNilai());
	        }
	        return model;
    }

    @Override
    public List<SurveyDetail> convertFromDto(List<SurveyDetailDto> dtoList) {
        List<SurveyDetail> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<SurveyDetail>();
            for (SurveyDetailDto dto: dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }
    
}
