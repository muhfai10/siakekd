/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.aspek;

import com.muhfai.uploadtest.dto.AspekDto;
import com.muhfai.uploadtest.model.Aspek;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class AspekRepository implements AbstractRepository<Aspek,AspekDto>{

    public List<AspekDto> findAllAspek(){
        List<Aspek> listAspek = Aspek.findAll();
        return convertToDto(listAspek);
    }
    public AspekDto findAspekById(Integer id){
        List<Aspek> listAspek = Aspek.find("id_aspek = ?",id);
            return convertToDto(listAspek.get(0));
    }
    
    @Override
    public AspekDto convertToDto(Aspek model) {
         AspekDto dto = null;
		if(model!=null){
			dto = new AspekDto();
			dto.setIdAspek(model.getInteger("id_aspek"));
			dto.setAspek(model.getString("aspek"));
		}
		return dto;
    }

    @Override
    public List<AspekDto> convertToDto(List<Aspek> modelList) {
         List<AspekDto> dtoList = null;
		if(modelList != null){
			dtoList = new ArrayList<AspekDto>();
			for(Aspek model:modelList){
				dtoList.add(convertToDto(model));
			}
		}
		return dtoList;
    }

    @Override
    public Aspek convertFromDto(AspekDto dto) {
         Aspek model = null;
	        if (dto != null) {
	            model = new Aspek();
	            model.setInteger("id_aspek", dto.getIdAspek());
	            model.setString("aspek",dto.getAspek());
	        }
	        return model;
    }

    @Override
    public List<Aspek> convertFromDto(List<AspekDto> dtoList) {
        List<Aspek> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<Aspek>();
            for (AspekDto dto: dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }
    
}
