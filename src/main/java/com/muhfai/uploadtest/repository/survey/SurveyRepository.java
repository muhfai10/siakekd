/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.survey;

import com.muhfai.uploadtest.dto.SurveyDto;
import com.muhfai.uploadtest.model.Survey;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class SurveyRepository implements AbstractRepository<Survey,SurveyDto>{
    
    public Integer insertSurvey(SurveyDto dto) {
        Survey model = convertFromDto(dto);
        model.insert();
        return model.getInteger("id_survey");
    }
    
    public Integer deleteSurveyById(Integer idSurvey){
        Survey model = Survey.findFirst("id_survey = ?", idSurvey);
        model.delete();
        return 1;
    }
    
    public SurveyDto findSurveyByMhsDosen(String noBp,String kodeDosen,Integer idPerkuliahan){
        List<Survey> listSurvey = Survey.find("no_bp= ? AND kode_dosen=? AND id_perkuliahan=?",noBp,kodeDosen,idPerkuliahan);
            return convertToDto(listSurvey.get(0));
    }
    
    @Override
    public SurveyDto convertToDto(Survey model) {
       SurveyDto dto = null;
		if(model!=null){
			dto = new SurveyDto();
			dto.setIdSurvey(model.getInteger("id_survey"));
			dto.setIdPerkuliahan(model.getInteger("id_perkuliahan"));
                        dto.setTanggal(model.getDate("tanggal"));
                        dto.setNoBp(model.getString("no_bp"));
                        dto.setKodeDosen(model.getString("kode_dosen"));
                        dto.setIdSemester(model.getInteger("semester"));
                       
		}
		return dto;
    }

    @Override
    public List<SurveyDto> convertToDto(List<Survey> modelList) {
        List<SurveyDto> dtoList = null;
		if(modelList != null){
			dtoList = new ArrayList<SurveyDto>();
			for(Survey model:modelList){
				dtoList.add(convertToDto(model));
			}
		}
		return dtoList;
    }

    @Override
    public Survey convertFromDto(SurveyDto dto) {
         Survey model = null;
	        if (dto != null) {
	            model = new Survey();
	            model.setInteger("id_survey", dto.getIdSurvey());
	            model.setInteger("id_perkuliahan",dto.getIdPerkuliahan());
                    model.setDate("tanggal",dto.getTanggal());
                    model.setString("no_bp",dto.getNoBp());
                    model.setString("kode_dosen",dto.getKodeDosen());
                    model.setInteger("semester",dto.getIdSemester());
	        }
	        return model;
    }

    @Override
    public List<Survey> convertFromDto(List<SurveyDto> dtoList) {
        List<Survey> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<Survey>();
            for (SurveyDto dto: dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }

    
}
