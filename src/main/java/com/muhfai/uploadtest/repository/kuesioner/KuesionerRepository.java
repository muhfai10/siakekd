/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.kuesioner;

import com.muhfai.uploadtest.dto.KuesionerDto;
import com.muhfai.uploadtest.model.Kuesioner;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class KuesionerRepository implements AbstractRepository<Kuesioner, KuesionerDto> {

    public List<KuesionerDto> findAllKuesioner() {
        List<Kuesioner> listKuisioner = Kuesioner.findAll();
        return convertToDto(listKuisioner);
    }

    public KuesionerDto findKuesionerById(Integer idKuesioner) {
        Kuesioner kuesioner = Kuesioner.findById(idKuesioner);
        return convertToDto(kuesioner);
    }
    
    public List<KuesionerDto> findKuesionerByAspek(Integer idAspek){
        List<Kuesioner> listKuesioner = Kuesioner.find("aspek = ?",idAspek);
        return convertToDto(listKuesioner);
    }
    public List<KuesionerDto> findPertanyaan(String pertanyaan){
        List<Kuesioner> kuesionerList = Kuesioner.where("pertanyaan LIKE '%"+pertanyaan+"%'");
        return convertToDto(kuesionerList);
    }

    public Integer insertKuesioner(KuesionerDto dto) {
        Kuesioner model = convertFromDto(dto);
        model.insert();
        return model.getInteger("id_kuesioner");
    }

    public boolean updateKuesioner(KuesionerDto dto) {
        Kuesioner model = convertFromDto(dto);
        return model.saveIt();
    }

    public Integer deleteKuesionerById(Integer idKuesioner) {
        Kuesioner model = Kuesioner.findFirst("id_kuesioner = ?", idKuesioner);
        model.delete();
        return 1;
    }

    @Override
    public KuesionerDto convertToDto(Kuesioner model) {
        KuesionerDto dto = null;
        if (model != null) {
            dto = new KuesionerDto();
            dto.setIdKuesioner(model.getInteger("id_kuesioner"));
            dto.setPertanyaan(model.getString("pertanyaan"));
            dto.setAspek(model.getInteger("aspek"));
        }
        return dto;
    }

    @Override
    public List<KuesionerDto> convertToDto(List<Kuesioner> modelList) {
        List<KuesionerDto> dtoList = null;
        if (modelList != null) {
            dtoList = new ArrayList<KuesionerDto>();
            for (Kuesioner model : modelList) {
                dtoList.add(convertToDto(model));
            }
        }
        return dtoList;
    }

    @Override
    public Kuesioner convertFromDto(KuesionerDto dto) {
        Kuesioner model = null;
        if (dto != null) {
            model = new Kuesioner();
            model.setInteger("id_kuesioner", dto.getIdKuesioner());
            model.setString("pertanyaan", dto.getPertanyaan());
            model.setInteger("aspek", dto.getAspek());
        }
        return model;
    }

    @Override
    public List<Kuesioner> convertFromDto(List<KuesionerDto> dtoList) {
        List<Kuesioner> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<Kuesioner>();
            for (KuesionerDto dto : dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }

}
