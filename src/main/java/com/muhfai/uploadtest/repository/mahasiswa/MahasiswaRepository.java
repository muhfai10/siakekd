/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.mahasiswa;

import com.muhfai.uploadtest.dto.MahasiswaDto;
import com.muhfai.uploadtest.model.Mahasiswa;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class MahasiswaRepository implements AbstractRepository<Mahasiswa, MahasiswaDto> {

    public MahasiswaDto findMahasiswaById(String noBp) {
        Mahasiswa listMahasiswa = Mahasiswa.findById(noBp);
        return convertToDto(listMahasiswa);
    }

    public List<MahasiswaDto> findAllMahasiswa() {
        List<Mahasiswa> listMahasiswa = Mahasiswa.findAll();
        return convertToDto(listMahasiswa);

    }
    
     public List<MahasiswaDto> findMahasiswa(String freeText){
        List<Mahasiswa> mahasiswaList = Mahasiswa.where("no_bp LIKE '%"+freeText+"%' OR nama LIKE '%"+freeText
                + "%' OR lokal LIKE '%"+freeText+"%' ");
        return convertToDto(mahasiswaList);
    }

    public String insertMahasiswa(MahasiswaDto dto) {
        Mahasiswa model = convertFromDto(dto);
        model.insert();
        return model.getString("no_bp");
    }

    public boolean updateMahasiswa(MahasiswaDto dto) {
        Mahasiswa model = convertFromDto(dto);
        return model.saveIt();
    }

    public Integer deleteMahasiswaById(String noBp) {
        Mahasiswa model = Mahasiswa.findFirst("no_bp = ?", noBp);
        model.delete();
        return 1;
    }

    @Override
    public MahasiswaDto convertToDto(Mahasiswa model) {
        MahasiswaDto dto = null;
        if (model != null) {
            dto = new MahasiswaDto();
            dto.setNoBp(model.getString("no_bp"));
            dto.setNama(model.getString("nama"));
            dto.setKelas(model.getInteger("kelas"));
            dto.setLokal(model.getString("lokal"));
            dto.setStatus(model.getInteger("status"));
            //dto.setSemester(model.getInteger("semester"));
            //dto.setProdi(model.getInteger("prodi"));
        }
        return dto;
    }

    @Override
    public List<MahasiswaDto> convertToDto(List<Mahasiswa> modelList) {
        List<MahasiswaDto> dtoList = null;
        if (modelList != null) {
            dtoList = new ArrayList<MahasiswaDto>();
            for (Mahasiswa model : modelList) {
                dtoList.add(convertToDto(model));
            }
        }
        return dtoList;
    }

    @Override
    public Mahasiswa convertFromDto(MahasiswaDto dto) {
        Mahasiswa model = null;
        if (dto != null) {
            model = new Mahasiswa();
            model.setString("no_bp", dto.getNoBp());
            model.setString("nama", dto.getNama());
            model.setInteger("kelas", dto.getKelas());
            model.setString("lokal", dto.getLokal());
            model.setInteger("status", dto.getStatus());
            //model.setInteger("semester",dto.getSemester());
            //model.setInteger("prodi",dto.getProdi());
        }
        return model;
    }

    @Override
    public List<Mahasiswa> convertFromDto(List<MahasiswaDto> dtoList) {
        List<Mahasiswa> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<Mahasiswa>();
            for (MahasiswaDto dto : dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }

}
