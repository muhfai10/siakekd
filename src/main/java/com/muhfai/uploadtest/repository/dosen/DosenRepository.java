/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.dosen;

import com.muhfai.uploadtest.dto.DosenDto;
import com.muhfai.uploadtest.model.Dosen;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class DosenRepository implements AbstractRepository<Dosen,DosenDto> {

    public List<DosenDto> findAllDosen(){
        List<Dosen> listDosen = Dosen.findAll();
        return convertToDto(listDosen);
    }
    public DosenDto findDosenById(String idDosen){
        Dosen dosen = Dosen.findById(idDosen);
            return convertToDto(dosen);
    }
    public List<DosenDto> findDosen(String freeText){
        List<Dosen> dosenList = Dosen.where("kode_dosen LIKE '%"+freeText+"%' OR nama LIKE '%"+freeText
                + "%' OR nip LIKE '%"+freeText+"%' OR nidn LIKE '%"+freeText+"%'");
        return convertToDto(dosenList);
    }
    
    public String insertDosen(DosenDto dto) {
        Dosen model = convertFromDto(dto);
        model.insert();
        return model.getString("kode_dosen");
    }
    public boolean updateDosen(DosenDto dto){
        Dosen model = convertFromDto(dto);
        return model.saveIt();
    }
    public Integer deleteDosenById(String kodeDosen){
        Dosen model = Dosen.findFirst("kode_dosen = ?", kodeDosen);
        model.delete();
        return 1;
    }
    @Override
    public DosenDto convertToDto(Dosen model) {
        DosenDto dto = null;
		if(model!=null){
			dto = new DosenDto();
			dto.setKodeDosen(model.getString("kode_dosen"));
			dto.setNama(model.getString("nama"));
                        dto.setNip(model.getString("nip"));
                        dto.setNidn(model.getString("nidn"));
                        dto.setNoTelp(model.getString("no_telp"));
                        dto.setEmail(model.getString("email"));
		}
		return dto;
    }

    @Override
    public List<DosenDto> convertToDto(List<Dosen> modelList) {
        List<DosenDto> dtoList = null;
		if(modelList != null){
			dtoList = new ArrayList<DosenDto>();
			for(Dosen model:modelList){
				dtoList.add(convertToDto(model));
			}
		}
		return dtoList;
    }

    @Override
    public Dosen convertFromDto(DosenDto dto) {
         Dosen model = null;
	        if (dto != null) {
	            model = new Dosen();
	            model.setString("kode_dosen", dto.getKodeDosen());
	            model.setString("nama",dto.getNama());
                    model.setString("nip",dto.getNip());
                    model.setString("nidn",dto.getNidn());
                    model.setString("no_telp",dto.getNoTelp());
                    model.setString("email",dto.getEmail());
	        }
	        return model;
    }

    @Override
    public List<Dosen> convertFromDto(List<DosenDto> dtoList) {
       List<Dosen> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<Dosen>();
            for (DosenDto dto: dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }
    
}
