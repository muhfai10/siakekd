/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.kelas;

import com.muhfai.uploadtest.dto.KelasDto;
import com.muhfai.uploadtest.model.Kelas;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class KelasRepository implements AbstractRepository<Kelas,KelasDto>{

    public KelasDto findKelasById(Integer idKelas){
        List<Kelas> listKelas = Kelas.find("id_kelas = ?",idKelas);
        return convertToDto(listKelas.get(0));
    }
    
    public List<KelasDto> findAllKelas(){
        List<Kelas> listKelas = Kelas.findAll();
        return convertToDto(listKelas);

    }
     public Integer insertKelas(KelasDto dto) {
        Kelas model = convertFromDto(dto);
        model.insert();
        return model.getInteger("id_kelas");
    }

    public boolean updateKelas(KelasDto dto) {
        Kelas model = convertFromDto(dto);
        return model.saveIt();
    }

    public Integer deleteKelasById(Integer idKelas) {
        Kelas model = Kelas.findFirst("id_kelas = ?", idKelas);
        model.delete();
        return 1;
    }
    
    @Override
    public KelasDto convertToDto(Kelas model) {
        KelasDto dto = null;
		if(model!=null){
			dto = new KelasDto();
			dto.setIdKelas(model.getInteger("id_kelas"));
			dto.setKelas(model.getString("kelas"));
                        dto.setProdi(model.getInteger("prodi"));
                        dto.setKode(model.getString("kode"));
                        
		}
		return dto;
    }

    @Override
    public List<KelasDto> convertToDto(List<Kelas> modelList) {
        List<KelasDto> dtoList = null;
		if(modelList != null){
			dtoList = new ArrayList<KelasDto>();
			for(Kelas model:modelList){
				dtoList.add(convertToDto(model));
			}
		}
		return dtoList;
    }

    @Override
    public Kelas convertFromDto(KelasDto dto) {
         Kelas model = null;
	        if (dto != null) {
	            model = new Kelas();
	            model.setInteger("id_kelas", dto.getIdKelas());
	            model.setString("kelas",dto.getKelas());
                    model.setInteger("prodi",dto.getProdi());
                    model.setString("kode",dto.getKode());
	        }
	        return model;
    }

    @Override
    public List<Kelas> convertFromDto(List<KelasDto> dtoList) {
        List<Kelas> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<Kelas>();
            for (KelasDto dto: dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }
    
}
