/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muhfai.uploadtest.repository.prodi;

import com.muhfai.uploadtest.dto.ProdiDto;
import com.muhfai.uploadtest.model.Prodi;
import com.muhfai.uploadtest.repository.AbstractRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MuhFai10
 */
@Repository
public class ProdiRepository implements AbstractRepository<Prodi,ProdiDto> {

    public List<ProdiDto> findAllProdi(){
        List<Prodi> listProdi = Prodi.findAll();
        return convertToDto(listProdi);
    }
    public ProdiDto findProdiById(Integer id){
        List<Prodi> listProdi = Prodi.find("id_prodi = ?",id);
            return convertToDto(listProdi.get(0));
    }
    @Override
    public ProdiDto convertToDto(Prodi model) {
        ProdiDto dto = null;
		if(model!=null){
			dto = new ProdiDto();
			dto.setIdProdi(model.getInteger("id_prodi"));
			dto.setProdi(model.getString("prodi"));
                        dto.setKode(model.getString("kode"));
		}
		return dto;
    }

    @Override
    public List<ProdiDto> convertToDto(List<Prodi> modelList) {
         List<ProdiDto> dtoList = null;
		if(modelList != null){
			dtoList = new ArrayList<ProdiDto>();
			for(Prodi model:modelList){
				dtoList.add(convertToDto(model));
			}
		}
		return dtoList;
    }

    @Override
    public Prodi convertFromDto(ProdiDto dto) {
         Prodi model = null;
	        if (dto != null) {
	            model = new Prodi();
	            model.setInteger("id_prodi", dto.getIdProdi());
	            model.setString("prodi",dto.getProdi());
                    model.setString("kode",dto.getKode());
	        }
	        return model;
    }

    @Override
    public List<Prodi> convertFromDto(List<ProdiDto> dtoList) {
        List<Prodi> modelList = null;
        if (dtoList != null) {
            modelList = new ArrayList<Prodi>();
            for (ProdiDto dto: dtoList) {
                modelList.add(convertFromDto(dto));
            }
        }
        return modelList;
    }
    
}
