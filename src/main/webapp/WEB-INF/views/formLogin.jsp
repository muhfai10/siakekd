<%-- 
    Document   : formLogin
    Created on : Jul 16, 2019, 2:05:02 PM
    Author     : MuhFai10
--%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>   
<%@page contentType="text/html" pageEncoding="windows-1252"%>

        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <title>Login Page</title>
   
  
        <div class="login-box">
            <div class="login-logo">
                <img src="/image/logo2.jpg" class="img-circle" style="width:120px; height:120px;">

            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <h5 class="login-box-msg">Portal Evaluasi Kinerja Dosen<br>Teknologi Informasi<br><b>Politeknik Negeri Padang</b></h5>

                <form class="" role="form" action="${pageContext.request.contextPath}/login/loginAccess" method="POST" >
                    <div class="form-group has-feedback">
                        <input type="text" name="username" class="form-control" id="userId" placeholder="Username" required="" autofocus="" />
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password"  name="password"  class="form-control" id="password" placeholder="Password" required="" />
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Login</button>
                        </div>
                        
                        <div class="col-xs-4">
                            
                        </div>
                        <!-- /.col -->
                    </div>
                    <br>
                    <h5 align="center"><b>� 2019 - Developed by MuhFai</b></h5>
                </form>

                <script type="text/javascript">
                    function preback() {
                        window.history.forward();
                    }
                    setTimeout("preback()", 0);
                    window.onunload = function () {
                        null
                    }
                </script>

            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box 
        
        <body>
           
                <form method="POST" action="${pageContext.request.contextPath}/login/loginAccess">
                <table style="with: 100%">
                <tr>
                 <td>UserName</td>
                 <td><input type="text" name="username" /></td>
                </tr>
                <tr>
                 <td>Password</td>
                 <td><input type="password" name="password" /></td>
                </tr>
        
                </table>
                <input type="submit" value="Submit" />
                        
                </form>	
        </body>
        
        </html>
        
        -->