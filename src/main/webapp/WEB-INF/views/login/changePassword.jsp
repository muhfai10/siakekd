<%-- 
    Document   : formUpdate
    Created on : Aug 13, 2019, 12:11:07 AM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css">
    .errormsg {
        color: red;
    }
</style>
<title>Change Password</title>

<section class="content-header">
    <h1>
        Form Change Password
        <small>Mahasiswa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Form Change Password</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Change Password</h3>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <c:if test="${not empty message}">
                        <div class="alert alert-error alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                            <i class="icon fa fa-check"></i>
                            ${message}
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="pull-right">
                <a href="${pageContext.request.contextPath}/login/successLoginMahasiswa" class="btn bg-yellow-gradient"><i class="fa fa-undo"></i> Kembali</a>
            </div>
        </div>
        <form:form role="form" action="${pageContext.request.contextPath}/login/updatePassword" method="post" modelAttribute="changeForm">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="form-group">
                            <label for="password">Password Lama</label>
                            <form:input type="password" path="password" class="form-control" id="password" placeholder="Password Lama" />
                            <small><form:errors path="password" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="konfirmPassword">Konfirmasi Password</label>
                            <form:input type="password" path="konfirmPassword" class="form-control" id="konfirmPassword" placeholder="Konfirmasi Password"/>
                            <small><form:errors path="konfirmPassword" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="newPassword">Password Baru</label>
                            <form:input type="password" path="newPassword" class="form-control" id="newPassword" placeholder="Password Baru"/>
                            <small><form:errors path="newPassword" cssClass="errormsg" /></small>
                        </div>
                        <form:hidden path="idUser"/>
                        <form:hidden path="username"/>
                        <form:hidden path="nama"/>
                        <form:hidden path="role"/>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-paper-plane"></i> Submit</button>
                                    <button type="reset" class="btn bg-green-gradient"><i class="fa fa-refresh"></i> Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form:form>
        </div>
    </div>
</section>
