<%-- 
    Document   : form
    Created on : Aug 12, 2019, 10:31:29 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css">
    .errormsg {
        color: red;
    }
</style>
<link href="src/selectstyle.css" rel="stylesheet">
<script src="//code.jquery.com/jquery.min.js"></script>
<script src="src/selectstyle.js"></script>

<title>Perkuliahan</title>
<section class="content-header">
    <h1>
        Form Data
        <small>Perkuliahan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Form Perkuliahan</li>
    </ol>
</section>
<section class="content">
    <h3>${message}</h3>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Form Input Data Perkuliahan</h3>
            <div class="pull-right">
                <a href="${pageContext.request.contextPath}/perkuliahan/listPerkuliahan" class="btn bg-yellow-gradient"><i class="fa fa-undo"></i> Kembali</a>
            </div>
        </div>
        <form:form role="form" action="${pageContext.request.contextPath}/perkuliahan/insertPerkuliahan" method="post" modelAttribute="perkuliahan">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <form:hidden path="idPerkuliahan"/>
                        <div class="form-group">
                            <label for="matakuliah">Matakuliah</label>
                            <form:select path="kodeMk" class="form-control select2" theme="google" data-search="true">
                                <form:options items="${mapMatkul}" />
                            </form:select>
                            <small><form:errors path="kodeMk" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="kodeDosen">Nama Dosen 1</label>
                            <form:select path="kodeDosen" class="form-control select2">
                                <form:options items="${mapDosen}" />
                            </form:select>
                            <small><form:errors path="kodeDosen" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="kodeDosen2">Nama Dosen 2</label>
                            <form:select path="kodeDosen2" class="form-control select2">
                                <form:option value="" label="--- NONE ---"/>
                                <form:options items="${mapDosen2}" />
                            </form:select>
                            <small><form:errors path="kodeDosen2" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="idKelas">Tingkat / Kelas</label>
                            <form:select path="idKelas" class="form-control select2">
                                <form:options items="${mapKelas}" />
                            </form:select>
                            <small><form:errors path="idKelas" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="idSemester">Semester</label>
                            <form:select path="idSemester" class="form-control select2">
                                <form:options items="${mapSemester}" />
                            </form:select>
                            <small><form:errors path="idSemester" cssClass="errormsg" /></small>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-paper-plane"></i> Submit</button>
                            <button type="reset" class="btn bg-green-gradient"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</section>
            <script type="text/javascript">
                $('select').selectstyle();
                
                </script>


