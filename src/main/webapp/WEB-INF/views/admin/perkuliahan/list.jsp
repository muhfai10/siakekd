<%-- 
    Document   : list
    Created on : Aug 12, 2019, 5:58:28 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page contentType="text/html" pageEncoding="windows-1252"%>

<!DOCTYPE html>
<title>Perkuliahan</title>
<section class="content-header">
    <h1>
        List Data
        <small>Perkuliahan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List Perkuliahan</li>
    </ol>
</section>
<section class="content">
    <c:if test="${not empty message}">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
            <i class="icon fa fa-check"></i>
            ${message}
        </div>
    </c:if>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h1 class="box-title">List Data Perkuliahan</h1>
            <br><br>

            <div class="row">
                <div class="col-sm-4">
                    <a href="${pageContext.request.contextPath}/perkuliahan/newPerkuliahan" class="btn bg-blue-gradient">
                        <i class="fa fa-user-plus fa-lg"></i> Tambah Data
                    </a>
                </div>

                <div class="col-sm-4">

                    <form class="form-inline" action="${pageContext.request.contextPath}/perkuliahan/searchBySemester">
                        <select name="semesterNow">
                            <c:forEach items="${listSmt}" var="semester">
                                <option value="${semester.idSemester}">
                                    ${semester.semester} / ${semester.tahunAjaran}
                                </option>
                            </c:forEach>
                        </select>
                        <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-search"></i> Search</button>
                    </form>

                </div>
                <div class="col-sm-4 col-xs-4">
                    <form class="form-inline" action="${pageContext.request.contextPath}/perkuliahan/searchPerkuliahan">
                        <input type="text" name="freeText" class="form-control" id="freeText" value="${param.freeText}" placeholder="Kode Mk"/>
                        <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-search"></i> Search</button>
                    </form>
                </div>
            </div>
            <br>
            <c:if test="${messageSearch eq 1}">
                <div class="row">
                    <div class="col-sm-4">

                    </div>
                    <div class="col-sm-4">
                        <form class="form-inline" action="${pageContext.request.contextPath}/perkuliahan/updateList/?semesterNow=${idSemester}" method="post">
                            <select name="semesterNew">
                                <c:forEach items="${listSmtr}" var="semester">
                                    <option value="${semester.idSemester}">
                                        ${semester.semester} / ${semester.tahunAjaran}
                                    </option>
                                </c:forEach>
                            </select>
                            <button type="submit" class="btn bg-blue-gradient"><i class="fa  fa-edit"></i> Update by Semester</button>
                        </form>
                    </div>
                    <div class="col-sm-4">

                    </div>
                </div>
            </c:if>
        </div>
        <br>
        <div class="box-body table-responsive">
            <table id="table_dosen" class="table table-striped table-bordered" >

                <thead>
                    <!--bgcolor="#0082d0"<font color="white">-->
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Matakuliah</th>
                        <th class="text-center">Kode Matakuliah</th>
                        <th class="text-center">Tingkat / Kelas</th>
                        <th class="text-center">Nama Dosen 1</th>
                        <th class="text-center">Nama Dosen 2</th>
                        <th class="text-center">Semester</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listPerkuliahan}" var="perkuliahan" varStatus="status">
                        <tr>    

                            <td><c:out value="${status.count}" /></td>
                            <td><c:out value="${perkuliahan.matakuliah}" /></td>
                            <td><c:out value="${perkuliahan.kodeMk}" /></td>
                            <td><c:out value="${perkuliahan.kelas}" /></td>
                            <td><c:out value="${perkuliahan.namaDosen}" /></td>
                            <td><c:out value="${perkuliahan.namaDosen2}" /></td>
                            <td><c:out value="${perkuliahan.semester}" /></td>
                            <td><a href="${pageContext.request.contextPath}/perkuliahan/editPerkuliahan/${perkuliahan.idPerkuliahan}" class="btn bg-blue-gradient btn-sm"><i class="fa fa-pencil fa-lg"></i>Edit</a> |
                                <a href="${pageContext.request.contextPath}/perkuliahan/deletePerkuliahan/${perkuliahan.idPerkuliahan}" class="btn bg-yellow-gradient btn-sm"><i class="fa fa-trash fa-lg"></i>Delete</a></td>

                        </tr>
                    </c:forEach>


            </table>
           <nav aria-label="Page navigation example">
                <ul class="pagination">

                <c:url value="/perkuliahan/listPerkuliahan" var="prev">
                    <c:param name="page" value="${page-1}"/>
                </c:url>
                <c:if test="${page > 1}">
                    <li class="page-item"><a href="<c:out value="${prev}" />" class="pn prev">Prev</a></li>
                </c:if>

                <c:forEach begin="1" end="${maxPages}" step="1" varStatus="i">
                    <c:choose>
                        <c:when test="${page == i.index}">
                            <li class="page-item active"><span>${i.index}</span></li>
                        </c:when>
                        <c:otherwise>
                            <c:url value="/perkuliahan/listPerkuliahan" var="url">
                                <c:param name="page" value="${i.index}"/>
                            </c:url>
                        <li class="page-item"><a href='<c:out value="${url}" />'>${i.index}</a></li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:url value="/perkuliahan/listPerkuliahan" var="next">
                    <c:param name="page" value="${page + 1}"/>
                </c:url>
                <c:if test="${page + 1 <= maxPages}">
                <li class="page-item"><a href='<c:out value="${next}" />' class="pn next">Next</a></li>
                </c:if>
                        
                </ul>
            </nav>
            </div>
        </div>

 
</section>
