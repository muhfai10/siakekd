<%-- 
    Document   : list
    Created on : Aug 12, 2019, 5:58:28 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page contentType="text/html" pageEncoding="windows-1252"%>

<!DOCTYPE html>


<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Data Mahasiswa</title>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        List Data
        <small>Mahasiswa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List Mahasiswa</li>
    </ol>
</section>
<section class="content">
    <c:if test="${not empty message}">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
            <i class="icon fa fa-check"></i>
            ${message}
        </div>
    </c:if>

    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title">List Data Mahasiswa</h2>
            <br><br>
            <div class="pull-left">
                <a href="${pageContext.request.contextPath}/mahasiswa/getImport" class="btn bg-green-gradient">
                    <i class="fa fa-upload fa-lg"></i> Import Data
                </a>
                     <a href="${pageContext.request.contextPath}/mahasiswa/getExcel" class="btn bg-green-gradient">
                    <i class="fa fa-download fa-lg"></i> Export Format
                </a>
            </div>
            <br><br>
            <div class="row2">
                <div class="col-sm-4">
                    <a href="${pageContext.request.contextPath}/mahasiswa/newMahasiswa" class="btn bg-blue-gradient">
                        <i class="fa fa-user-plus fa-lg"></i> Tambah Data
                    </a>
                </div>
                <div class="col-sm-4">
                    <form class="form-inline" action="${pageContext.request.contextPath}/mahasiswa/searchMahasiswa">
                        <input type="text" name="freeText" class="form-control" id="freeText" value="${param.freeText}" placeholder="No BP, Nama, Lokal"/>
                        <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-search"></i> Search</button>
                    </form>
                </div>
            </div>
        </div>
        <br>
        <div class="box-body table-responsive">
            <table id="table_dosen" class="table table-bordered table-hover" >

                <thead>
                    <tr>
                        <th>No</th>
                        <th>No BP</th>
                        <th>Nama Mahasiswa</th>
                        <th>Kelas</th>
                        <th>Tingkat</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listMahasiswa}" var="mahasiswa" varStatus="status">
                        <tr>    

                            <td><c:out value="${status.count}" /></td>
                            <td><c:out value="${mahasiswa.noBp}" /></td>
                            <td><c:out value="${mahasiswa.nama}" /></td>
                            <td><c:out value="${mahasiswa.lokal}" /></td>
                            <td><c:out value="${mahasiswa.kelas}" /></td>
                            <td><c:out value="${mahasiswa.status}" /></td>
                            <td><a href="${pageContext.request.contextPath}/mahasiswa/editMahasiswa/${mahasiswa.noBp}" class="btn bg-blue-gradient btn-sm"><i class="fa fa-pencil fa-lg"></i>Edit</a> | 
                                <a href="${pageContext.request.contextPath}/mahasiswa/deleteMahasiswa/${mahasiswa.noBp}" class="btn bg-yellow-gradient btn-sm"><i class="fa fa-trash fa-lg"></i>Delete</a></td>

                        </tr>
                    </c:forEach>


            </table>
            <nav aria-label="Page navigation example">
                <ul class="pagination">

                    <c:url value="/mahasiswa/listMahasiswa" var="prev">
                        <c:param name="page" value="${page-1}"/>
                    </c:url>
                    <c:if test="${page > 1}">
                        <li class="page-item"><a href="<c:out value="${prev}" />" class="pn prev">Prev</a></li> 
                    </c:if>

                    <c:forEach begin="1" end="${maxPages}" step="1" varStatus="i">
                        <c:choose>
                            <c:when test="${page == i.index}">
                                <li class="page-item active"><span>${i.index}</span></li> 
                            </c:when>
                            <c:otherwise>
                                <c:url value="/mahasiswa/listMahasiswa" var="url">
                                    <c:param name="page" value="${i.index}"/>
                                </c:url>
                                <li class="page-item"><a href='<c:out value="${url}" />'>${i.index}</a></li> 
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <c:url value="/mahasiswa/listMahasiswa" var="next">
                        <c:param name="page" value="${page + 1}"/>
                    </c:url>
                    <c:if test="${page + 1 <= maxPages}">
                        <li class="page-item"><a href='<c:out value="${next}" />' class="pn next">Next</a></li> 
                    </c:if>
                </ul>
            </nav>
        </div>
    </div>

</section>


