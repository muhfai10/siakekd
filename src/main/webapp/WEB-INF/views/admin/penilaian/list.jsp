<%-- 
    Document   : list
    Created on : Aug 27, 2019, 12:13:06 AM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Penilaian</title>

<section class="content-header">
    <h1>
        List Data
        <small>Penilaian</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List Penilaian</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h1 class="box-title">List Data Penilaian Dosen</h1>
            <br>
            <br>
            <c:if test="${not empty dataPenilaianList}">
                <div id="chartContainer" style="height: 200px; width: 100%;"></div>
            </c:if>
            <br>
            <br>
            <div class="row2">
                <div class="col-sm-4">
                    <a href="${pageContext.request.contextPath}/penilaian/downloadPdf/${idProdi}/?idSemester=${idSemester}" class="btn bg-blue-gradient">
                        <i class="fa fa-download fa-lg"></i> Download PDF
                    </a>

                </div>
                <div class="col-sm-4">
                    <form class="form-inline" action="${pageContext.request.contextPath}/penilaian/searchPenilaian/${idProdi}">
                        <select name="semester">
                            <c:forEach items="${listSmt}" var="semester">
                                <option value="${semester.idSemester}">
                                    ${semester.semester} / ${semester.tahunAjaran}
                                </option>
                            </c:forEach>
                        </select>
                        <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-search"></i> Search</button>
                    </form>
                </div>
            </div>

        </div>

        <script type="text/javascript">
            window.onload = function () {

                var dps = [[]];
                var chart = new CanvasJS.Chart("chartContainer", {
                    theme: "light1", // "light1", "light2", "dark1"
                    animationEnabled: true,
                    title: {
                        text: "Grafik Penilaian Evaluasi Kinerja Dosen"
                    },
                    axisY: {
                        title: "",
                        prefix: "",
                        suffix: ""
                    },
                    data: [{
                            type: "spline",
                            yValueFormatString: "#,##0.0",
                            indexLabel: "{y}",
                            dataPoints: dps[0]
                        }]
                });

                var yValue;
                var label;

            <c:forEach items="${dataPenilaianList}" var="dataPoints" varStatus="loop">
                <c:forEach items="${dataPoints}" var="dataPoint">
                yValue = parseFloat("${dataPoint.y}");
                label = "${dataPoint.label}";
                dps[parseInt("${loop.index}")].push({
                    label: label,
                    y: yValue
                });
                </c:forEach>
            </c:forEach>

                chart.render();

            }
        </script>


        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
        <br>
        <br>
        <div class="box-body table-responsive">
            <table id="table_dosen" class="table table-bordered table-hover" >

                <thead>
                    <tr bgcolor="#0082d0">
                        <th class="text-center"><font color="white">No</th>
                        <th class="text-center"><font color="white">Nama Dosen</th>
                        <th class="text-center"><font color="white">Matakuliah</th>
                        <th class="text-center"><font color="white">Reliability</th>
                        <th class="text-center"><font color="white">Responsiveness</th>
                        <th class="text-center"><font color="white">Assurance</th>
                        <th class="text-center"><font color="white">Empathy</th>
                        <th class="text-center"><font color="white">Tangible</th>
                        <th class="text-center"><font color="white">Rata-Rata</th>

                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listPenilaian}" var="penilaian" varStatus="status">
                        <tr>    

                            <td class="text-center"><c:out value="${status.count}" /></td>
                            <td class="text-center"><c:out value="${penilaian.namaDosen}" /></td>
                            <td class="text-center"><c:out value="${penilaian.matakuliah}" /></td>
                            <td class="text-center"><c:out value="${penilaian.reliability}" /></td>
                            <td class="text-center"><c:out value="${penilaian.responsiveness}" /></td>
                            <td class="text-center"><c:out value="${penilaian.assurance}" /></td>
                            <td class="text-center"><c:out value="${penilaian.empathy}" /></td>
                            <td class="text-center"><c:out value="${penilaian.tangible}" /></td>
                            <td class="text-center"><c:out value="${penilaian.rerata}" /></td>

                        </tr>
                    </c:forEach>
            </table>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    

                        <c:url value="/penilaian/listPenilaian" var="prev">
                            <c:param name="page" value="${page-1}"/>
                        </c:url>
                        <c:if test="${page > 1}">
                            <li class="page-item"><a href="<c:out value="${prev}" />" class="pn prev">Prev</a></li> 
                        </c:if>

                        <c:forEach begin="1" end="${maxPages}" step="1" varStatus="i">
                            <c:choose>
                                <c:when test="${page == i.index}">
                                    <li class="page-item active"><span>${i.index}</span></li>
                                </c:when>
                                <c:otherwise>
                                    <c:url value="/penilaian/listPenilaian" var="url">
                                        <c:param name="page" value="${i.index}"/>
                                    </c:url>
                                    <li class="page-item"><a href='<c:out value="${url}" />'>${i.index}</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <c:url value="/penilaian/listPenilaian" var="next">
                            <c:param name="page" value="${page + 1}"/>
                        </c:url>
                        <c:if test="${page + 1 <= maxPages}">
                        <li class="page-item"><a href='<c:out value="${next}" />' class="pn next">Next</a></li> 
                        </c:if>
                  
                </ul>
            </nav>
        </div>

    </div>
</section>

