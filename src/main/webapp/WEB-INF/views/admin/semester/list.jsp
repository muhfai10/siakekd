<%-- 
    Document   : list
    Created on : Aug 12, 2019, 5:58:28 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page contentType="text/html" pageEncoding="windows-1252"%>

<!DOCTYPE html>


<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Data Semester</title>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        List Data
        <small>Semester</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List Semester</li>
    </ol>
</section>
<section class="content">
    <c:if test="${not empty message}">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
            <i class="icon fa fa-check"></i>
            ${message}
        </div>
    </c:if>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h1 class="box-title">List Data Semester</h1>
            <br><br>

            <div class="pull-left">
                <a href="${pageContext.request.contextPath}/semester/newSemester" class="btn bg-blue-gradient">
                    <i class="fa fa-user-plus fa-lg"></i> Tambah Data
                </a>
            </div>
        </div>
        <br>
        <div class="box-body table-responsive">
            <table id="table_dosen" class="table table-bordered table-hover" >

                <thead>
                    <tr>
                        <th>No</th>
                        <th>Semester</th>
                        <th>Tahun Ajaran</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listSemester}" var="semester" varStatus="status">
                        <tr>    

                            <td><c:out value="${status.count}" /></td>
                            <td><c:out value="${semester.semester}" /></td>
                            <td><c:out value="${semester.tahunAjaran}" /></td>
                            <td><c:out value="${semester.status}" /></td>
                            <td><a href="${pageContext.request.contextPath}/semester/editSemester/${semester.idSemester}" class="btn bg-blue-gradient btn-sm"><i class="fa fa-pencil fa-lg"></i>Edit</a> 
                        </tr>
                    </c:forEach>

            </table>
        </div>

    </div>
</section>


