<%-- 
    Document   : form
    Created on : Aug 12, 2019, 10:31:29 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>

<title>Input Data Dosen</title>

<style type="text/css">
    .errormsg {
        color: red;
    }
</style>

<section class="content-header">
    <h1>
        Form Data
        <small>Dosen</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Form Dosen</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Form Input Data Dosen</h3>
            <div class="pull-right">
                <a href="${pageContext.request.contextPath}/dosen/listDosen" class="btn bg-yellow-gradient"><i class="fa fa-undo"></i> Kembali</a>
            </div>
        </div>
        <form:form role="form" action="${pageContext.request.contextPath}/dosen/insertDosen" method="post" modelAttribute="dosen">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="form-group">
                            <label for="kodeDosen">Kode Dosen</label>
                            <form:input type="text" path="kodeDosen" class="form-control" id="kodeDosen" placeholder="Kode Dosen" maxlength="5"/>
                            <small><form:errors path="kodeDosen" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama Dosen</label>
                            <form:input type="text" path="nama" class="form-control" id="nama" placeholder="Nama Dosen"/>
                            <small><form:errors path="nama" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="nip">NIP Dosen</label>
                            <form:input type="text" path="nip" class="form-control" id="nip" placeholder="NIP" maxlength="21"/>
                            <small><form:errors path="nip" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="nidn">NIDN Dosen</label>
                            <form:input type="text" path="nidn" class="form-control" id="nidn" placeholder="NIDN" maxlength="10"/>
                            <small><form:errors path="nidn" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="noTelp">No. Telepon</label>
                            <form:input type="text" path="noTelp" class="form-control" id="noTelp" placeholder="No. Telepon" maxlength="12"/>
                            <small><form:errors path="noTelp" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <form:input type="text" path="email" class="form-control" id="email" placeholder="Email" maxlength="25"/>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-paper-plane"></i> Submit</button>
                            <button type="reset" class="btn bg-green-gradient"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                </div>

            </form:form>
        </div>
    </div>
</section>

