<%-- 
    Document   : loginSuccess
    Created on : Jul 16, 2019, 2:10:53 PM
    Author     : MuhFai10
--%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>

<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Kuesioner</title>
<section class="content-header">
    <h1>
        Selamat Datang,
        <small>${nama}!</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kuesioner</li>
    </ol>
</section>

<section class="content">
    <c:if test="${not empty message}">
        <c:choose>

            <c:when test="${message eq 'Input Survey Success'}">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                    <i class="icon fa fa-check"></i>
                    ${message}
                </div>
            </c:when>
            <c:when test="${message ne 'Input Survey Success'}">
                <div class="alert alert-error alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                    <i class="icon fa fa-check"></i>
                    ${message}
                </div>
            </c:when>
        </c:choose>
    </c:if>


    <div class="box box-primary">
        <br>
        <table id="table_dosen" class="table2 table-hover table-striped"  border="1">

            <tr>
                <th width="45px">No BP</th>
                <td width="45px">${noBp}</td>
            </tr>
            <tr>
                <th width="45px">Nama</th>
                <td width="45px">${nama}</td>
            </tr>

            <tr>
                <th width="45px">Tingkat</th>
                <td width="45px">${kelas}</td>
            </tr>
            <tr>
                <th width="45px">Kelas</th>
                <td width="45px">${lokal}</td>
            </tr>

        </table>

        <div class="box-body table-responsive">
            <table id="table_dosen" class="table table-hover table-bordered table-striped" >
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Kode Matkul</th>
                    <th class="text-center">Nama Matkul</th>
                    <th class="text-center">Nama Dosen 1</th>
                    <th class="text-center">Nama Dosen 2</th>

                </tr>
                <c:forEach items="${kuliahList}" var="kuliah" varStatus="status">
                    <tr>    

                        <td class="text-center"><c:out value="${status.count}" /></td>
                        <td style="display:none;"><c:out value="${kuliah.idPerkuliahan}" /></td>
                        <td style="display:none;"><c:out value="${kuliah.kodeDosen}" /></td>
                        <td style="display:none;"><c:out value="${kuliah.kodeDosen2}" /></td>
                        <td class="text-center"><c:out value="${kuliah.kodeMatkul}" /></td>
                        <td class="text-center"><c:out value="${kuliah.mataKuliah}" /></td>
                        <td class="text-center">
                            <c:set var = "namaDosen" scope = "session" value = "${kuliah.namaDosen}"/>
                            <c:choose>
                                <c:when test="${namaDosen=='Telah diisi'}">
                                    <a class="btn bg-green-gradient"><c:out value="${kuliah.namaDosen}" /></a>
                                </c:when>
                                <c:otherwise>
                                    <a href="${pageContext.request.contextPath}/login/${noBp}/toKuesioner/${kuliah.kodeDosen}/${kuliah.kodeMatkul}/${kuliah.idPerkuliahan}" class="btn bg-blue-gradient"><c:out value="${kuliah.namaDosen}" /></a>    
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="text-center">
                            <c:set var = "namaDosen2" scope = "session" value = "${kuliah.namaDosen2}"/>
                            <c:if test="${not empty namaDosen2}">
                                <c:choose>
                                    <c:when test="${namaDosen2=='Telah diisi'}">
                                        <a class="btn bg-green-gradient"><c:out value="${kuliah.namaDosen2}" /></a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="${pageContext.request.contextPath}/login/${noBp}/toKuesioner_/${kuliah.kodeDosen2}/${kuliah.kodeMatkul}/${kuliah.idPerkuliahan}" class="btn bg-blue-gradient"><c:out value="${kuliah.namaDosen2}" /></a>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>


                        </td>

                    </tr>
                </c:forEach>
            </table>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        <a href="${pageContext.request.contextPath}/login/downloadPdf" class="btn bg-green-gradient"> <i class="fa fa-print fa-lg"></i> Cetak Bukti Pengisian Kuesioner</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<script type="text/javascript">
    function preback() {
        window.history.forward();
    }
    setTimeout("preback()", 0);
    window.onunload = function () {
        null
    }
</script>
