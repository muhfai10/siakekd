<%-- 
    Document   : kuisioner
    Created on : Jul 27, 2019, 4:14:51 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>

        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>Kuesioner</title>
        <section class="content-header">
    <h1>
        Pengisian Kuesioner
        <small>${namaDosen} | ${nip}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pengisian Kuesioner</li>
    </ol>
</section>
    <section class="content">
         <form:form method="POST" action="${pageContext.request.contextPath}/login/saveSurvey" modelAttribute="surveyForm">
       
        <table id="table_dosen" class="table2 table-hover table-striped" border="1" width="425px" >
         <tr>
                <th style="display:none;">Id Perkuliahan</th>
                <td style="display:none;"><input type="text" value="${idPerkuliahan}" name="idPerkuliahan" readonly/></td>
            </tr>
            <tr>
                <th style="display:none;">Kode Dosen</th>
                <td style="display:none;"><input type="text" value="${kodeDosen}" name="kodeDosen" readonly/></td>
            </tr>
            <tr>
                <th style="display:none;">Semester</th>
                <td style="display:none;"><input type="text" value="${idSemester}" name="idSemester" readonly/></td>
            </tr>
             <tr>
                <th>Nama Dosen</th>
                <td>${namaDosen}</td>
            </tr>

            <tr>
                <th>NIDN</th>
                <td>${nidn}</td>
            </tr>      
            <tr>
                <th>Matakuliah</th>
                <td>${mataKuliah}</td>
            </tr>      
    </table>
     <div class="box box-primary">
        <div class="box-header with-border">
            <h1 class="box-title">Pengisian Kuesioner</h1>
             <div class="pull-right">
                <a href="${pageContext.request.contextPath}/login/successLoginMahasiswa" class="btn bg-yellow-gradient"><i class="fa fa-undo"></i> Kembali</a>
            </div>
        </div>
        <br>
            <div class="box-body table-responsive">
            <table id="table_dosen" class="table table-hover table-striped" >
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Kuesioner</th>
                    <th class="text-center">Sangat Setuju</th>
                    <th class="text-center">Setuju</th>
                    <th class="text-center">Cukup</th>
                    <th class="text-center">Kurang</th>

                </tr>
               <c:forEach items="${surveyForm.listSurvey}" var="survey" varStatus="status">
                    <tr>    

                        <td class="text-center"><c:out value="${status.count}" /></td>
                        <td style="display:none;"><input name="listSurvey[${status.index}].idKuesioner" value="${survey.idKuesioner}"/></td>
                    <td><c:out value="${survey.pertanyaan}" /></td>
                    <td class="text-center"><form:radiobutton path="listSurvey[${status.index}].nilai" value="4" /></td>
                    <td class="text-center"><form:radiobutton path="listSurvey[${status.index}].nilai" value="3" /></td>
                    <td class="text-center"><form:radiobutton path="listSurvey[${status.index}].nilai" value="2" /></td>
                    <td class="text-center"><form:radiobutton path="listSurvey[${status.index}].nilai" value="1" /></td>
                   

                    </tr>
                </c:forEach>
            </table>
                <br><br>
                <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-paper-plane"></i> Submit</button>
                    <button type="reset" class="btn bg-green-gradient"><i class="fa fa-refresh"></i> Reset</button>
                
                </form:form>
        </div>
        
             </div>
</section>
            
            
