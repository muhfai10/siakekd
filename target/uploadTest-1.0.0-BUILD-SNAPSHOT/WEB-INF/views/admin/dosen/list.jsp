<%-- 
    Document   : list
    Created on : Aug 12, 2019, 5:58:28 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Data Dosen</title>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        List Data
        <small>Dosen</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List Dosen</li>
    </ol>
</section>
<section class="content">
    <c:if test="${not empty message}">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
            <i class="icon fa fa-check"></i>
            ${message}
        </div>
    </c:if>
    <div class="box box-primary">
        <div class="box-header">
            <h1 class="box-title">List Data Dosen</h1>
            <br><br>
            <div class="row2">
                <div class="col-sm-4">
                    <a href="${pageContext.request.contextPath}/dosen/newDosen" class="btn bg-blue-gradient">
                        <i class="fa fa-user-plus fa-lg"></i> Tambah Data
                    </a>
                </div>
                <div class="col-sm-4">
                    <form class="form-inline" action="${pageContext.request.contextPath}/dosen/searchDosen">
                        <input type="text" name="freeText" class="form-control" id="freeText" value="${param.freeText}" placeholder="Kode Dosen, Nama, NIP, NIDN.."/>
                        <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-search"></i> Search</button>

                    </form>
                </div>

            </div>

        </div>

        <br>
        <div class="box-body table-responsive">
            <table id="table_dosen" class="table table-bordered table-hover" >

                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Dosen</th>
                        <th>Nama Dosen</th>
                        <th>NIP</th>
                        <th>NIDN</th>
                        <th>No. Telepon</th>
                        <th>Email</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listDosen}" var="dosen" varStatus="status">
                        <tr>    

                            <td><c:out value="${status.count}" /></td>
                            <td><c:out value="${dosen.kodeDosen}" /></td>
                            <td><c:out value="${dosen.nama}" /></td>
                            <td><c:out value="${dosen.nip}" /></td>
                            <td><c:out value="${dosen.nidn}" /></td>
                            <td><c:out value="${dosen.noTelp}" /></td>
                            <td><c:out value="${dosen.email}" /></td>
                            <td><a href="${pageContext.request.contextPath}/dosen/editDosen/${dosen.kodeDosen}" class="btn bg-blue-gradient btn-sm"><i class="fa fa-pencil fa-lg"></i>Edit</a> | 
                                <a href="${pageContext.request.contextPath}/dosen/deleteDosen/${dosen.kodeDosen}" class="btn bg-yellow-gradient btn-sm"><i class="fa fa-trash fa-lg"></i>Delete</a></td>

                        </tr>
                    </c:forEach>


            </table>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <c:url value="/dosen/listDosen" var="prev">
                        <c:param name="page" value="${page-1}"/>
                    </c:url>
                    <c:if test="${page > 1}">
                        <li class="page-item"><a href="<c:out value="${prev}" />" class="pn prev">Prev</a></li>
                    </c:if>

                    <c:forEach begin="1" end="${maxPages}" step="1" varStatus="i">
                        <c:choose>
                            <c:when test="${page == i.index}">
                                 <li class="page-item active"><span>${i.index}</span></li>
                            </c:when>
                            <c:otherwise>
                                <c:url value="/dosen/listDosen" var="url">
                                    <c:param name="page" value="${i.index}"/>
                                </c:url>
                                 <li class="page-item"><a href='<c:out value="${url}" />'>${i.index}</a></li> 
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <c:url value="/dosen/listDosen" var="next">
                        <c:param name="page" value="${page + 1}"/>
                    </c:url>
                    <c:if test="${page + 1 <= maxPages}">
                        <li class="page-item"><a href='<c:out value="${next}" />' class="pn next">Next</a></li> 
                    </c:if>
                </ul>
            </nav>
        </div>
    </div>

</section>


