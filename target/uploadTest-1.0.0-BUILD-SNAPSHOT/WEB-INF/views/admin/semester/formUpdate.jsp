<%-- 
    Document   : form
    Created on : Aug 12, 2019, 10:31:29 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css">
    .errormsg {
        color: red;
    }
</style>
<title>Semester</title>

<section class="content-header">
    <h1>
        Form Data
        <small>Semester</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Form Semester</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Form Input Data Semester</h3>
            <div class="pull-right">
                <a href="${pageContext.request.contextPath}/semester/listSemester" class="btn bg-yellow-gradient"><i class="fa fa-undo"></i> Kembali</a>
            </div>
        </div>
        <form:form role="form" action="${pageContext.request.contextPath}/semester/updateSemester" method="post" modelAttribute="semester">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <form:hidden path="idSemester"/>
                        <div class="form-group">
                            <label for="semester">Semester</label>
                            <form:input type="text" path="semester" class="form-control" id="semester" placeholder="Semester" readonly="true"   />
                            <small><form:errors path="semester" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="tahunAjaran">Tahun Ajaran</label>
                            <form:input type="text" path="tahunAjaran" class="form-control" id="Tahun Ajaran" placeholder="Tahun Ajaran"/>
                            <small><form:errors path="tahunAjaran" cssClass="errormsg" /></small>
                        </div>

                        <div class="form-group">
                            <label for="status">Status</label>
                            <form:select path="status" class="form-control select2">
                                <form:options items="${mapStatus}" />
                            </form:select>
                            <small><form:errors path="status" cssClass="errormsg" /></small>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-paper-plane"></i> Submit</button>
                            <button type="reset" class="btn bg-green-gradient"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</section>
