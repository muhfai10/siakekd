<%-- 
    Document   : dashboard
    Created on : Aug 16, 2019, 1:20:02 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>

<title>Dashboard</title>
<section class="content-header">
    <h1>
        Dashboard
        <small> Control Panel</small>
        <br/>
        <small>
            Tanggal : <b>${tanggal}</b>


        </small>

    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-blue-gradient"><i class="fa fa-group"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><b>DATA MAHASISWA<br/>TEKINFO</b></span>
                    <span class="info-box-number">

                        <tr>${totalMhs}</tr>

                    </span>

                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow-gradient"><i class="fa fa-user"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><b>DATA DOSEN<br/>TEKINFO</b></span>
                    <span class="info-box-number">

                        <tr>${totalDosen}</tr>

                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-blue-gradient"><i class="fa fa-user-circle-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><b>DATA USERS <br/>TERDAFTAR</b></span>
                    <span class="info-box-number">

                        <tr>${totalUser}</tr>


                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow-gradient"><i class="glyphicon glyphicon-education"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><b>DATA PERKULIAHAN <br/>TEKINFO</b></span>
                    <span class="info-box-number">

                        <tr>${totalPerk}</tr>

                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
 
</section>

<!DOCTYPE html>


<!DOCTYPE html>

