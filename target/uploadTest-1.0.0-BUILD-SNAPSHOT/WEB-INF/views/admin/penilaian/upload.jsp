<%-- 
    Document   : upload
    Created on : Sep 4, 2019, 9:18:21 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<title>Upload Excel</title>

<section class="content-header">
    <h1>
        Import Data
        <small>Mahasiswa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Import Data</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <form action="${pageContext.request.contextPath}/dashboard/upload/2007" method="post" enctype="multipart/form-data">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="box-title">Import Excel</h4>
                    </div>
                   
                    <div class="panel-body">	
                         <div class="pull-right">
                        <a href="${pageContext.request.contextPath}/mahasiswa/listMahasiswa" class="btn bg-yellow-gradient"><i class="fa fa-undo"></i> Kembali</a>
                    </div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Import Data</label>
                                <div class="col-sm-6">
                                    <input name="excelfile2007" type="file" class="btn btn-browser">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <button type="submit" class="btn bg-blue-gradient" id="saveEdit">
                                        <span class="glyphicon glyphicon-floppy-disk"></span> Save
                                    </button>					
                                </div>	
                            </div>
                        </div>
                    </div>
                </div>	    
            </form>
        </div>
    </div>
</section>