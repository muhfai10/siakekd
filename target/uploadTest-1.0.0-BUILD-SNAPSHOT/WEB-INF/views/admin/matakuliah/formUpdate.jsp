<%-- 
    Document   : formUpdate
    Created on : Aug 13, 2019, 12:11:07 AM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Edit Data Matakuliah</title>
<style type="text/css">
    .errormsg {
        color: red;
    }
</style>
<section class="content-header">
    <h1>
        Form Data
        <small>Matakuliah</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Form Matakuliah</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Form Edit Data Matakuliah</h3>
            <div class="pull-right">
                <a href="${pageContext.request.contextPath}/matakuliah/listMatkul" class="btn bg-yellow-gradient"><i class="fa fa-undo"></i> Kembali</a>
            </div>
        </div>
        <form:form role="form" action="${pageContext.request.contextPath}/matakuliah/updateMatkul" method="post" modelAttribute="matakuliah">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="form-group">
                            <label for="kodeMk">Kode Matakuliah</label>
                            <form:input type="text" path="kodeMk" class="form-control" id="kodeMk" placeholder="Kode Matakuliah" maxlength="6"/>
                            <small><form:errors path="kodeMk" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="mataKuliah">Nama Matakuliah</label>
                            <form:input type="text" path="mataKuliah" class="form-control" id="mataKuliah" placeholder="Nama Matakuliah"/>
                            <small><form:errors path="mataKuliah" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="nip">SKS</label>
                            <form:input type="number" path="sks" class="form-control" id="sks" placeholder="SKS" maxlength="3"/>
                            <small><form:errors path="sks" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <form:input type="text" path="keterangan" class="form-control" id="keterangan" placeholder="Keterangan"/>
                            <small><form:errors path="keterangan" cssClass="errormsg" /></small>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-paper-plane"></i> Submit</button>
                            <button type="reset" class="btn bg-green-gradient"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</section>

