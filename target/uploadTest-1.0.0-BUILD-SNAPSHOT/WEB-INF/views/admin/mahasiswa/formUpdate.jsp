<%-- 
    Document   : formUpdate
    Created on : Aug 13, 2019, 12:11:07 AM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css">
    .errormsg {
        color: red;
    }
</style>
<title>Mahasiswa</title>

<section class="content-header">
    <h1>
        Form Data
        <small>Mahasiswa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Form Mahasiswa</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Form Edit Data Mahasiswa</h3>
            <div class="pull-right">
                <a href="${pageContext.request.contextPath}/mahasiswa/listMahasiswa" class="btn bg-yellow-gradient"><i class="fa fa-undo"></i> Kembali</a>
            </div>
        </div>
        <form:form role="form" action="${pageContext.request.contextPath}/mahasiswa/updateMahasiswa" method="post" modelAttribute="mahasiswa">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="form-group">
                            <label for="noBp">No. BP</label>
                            <form:input type="text" path="noBp" class="form-control" id="noBp" placeholder="No. BP" maxlength="10"/>
                            <small><form:errors path="noBp" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama Mahasiswa</label>
                            <form:input type="text" path="nama" class="form-control" id="nama" placeholder="Nama Mahasiswa"/>
                            <small><form:errors path="nama" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="kelas">Kelas / Tingkat</label>
                            <form:select path="kelas" class="form-control select2">
                                <form:options items="${mapKelas}" />
                            </form:select>
                            <small><form:errors path="kelas" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="lokal">Lokal</label>
                            <form:input type="text" path="lokal" class="form-control" id="lokal" placeholder="Lokal"/>
                            <small><form:errors path="lokal" cssClass="errormsg" /></small>
                        </div>
                        <form:hidden path="status"/>
                        <div class="box-footer">
                            <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-paper-plane"></i> Submit</button>
                            <button type="reset" class="btn bg-green-gradient"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                </div>

            </form:form>
        </div>
    </div>
</section>
