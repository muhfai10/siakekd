<%-- 
    Document   : form
    Created on : Aug 12, 2019, 10:31:29 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<%-- 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        
        <title>JSP Page</title>
    </head>
    <body>
--%>
<title>Pertanyaan</title>
<style type="text/css">
    .errormsg {
        color: red;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Form Pertanyaan
        <small>Mahasiswa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Form Pertanyaan</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Form Input Data Pertanyaan</h3>
            <div class="pull-right">
                <a href="${pageContext.request.contextPath}/pertanyaan/listPertanyaan" class="btn bg-yellow-gradient"><i class="fa fa-undo"></i> Kembali</a>
            </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form:form role="form" action="${pageContext.request.contextPath}/pertanyaan/insertPertanyaan" method="post" modelAttribute="pertanyaan">
            <div class="box-body">
                <div class="col-md-4 col-md-offset-4">
                    <div class="form-group">
                        <form:hidden path="idKuesioner"/>
                        <div class="form-group">
                            <label for="pertanyaan">Pertanyaan</label>
                            <form:input type="text" path="pertanyaan" class="form-control" id="pertanyaan" placeholder="Pertanyaan"/>
                            <small><form:errors path="pertanyaan" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="aspek">Aspek Penilaian</label>
                            <form:select path="aspek" class="form-control select2">
                                <form:options items="${mapAspek}" />
                            </form:select>
                            <small><form:errors path="aspek" cssClass="errormsg" /></small>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-paper-plane"></i> Submit</button>
                            <button type="reset" class="btn bg-green-gradient"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</section>
