<%-- 
    Document   : list
    Created on : Aug 12, 2019, 5:58:28 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<title>Pertanyaan</title>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        List Kelas
        <small>Mahasiswa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List Kelas</li>
    </ol>
</section>
<section class="content">
 <c:if test="${not empty message}">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
            <i class="icon fa fa-check"></i>
            ${message}
        </div>
    </c:if>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h1 class="box-title">List Data Kelas</h1>
            <br>
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <a href="${pageContext.request.contextPath}/kelas/newKelas" class="btn bg-blue-gradient">
                        <i class="fa fa-user-plus fa-lg"></i> Tambah Data
                    </a>
                </div>
                

            <br>
        </div>

        <div class="box-body table-responsive">
            <table id="table_pertanyaan" class="table table-bordered table-hover" >

                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kelas</th>
                        <th>Prodi</th>
                        <th>Kode</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listKelas}" var="kelas" varStatus="status">
                        <tr>    

                            <td><c:out value="${status.count}" /></td>
                            <td style="display:none;"><c:out value="${kelas.idKelas}" /></td>
                            <td><c:out value="${kelas.kelas}" /></td>
                            <td><c:out value="${kelas.prodi}" /></td>
                            <td><c:out value="${kelas.kode}" /></td>

                            <td><a href="${pageContext.request.contextPath}/kelas/editKelas/${kelas.idKelas}" class="btn bg-blue-gradient btn-sm"><i class="fa fa-pencil fa-lg"></i>&nbsp;&nbsp;&nbsp;Edit</a> | <a href="${pageContext.request.contextPath}/kelas/deleteKelas/${kelas.idKelas}" class="btn bg-yellow-gradient btn-sm">
                                    <i class="fa fa-trash fa-lg"></i>&nbsp;&nbsp;&nbsp;Delete</a></td>

                        </tr>
                    </c:forEach>


            </table>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <c:url value="/kelas/listKelas" var="prev">
                        <c:param name="page" value="${page-1}"/>
                    </c:url>
                    <c:if test="${page > 1}">
                        <li class="page-item"><a href="<c:out value="${prev}" />" class="pn prev">Prev</a></li> 
                        </c:if>

                    <c:forEach begin="1" end="${maxPages}" step="1" varStatus="i">
                        <c:choose>
                            <c:when test="${page == i.index}">
                                <li class="page-item active"><span>${i.index}</span></li> 
                                    </c:when>
                                    <c:otherwise>
                                        <c:url value="/kelas/listKelas" var="url">
                                            <c:param name="page" value="${i.index}"/>
                                        </c:url>
                                <li class="page-item"> <a href='<c:out value="${url}" />'>${i.index}</a></li> 
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <c:url value="/kelas/listKelas" var="next">
                            <c:param name="page" value="${page + 1}"/>
                        </c:url>
                        <c:if test="${page + 1 <= maxPages}">
                        <li class="page-item"><a href='<c:out value="${next}" />' class="pn next">Next</a></li> 
                        </c:if>
                </ul>
            </nav>
        </div>
    </div>


</section>

<%--
<script type="text/javascript">
    $(document).ready(function(){
    var data = eval('${listPertanyaan}'); //name mentioned in controller using model attribute
    var table = $('#table1').DataTable({ //id name of a table
    "sAjaxSource": "/listPertanyaan",  //request mapping for the particular url
            "sAjaxDataProp": "",
            "aaData": data,
            "aoColumns": [
                    { "mData": "idKuesioner"},
                    { "mData": "pertanyaan"}
            ],
            "paging":true,
            "retrieve":true

            });
    });
</script>
--%>