<%-- 
    Document   : form
    Created on : Aug 12, 2019, 10:31:29 PM
    Author     : MuhFai10
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css">
    .errormsg {
        color: red;
    }
</style>
<title>User</title>

<section class="content-header">
    <h1>
        Form Data
        <small>User</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Form User</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Form Input Data User</h3>
            <div class="pull-right">
                <a href="${pageContext.request.contextPath}/user/listUser" class="btn bg-yellow-gradient"><i class="fa fa-undo"></i> Kembali</a>
            </div>
        </div>
        <form:form role="form" action="${pageContext.request.contextPath}/user/insertUser" method="post" modelAttribute="user">
            <div class="box-body">
                <div class="col-md-4 col-md-offset-4">
                    <div class="form-group">
                        <form:hidden path="idUser"/>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <form:input type="text" path="username" class="form-control" id="username" placeholder="Username" maxlength="16"/>
                            <small><form:errors path="username" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <form:input type="text" path="password" class="form-control" id="password" placeholder="Password"/>
                            <small><form:errors path="password" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <form:input type="text" path="nama" class="form-control" id="nama" placeholder="Nama"/>
                            <small><form:errors path="nama" cssClass="errormsg" /></small>
                        </div>
                        <div class="form-group">
                            <label for="role">Role</label>
                            <form:select path="role" class="form-control select2">
                                <form:options items="${mapRole}" />
                            </form:select>
                            <small><form:errors path="role" cssClass="errormsg" /></small>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn bg-blue-gradient"><i class="fa fa-paper-plane"></i> Submit</button>
                            <button type="reset" class="btn bg-green-gradient"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</section>
